<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yts_banners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('banner_full_width', '255')->nullable();
            $table->string('banner_full_width_title', '255')->nullable();
            $table->string('banner_full_width_link', '255')->nullable();
            $table->string('banner_left', '255')->nullable();
            $table->string('banner_left_title', '255')->nullable();
            $table->string('banner_left_link', '255')->nullable();
            $table->string('banner_right_top', '255')->nullable();
            $table->string('banner_right_top_title', '255')->nullable();
            $table->string('banner_right_top_link', '255')->nullable();
            $table->string('banner_right_bottom_left', '255')->nullable();
            $table->string('banner_right_bottom_left_title', '255')->nullable();
            $table->string('banner_right_bottom_left_link', '255')->nullable();
            $table->string('banner_right_bottom_right', '255')->nullable();
            $table->string('banner_right_bottom_right_title', '255')->nullable();
            $table->string('banner_right_bottom_right_link', '255')->nullable();
            $table->string('product_ads', '255')->nullable();
            $table->string('product_ads_title', '255')->nullable();
            $table->string('product_ads_link', '255')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yts_banners');
    }
}
