<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yts_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('product_id')->unsigned()->nullable();
            $table->string('color')->nullable();
            $table->integer('price')->nullable();
            $table->smallInteger('quantity')->unsigned()->nullable();
            $table->integer('order_info_id')->unsigned()->nullable();
            $table->foreign('order_info_id')
                    ->references('id')->on('yts_order_info')->onDelete('cascade');
            $table->integer('unique_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yts_orders');
    }
}
