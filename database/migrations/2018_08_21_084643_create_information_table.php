<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yts_information', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address', '250')->nullable();
            $table->string('email1', '150')->nullable();
            $table->string('email2', '150')->nullable();
            $table->string('phone1', '50')->nullable();
            $table->string('phone2', '50')->nullable();
            $table->string('hotline', '50')->nullable();
            $table->string('working_hours', '250')->nullable();
            $table->integer('shipping_fee')->default(0);
            $table->text('bank_info')->nullable();
            $table->string('facebook', '100')->nullable();
            $table->string('twitter', '100')->nullable();
            $table->string('instagram', '100')->nullable();
            $table->string('youtube', '100')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yts_information');
    }
}
