<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yts_brand_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image', '200')->nullable();
            $table->string('title', '200')->nullable();
            $table->string('link', '200')->nullable();
            $table->smallInteger('order')->unsigned()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yts_brand_images');
    }
}
