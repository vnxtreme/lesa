<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yts_order_info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('payment_name')->nullable();
            $table->string('payment_email', '100')->nullable();
            $table->string('payment_telephone', '20')->nullable();
            $table->string('payment_address', '200')->nullable();
            $table->string('payment_district', '200')->nullable();
            $table->integer('payment_zone')->nullable();
            $table->string('shipping_method')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('yts_order_info', function (Blueprint $table) {
            //
        });
    }
}
