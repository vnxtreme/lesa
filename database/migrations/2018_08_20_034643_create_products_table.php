<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yts_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', '255')->nullable();
            $table->integer('current_price')->default(0);
            $table->integer('old_price')->default(0);
            $table->string('brand')->nullable();
            $table->integer('quantity')->default(0);
            $table->boolean('latest')->default(0);
            $table->boolean('outstanding')->default(0);
            $table->boolean('bestseller')->default(0);
            $table->boolean('promotion')->default(0);
            $table->text('images')->nullable();
            $table->integer('warranty')->default(0);
            
            $table->integer('author_id')->unsigned()->nullable();
            $table->string('title')->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            $table->string('seo_title')->nullable();
            $table->text('excerpt')->nullable();
            $table->text('body')->nullable();
            $table->string('image')->nullable();
            $table->string('slug')->unique();
            $table->text('meta_description')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->enum('status', ['PUBLISHED', 'DRAFT', 'PENDING'])->default('DRAFT');
            // $table->boolean('featured')->default(0);
            $table->timestamps();
            
            $table->foreign('category_id')->references('id')->on('yts_categories');
            $table->foreign('author_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yts_products');
    }
}
