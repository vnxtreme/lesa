/*$(window).bind('popstate', function() {
	$.ajax({url:location.pathname,success: function(data){
		//$('#content').html(data);
	}});
});*/

$(document).ready(function(){
    
    showLoading = function(title, mess) {
        $('body').addClass('bodyLoading');
        if(title)
            $('.titleLoading').html(title);
        if(mess)
            $('.messLoading').html(mess);
    }

    hideLoading = function() {
        $('.titleLoading').html('Đang tải dữ liệu..');
        $('.messLoading').html('Vui lòng đợi trong giây lát');
        $('body').removeClass('bodyLoading');
    }

    var msie6 = $.browser == 'msie' && $.browser.version < 7;
    var msie8 = $.browser == 'msie' && $.browser.version < 9;
    var complete = 0;
    var currentSelection = 0;
    var currentUrl = '';
    for(var i = 0; i < $("#autocomplete ul li a").length; i++) {
        $("#autocomplete ul li a").eq(i).data("number", i);
    }
    function navigate(direction) {

        // Check if any of the menu items is selected
        if($("#autocomplete ul li.search_hover a").length == 0) {
            currentSelection = -1;
        }
		
        //JBP - focus back on search field if up arrow pressed on top search result
        if(direction == 'up' && currentSelection == 0) {
            $('#mallSearch #keyword').focus();
        }
        //

        if(direction == 'up' && currentSelection != -1) {
            if(currentSelection != 0) {
                currentSelection--;
            }
        } else if (direction == 'down') {
            if(currentSelection != $("#autocomplete ul li").length -1) {
                currentSelection++;
            }
        }
        setSelected(currentSelection);
    }

    function setSelected(menuitem) {

        //JBP - get search result to place in search field on hover
        var title = $("#autocomplete ul li a").eq(menuitem).attr('title');
        $('#mallSearch #keyword').val(title);
        //

        $("#autocomplete ul li").removeClass("search_hover");
        $("#autocomplete ul li a").eq(menuitem).parent().addClass("search_hover");
        currentUrl = $("#autocomplete ul li a").eq(menuitem).attr("href");
    }
    $('#mallSearch #keyword').keydown(function(e) {
        switch(e.keyCode) { 
            // User pressed "up" arrow
            case 38:
                navigate('up');
                break;
            // User pressed "down" arrow
            case 40:
                navigate('down');
                break;
            // User pressed "enter"
            case 13:
                if(currentUrl != '' && currentUrl != "undefined") {
                    window.location = currentUrl;
                }else{
                //$('#mallSearch button').click();
                }
                break;
        }
    });
    $('#mallSearch #keyword').keydown(function(e){

        //jump from search field to search results on keydown
        if (e.keyCode == 40) { 
            $('#mallSearch #keyword').blur();
            return false;
        }

        //hide search results on ESC
        if (e.keyCode == 27) { 
            $("#autocomplete").hide();
            $('#mallSearch #keyword').blur();
            return false;
        }

        //focus on search field on back arrow or backspace press
        if (e.keyCode == 37 || e.keyCode == 8) { 
            $('#mallSearch #keyword').focus();
        }

    });

    $("body").click(function() {
        $("#autocomplete").hide();
    });
    $('#mallSearch button').click(function(){
        var values_input_search = $.trim($(this).parent().find('input').val());
        if(!values_input_search || values_input_search == ''){
            alert('Chưa nhập từ khóa!');
            return false;
        }else{
            $.post(web_address+'tim-kiem',{
                keyword:values_input_search
            },function(data){
                window.location = web_address+'tim-kiem'+url_prefix+'?s='+data.replace(' ','+');
            });
            return false;
        }
    });
    $('#mallSearch #keyword').bind('keyup click', function() {
        $(this).addClass('zme-autocomplete-loading');
        if( $(this).val() == ''){
            $(this).removeClass('zme-autocomplete-loading');
        }
        setTimeout('search_autocomplete()',1000);
    });
    $("#mallSearch #keyword").focus(function() {
        $(this).css('color','#333333');
        var sv = $(this).val(); //get current value of search field
    });
    search_autocomplete = function(){
        $.post(web_address+"ajax/autocomplete", //post
        {
            q:$('#mallSearch #keyword').val()
        }, 
        function(data){
            $('#mallSearch #keyword').removeClass('zme-autocomplete-loading');
            //hide autocomplete if no more than 2 characters
            if (data == 'hide') {
                $('#autocomplete').hide();
            }

            //show autocomplete if more than 2 characters
            if (data != 'hide') {
                $("#autocomplete").html(data);
                if (data) {
                    $("#autocomplete").show();
                }
            }
        });	
    };
    change_relate_right = function(type,ob){
        $.post(web_address+'ajax/change_relate_right',{
            type:type
        },function(data){
            $('.box_relate_right li.current').removeClass('current');
            $(ob).addClass('current');
            $('.show_relate_right .relate_right').html(data);
        });
    }
    
    
    send_mess_ajax = function(){
        var comment_content = $('#comment_content').val();
        var comment_title = $('#comment_title').val();
        if($('#comment_parent').length){
            var comment_parent = $('#comment_parent').val();
        }else{
            var comment_parent = 0;
        }
        if(!comment_title){
            quatang.box.show('<i>Chưa nhập tiêu đề tin!</i>',0,0,0,0,2);
        }else if(!$.trim(comment_content)){
            quatang.box.show('<i>Chưa nhập nội dung tin nhắn!</i>',0,0,0,0,2);
        }else{
            $.post(web_address+'ajax/send_mess',{
                comment_content:comment_content,
                comment_parent:comment_parent,
                comment_title:comment_title,
            },function(data_1){
                if(data_1 == 1){
                    alert('TIn nhắn được gửi thành công');
                    window.location.hash = "tin-da-gui";
                }else{
                    alert(data_1);
                }
            });
        }
    }
    send_review_ajax = function(){
        var comment_content = $('#comment_content').val();
        var get_id = ($('#comment_prod_id').val()).split('-');
        if(get_id.length == 2){
            var comment_prod_id = get_id[0];
            var comment_parent = get_id[1];	
        }else{
            quatang.box.show('<i>Chưa chọn sản phẩm đánh giá!</i>',0,0,0,0,2);
            return false;
        }
        if(!$.trim(comment_content)){
            quatang.box.show('<i>Chưa nhập nội dung đánh giá!</i>',0,0,0,0,2);
        }else if(!comment_prod_id){
            quatang.box.show('<i>Chưa chọn sản phẩm đánh giá!</i>',0,0,0,0,2);
        }else{
            $.post(web_address+'ajax/send_review',{
                comment_content:comment_content,
                comment_parent:comment_parent,
                comment_prod_id:comment_prod_id,
            },function(data_1){
                if(data_1 == 1){
                    alert('Đánh giá được gửi thành công');
                    window.location.hash = "order-review";
                }else{
                    alert(data_1);
                }
            });
        }
    };

    open_box_rate = function(){
        if(!$('.rate-from').hasClass('opened')) {
            $('.rate-from').slideDown(500).addClass('opened');
            $('.rating-col-submit button').html('đóng');
        } else {
            $('.rate-from').slideUp(500).removeClass('opened');
            $('.rating-col-submit button').html('Đánh giá sản phẩm');
        }
    }

    send_rate_ajax = function(){
        //var rate_star = $('#rate-product').attr('data-rate-value');
        var rate_star = $('#rate_prod_star').val();
        if(!rate_star){
            $('.rating-col-submit').notify("Bạn chưa chọn sao!",{ className: "error",position:"top right"});
        }else{
            if(check_user){
                if(rate_star){
                    var rate_prod_id = $('#rate_prod_id').val();
                    $.post(web_address+'ajax/send_rate',{
                        rate_star:rate_star,
                        rate_prod_id:rate_prod_id,
                    },function(data_1){
                        if(data_1 == 1){
                            
                            $("#rate-product").rateYo("option", "readOnly", true);
                            //$("#rate-product").rateYo("readOnly");
                           // $("#rate-product").rateYo("option", "readOnly");
                            //$("#rate-product").rate("destroy");
                            $('#btn-send-rate').hide();
                            $('.rating-col-submit').notify("Đánh giá thành công!",{ className: "success",position:"bottom center"}); 
                        }else{
                            $('.rating-col-submit').notify("Không thành công",{ className: "error",position:"bottom center"});
                        }
                        console.log(data_1);
                    });
                }
            }else{ 
                $("#myLogin").modal()
            }
        }
    };

    send_comment_ajax = function(){
        var comment_content = $('#comment_content').val();
        if(!comment_content){
            //$('#comment_content').focus();
            $('textarea[name=comment_content]').focus();
            $('#respond').notify("Bạn chưa nhập nội dung bình luận!",{ className: "error",position:"bottom right"});
        }else{
            if(check_user){
                if(comment_content){
                    var comment_parent = $('#comment_parent').val();
                    var comment_prod_id = $('#comment_prod_id').val();
                    $.post(web_address+'ajax/send_comment',{
                        comment_content:comment_content,
                        comment_parent:comment_parent,
                        comment_prod_id:comment_prod_id,
                    },function(data_1){
                        if(data_1 == 1){
                            $('#respond').notify("Bình luận được gửi thành công!",{ className: "success",position:"bottom right"});
                            //alert('Hỏi đáp được gửi thành công');
                            window.location.reload(true);
                        }else{
                            alert(data_1);
                        }
                    });
                }
            }else{
                var comment_parent = $('#comment_parent').val();
                var comment_prod_id = $('#comment_prod_id').val();
                //TINY.box.show({html:$('#wrapper_login').html(),animate:false});
                //box_login_ajax();
                $("#myLogin").modal()
            }
        }
    };

    send_comment_ajax = function(){
        var comment_content = $('#comment_content').val();
        if(!comment_content){
            //$('#comment_content').focus();
            $('textarea[name=comment_content]').focus();
            $('#respond').notify("Bạn chưa nhập nội dung bình luận!",{ className: "error",position:"bottom right"});
        }else{
            if(check_user){
                if(comment_content){
                    var comment_parent = $('#comment_parent').val();
                    var comment_prod_id = $('#comment_prod_id').val();
                    $.post(web_address+'ajax/send_comment',{
                        comment_content:comment_content,
                        comment_parent:comment_parent,
                        comment_prod_id:comment_prod_id,
                    },function(data_1){
                        if(data_1 == 1){
                            $('#respond').notify("Bình luận được gửi thành công!",{ className: "success",position:"bottom right"});
                            //alert('Hỏi đáp được gửi thành công');
                            window.location.reload(true);
                        }else{
                            alert(data_1);
                        }
                    });
                }
            }else{
                var comment_parent = $('#comment_parent').val();
                var comment_prod_id = $('#comment_prod_id').val();
                //TINY.box.show({html:$('#wrapper_login').html(),animate:false});
                //box_login_ajax();
                $("#myLogin").modal()
            }
        }
    };

    remove_prod_in_cart = function(rowid){
        $.post(web_address+'ajax/load_cart',{
            action:'del_prod',
            rowid:rowid
        },function(data){
            $('#minicart_item_'+rowid).hide();
            setTimeout(get_productcart(), 500);
            //setTimeout(showincartdropdown(), 2000);
            //$('.header-main .header-cart .dropdown-toggle').dropdown('toggle');
            //console.log(data);
            get_data = data.split('|||||');
            // prod_in_cart = get_data[0];
            total_prod_in_cart = get_data[2];
            $('.header-cart .circle').html(total_prod_in_cart);
            // total_price_prod_in_cart = get_data[1];
            // $('.text_cart span').html(prod_in_cart);
            // $('.text_cart').attr('rel',total_prod_in_cart);
            // $('.slide_bottom .box_slide .number_price').html(total_price_prod_in_cart);
            // $('.slide_bottom .box_left').attr("rel",'no-active');
            // $('.slide_bottom .box_body').attr("data-prod-cart",prod_in_cart);
            // $('.slide_bottom .box_body').animate({
            //     'top': '-10px'
            // },500,function(){
            //     setTimeout(get_productcart(), 500);
            //         setTimeout(showincartdropdown(), 2000);
            //     //get_productcart();
            //     //show_cart_animate();
            // });
        });
    }
    $('.header-cart').on('show.bs.dropdown', function () {
      // do something…
    });

    reply_comment = function(comment_parent){
        var box_send_comment = $('#respond').html();
        $('#respond').remove();
        if(comment_parent > 0){
            $('#li-comment-'+comment_parent).append('<div id="respond">'+box_send_comment+'</div>');
            $('#respond #comment_parent').val(comment_parent);
            $('#cancel_reply').removeClass('hide_reply');	
        }else{
            $('.commentlist').parent().append('<div id="respond">'+box_send_comment+'</div>');
            $('#respond #comment_parent').val(0);
            $('#cancel_reply').addClass('hide_reply');	
        }		
    }
    buy_product = function(prod_id){
        if(check_user){
            window.location.href = web_address+'mua-hang-nhanh/p'+prod_id+'m0'+url_prefix;
        }else{
            tb_show_inline(web_address+'ajax/box_login?buy_product='+prod_id+'&KeepThis=true&height=288&width=482');
        }
    };
    like_product = function(prod_id){
        $.post(web_address+'ajax/like_product',{
            prod_id:prod_id,
        },function(data_1){
            alert(data_1);
        });
    };
    favorite = function(prod_id){
        if(check_user){
            $.post(web_address+'ajax/send_favorite',{
                prod_id:prod_id,
            },function(data_1){
                alert(data_1);
            });
        }else{
            tb_show_inline(web_address+'ajax/box_login_favorite?favorite='+prod_id+'&KeepThis=true&height=288&width=482');
        }
    };
    box_register_ajax = function(from_box_login){
        var box_login = $('.wrapper_login');
        var buy_product = $('[name="buy_product"]',box_login).val();
        if(!buy_product){
            buy_product = '';
        }
        if(from_box_login === false){
            tb_show_inline(web_address+'ajax/box_register?buy_product='+buy_product+'KeepThis=true&height=432&width=482');	
        }
        else{
            tb_remove();
            setTimeout("tb_show_inline(web_address+'ajax/box_register?buy_product="+buy_product+"&KeepThis=true&height=432&width=482')",300);
        }
    }
    box_register_favorite_ajax = function(from_box_login){
        var box_login = $('.wrapper_login');
        var favorite = $('[name="favorite"]',box_login).val();
        if(from_box_login === false){
            tb_show_inline(web_address+'ajax/box_register_favorite?favorite='+favorite+'KeepThis=true&height=412&width=482');	
        }
        else{
            tb_remove();
            setTimeout("tb_show_inline(web_address+'ajax/box_register_favorite?favorite="+favorite+"&KeepThis=true&height=412&width=482')",300);
        }
    }
    box_login_ajax = function(from_box_register){
        var box_login = $('.wrapper_register');
        var buy_product = $('[name="buy_product"]',box_login).val();
		
        if(!buy_product){
            buy_product = '';
        }
        if(from_box_register === false){
            tb_show_inline(web_address+'ajax/box_login?buy_product='+buy_product+'KeepThis=true&height=288&width=482');	
        }
        else{
            tb_remove();
            setTimeout("tb_show_inline(web_address+'ajax/box_login?buy_product="+buy_product+"&KeepThis=true&height=288&width=482')",300);
        }
    }
    box_login_favorite_ajax = function(from_box_register){
        var box_login = $('.wrapper_register');
        var name_favorite = $('[name="favorite"]',box_login).val();
        if(!name_favorite){
            name_favorite = '';
        }
        if(from_box_register === false){
            tb_show_inline(web_address+'ajax/box_login_favorite?favorite='+name_favorite+'KeepThis=true&height=288&width=482');	
        }
        else{
            tb_remove();
            setTimeout("tb_show_inline(web_address+'ajax/box_login_favorite?favorite="+name_favorite+"&KeepThis=true&height=288&width=482')",300);
        }
    }
    register_button_ajax = function(){
        var box_register = $('.wrapper_register');
        var user_name = $('#user_name',box_register).val();
        var user_login = $('#user_login',box_register).val();
        var user_pass = $('#user_pass',box_register).val();
        var user_fullname = $('#user_fullname',box_register).val();
        var user_phone = $('#user_phone',box_register).val();
        var user_pass_again = $('#user_pass_again',box_register).val();
        if(!user_name){
            alert('Vui lòng điền tên đăng nhập');
            $('#user_name',box_register).focus();
        }else if(user_name.length < 6){
            alert('Tên đăng nhập tối thiểu 6 ký tự');
            $('#user_name',box_register).focus();
        }/*else if(!user_login){
            alert('Vui lòng nhập địa chỉ email của bạn');
            $('#user_login',box_register).focus();
        }else if(check_email(user_login) == !1){
            alert('Địa chỉ email không hợp lệ');
            $('#user_login',box_register).focus();
        }*/else if(!user_pass){
            alert('Vui lòng nhập mật khẩu đăng nhập');
            $('#user_pass',box_register).focus();
        }else if(user_pass.length < 6 || user_pass.length > 10){
            alert('Mật khẩu tối thiểu từ 6 đến 20 ký tự');
            $('#user_pass',box_register).focus();
        }else if(!user_pass_again){
            alert('Vui lòng nhập mật khẩu đăng nhập');
            $('#user_pass_again',box_register).focus();
        }else if(user_pass != user_pass_again){
            alert('Mật khẩu không trùng nhau');
            $('#user_pass_again',box_register).focus();
        }else if(user_phone.length < 8 || user_phone.length > 15){
            alert('Số điện thoại tối thiểu 8 đến 15 ký tự');
            $('#user_phone',box_register).focus();
        }else if(!user_fullname){
            alert('Vui lòng nhập họ tên của bạn');
            $('#user_fullname',box_register).focus();
        }else if(user_fullname.length < 10 || user_fullname.length > 100){
            alert('Họ tên tối thiểu từ 10 đến 100 ký tự');
            $('#user_fullname',box_register).focus();
        }else{
            $('#loading_box').show();
            $('#registerButton').attr('disabled',true);
            $.post(web_address+'ajax/register_ajax',{
                user_name:user_name,
                user_email:user_login,
                user_pass:user_pass,
                user_pass_repeat:user_pass_again,
                user_phone:user_phone,
                user_fullname:user_fullname,
            },function(data){
                if(data == 1){
                    var comment_content = $('#comment_content').val();
                    var buy_product = $('[name="buy_product"]',box_register).val();
                    var name_favorite = $('[name="favorite"]',box_register).val();
                    if(buy_product){
                        var images_id = $('.inline_model_buy input:checkbox:checked[name="check_box_select_model[]"]').val();
                        if(!images_id){
                            images_id = 0;
                        }
                        window.location.href = web_address+'mua-hang-nhanh/p'+buy_product+'m'+images_id+url_prefix;
                    }else if(name_favorite){
                        $.post(web_address+'ajax/send_favorite',{
                            prod_id:name_favorite,
                        },function(data_1){
                            alert(data_1);
                        });
                        window.location.reload(true);
                        tb_remove();
                    }else if(comment_content){
                        var comment_parent = $('#comment_parent').val();
                        var comment_prod_id = $('#comment_prod_id').val();
                        $.post(web_address+'ajax/send_comment',{
                            comment_content:comment_content,
                            comment_parent:comment_parent,
                            comment_prod_id:comment_prod_id,
                        },function(data_1){
                            if(data_1 == 1){
                                alert('Hỏi đáp được gửi thành công');
                                window.location.reload(true);
                            }else{
                                alert(data_1);
                            }
                        });
                    }else{
                        window.location.reload(true);
                    }
                }else{
                    alert(data);
                    $('#loading_box').hide();
                    $('#registerButton').attr('disabled',false);
                }
            });
        }
    }

    register_form_outside = function(){
        var box_register = $('.wrapper_register');
        var user_name = $('#user_name',box_register).val();
        var user_login = $('#user_login',box_register).val();
        var user_pass = $('#user_pass',box_register).val();
        var user_fullname = $('#user_fullname',box_register).val();
        var user_phone = $('#user_phone',box_register).val();
        var user_pass_again = $('#user_pass_again',box_register).val();
        if(!user_name){
            alert('Vui lòng điền tên đăng nhập');
            $('#user_name',box_register).focus();
        }else if(user_name.length < 6){
            alert('Tên đăng nhập tối thiểu 6 ký tự');
            $('#user_name',box_register).focus();
        }/*else if(!user_login){
            alert('Vui lòng nhập địa chỉ email của bạn');
            $('#user_login',box_register).focus();
        }else if(check_email(user_login) == !1){
            alert('Địa chỉ email không hợp lệ');
            $('#user_login',box_register).focus();
        }*/else if(!user_pass){
            alert('Vui lòng nhập mật khẩu đăng nhập');
            $('#user_pass',box_register).focus();
        }else if(user_pass.length < 6 || user_pass.length > 10){
            alert('Mật khẩu tối thiểu từ 6 đến 20 ký tự');
            $('#user_pass',box_register).focus();
        }else if(!user_pass_again){
            alert('Vui lòng nhập mật khẩu đăng nhập');
            $('#user_pass_again',box_register).focus();
        }else if(user_pass != user_pass_again){
            alert('Mật khẩu không trùng nhau');
            $('#user_pass_again',box_register).focus();
        }else if(user_phone.length < 8 || user_phone.length > 15){
            alert('Số điện thoại tối thiểu 8 đến 15 ký tự');
            $('#user_phone',box_register).focus();
        }else if(!user_fullname){
            alert('Vui lòng nhập họ tên của bạn');
            $('#user_fullname',box_register).focus();
        }else if(user_fullname.length < 10 || user_fullname.length > 100){
            alert('Họ tên tối thiểu từ 10 đến 100 ký tự');
            $('#user_fullname',box_register).focus();
        }else{
            $('#loading_box').show();
            $('#registerButton').attr('disabled',true);
            $.post(web_address+'ajax/register_ajax',{
                user_name:user_name,
                user_email:user_login,
                user_pass:user_pass,
                user_pass_repeat:user_pass_again,
                user_phone:user_phone,
                user_fullname:user_fullname,
            },function(data){
                if(data == 1){
                    var comment_content = $('#comment_content').val();
                    var buy_product = $('[name="buy_product"]',box_register).val();
                    var name_favorite = $('[name="favorite"]',box_register).val();
                    if(buy_product){
                        var images_id = $('.inline_model_buy input:checkbox:checked[name="check_box_select_model[]"]').val();
                        if(!images_id){
                            images_id = 0;
                        }
                        window.location.href = web_address+'mua-hang-nhanh/p'+buy_product+'m'+images_id+url_prefix;
                    }else if(name_favorite){
                        $.post(web_address+'ajax/send_favorite',{
                            prod_id:name_favorite,
                        },function(data_1){
                            alert(data_1);
                        });
                        window.location.reload(true);
                        tb_remove();
                    }else if(comment_content){
                        var comment_parent = $('#comment_parent').val();
                        var comment_prod_id = $('#comment_prod_id').val();
                        $.post(web_address+'ajax/send_comment',{
                            comment_content:comment_content,
                            comment_parent:comment_parent,
                            comment_prod_id:comment_prod_id,
                        },function(data_1){
                            if(data_1 == 1){
                                alert('Hỏi đáp được gửi thành công');
                                window.location.reload(true);
                            }else{
                                alert(data_1);
                            }
                        });
                    }else{
                        window.location.reload(true);
                    }
                }else{
                    alert(data);
                    $('#loading_box').hide();
                    $('#registerButton').attr('disabled',false);
                }
            });
        }
    }

    login_button_ajax = function(isform='login_form'){
        //var box_login = $('.wrapper_login');
        //alert('co');
        var user_login = $('#'+isform+' #user_login').val();
        var user_pass = $('#'+isform+' #user_pass').val();
        if(!user_login){
            $.notify(
                "Vui lòng nhập Email / Số điện thoại",
                { className: "error",autoHideDelay:1000}
            );
            $('#user_login').focus();
        }else if(!user_pass){
            //$('#myLogin').html('Email / Số điện thoại không được để trống!')
            $.notify(
                "Vui lòng nhập mật khẩu",
                { className: "error",autoHideDelay:1000}
            );
            $('#user_pass').focus();
        }else{
             //alert(user_login);
            //$('#loading_box').show();
            //$('#loginButton').attr('disabled',true);
            $.post(web_address+'ajax/login',{
                user_login:user_login,
                user_pass:user_pass
            },function(data){

                if(data == 1){
                    var comment_content = $('#comment_content').val();
                    var buy_product = $('[name="buy_product"]',box_login).val();
                    var name_favorite = $('[name="favorite"]',box_login).val();
                    if(buy_product){
                        var images_id = $('.inline_model_buy input:checkbox:checked[name="check_box_select_model[]"]').val();
                        if(!images_id){
                            images_id = 0;
                        }
                        window.location.href = web_address+'mua-hang-nhanh/p'+buy_product+'m'+images_id+url_prefix;
                    }else if(name_favorite){
                        $.post(web_address+'ajax/send_favorite',{
                            prod_id:name_favorite,
                        },function(data_1){
                            alert(data_1);
                            window.location.reload(true);
                        });
                    }else if(comment_content){
                        var comment_parent = $('#comment_parent').val();
                        var comment_prod_id = $('#comment_prod_id').val();
                        $.post(web_address+'ajax/send_comment',{
                            comment_content:comment_content,
                            comment_parent:comment_parent,
                            comment_prod_id:comment_prod_id,
                        },function(data_1){
                            if(data_1 == 1){
                                alert('Hỏi đáp được gửi thành công');
								
                            }else{
                                alert(data_1);
                            }
                            window.location.reload(true);
                        });
                    }else{
                        
                        $.notify("Đăng nhập thành công", "success");
                         setTimeout(function () {
                            window.location.reload();
                            //window.location.href = web_address+'quan-ly'+url_prefix; 
                        },500);
                        //window.location.href = web_address+'quan-ly'+url_prefix; 
                    }
                }else{
                    $('.form-error').html(data);
                    $('.form-error').slideDown(500).delay(2000).slideUp(500);
                    
                    
                    $.notify(data,{ className: "error",autoHideDelay:1000});
                    ///alert(data);
                    $('#loading_box').hide();
                    $('#loginButton').attr('disabled',false);
                }
            });
        }
    };

    login_cart_ajax = function(isform='login_form'){

        var user_login = $('#'+isform+' #user_login').val();
        var user_pass = $('#'+isform+' #user_pass').val();
        if(!user_login){
            $('#login-dialog .modal-body').notify("Vui lòng nhập Email / Số điện thoại",{ className: "error",position:"top center"});
            $('#user_login').focus();
        }else if(!user_pass){
            $('#login-dialog .modal-body').notify("Vui lòng nhập mật khẩu",{ className: "error",position:"top center"});
            $('#user_pass').focus();
        }else{
            $.post(web_address+'ajax/login',{
                user_login:user_login,
                user_pass:user_pass
            },function(data){

                if(data == 1){
                    var comment_content = $('#comment_content').val();
                    var buy_product = $('[name="buy_product"]',box_login).val();
                    var name_favorite = $('[name="favorite"]',box_login).val();
                    $('#login-dialog .modal-body').notify("Đăng nhập thành công",{ className: "success",position:"top center"});
                    setTimeout(function () {
                        window.location.href = '/thong-tin-van-chuyen';
                    },500);
                }else{
                    $('#login-dialog .modal-body').notify(data,{ className: "error",position:"top center",autoHideDelay:4000});
                }
            });
        }
    };


    $('#login-dialog input[name=is_guest]').change(function(){
        var input_guest = $('#login-dialog input[name=is_guest]:checked').val();
        loginBoxShow(parseInt(input_guest));
    });

    loginBoxShow = function (guestVal){
        var box = $('#login-dialog');
        if(!guestVal){
            $('.hideform').slideDown();
        }else{
            $('.hideform').slideUp();
        }
    }

    add_smile_ajax = function(){
        tb_show('Chèn hình biểu tượng cảm xúc',web_address+'ajax/box_smile?height=390&width=520');
    };
    loginButton = function(){
        alert('csacsacacas');
    };
    number_format = function (num) {
        num = num.toString().replace(/\$|\,/g, '');
        if (isNaN(num)) num = "0";
        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        cents = num % 100;
        num = Math.floor(num / 100).toString();
        if (cents < 10) cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
            num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
        return (((sign) ? '' : '-') + num);
    };
    add_smile = function(text){
        tb_remove();
        var areaId = 'comment_content';
        var text = '/:'+text+':\\';
        var txtarea = document.getElementById(areaId);
        var scrollPos = txtarea.scrollTop;
        var strPos = 0;
        var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ? 
            "ff" : (document.selection ? "ie" : false ) );
        if (br == "ie") { 
            txtarea.focus();
            var range = document.selection.createRange();
            range.moveStart ('character', -txtarea.value.length);
            strPos = range.text.length;
        }
        else if (br == "ff") strPos = txtarea.selectionStart;
		
        var front = (txtarea.value).substring(0,strPos);  
        var back = (txtarea.value).substring(strPos,txtarea.value.length); 
        txtarea.value=front+text+back;
        strPos = strPos + text.length;
        if (br == "ie") { 
            txtarea.focus();
            var range = document.selection.createRange();
            range.moveStart ('character', -txtarea.value.length);
            range.moveStart ('character', strPos);
            range.moveEnd ('character', 0);
            range.select();
        }
        else if (br == "ff") {
            txtarea.selectionStart = strPos;
            txtarea.selectionEnd = strPos;
            txtarea.focus();
        }
        txtarea.scrollTop = scrollPos;
    }
    price_format = function (price) {
        if(price)
            price = price.replace(/\$|\=|\ |\./g, '');
        else
            price = 0;
        return parseInt(price);
    };
    $(' .plus_viewprod').click(function(){
        var val_current = price_format($(this).parent().parent().find('.update_prod_quantity').val());
        var val_stock = $(this).parent().next().find('span').text();
        val_stock = price_format(val_stock);
        var prod_weight = price_format($('.show_info_view .prod_weight').text());
        //if(val_current < val_stock){
            $.post(web_address+"ajax/change_price_region",{
                prod_weight:(prod_weight*(val_current+1))
            },function(data){
                $('.show_info_view .box_detail .ship_price').html(data);
            })
            $(this).parent().parent().find('.update_prod_quantity').val(val_current+1);
        //}else{
            //alert('Bạn chỉ có thể mua ' + val_current + ' cái. ');
        //}
    });
    $(' .minus_viewprod').click(function(){
        var val_current = price_format($(this).parent().parent().find('.update_prod_quantity').val());
        var val_stock = $(this).parent().next().find('span').text();
        val_stock = price_format(val_stock);
        var prod_weight = price_format($('.show_info_view .prod_weight').text());
        if(val_current > 1){
            $.post(web_address+"ajax/change_price_region",{
                prod_weight:(prod_weight*(val_current-1))
            },function(data){
                $('.show_info_view .box_detail .ship_price').html(data);
            })
            $(this).parent().parent().find('.update_prod_quantity').val(val_current-1);
        }
    });
    $('.cart_regist input').keypress(function(e){
        var keycode;
        if(window.e) keycode = window.e.keyCode;
        else if (e) keycode = e.which;
        else return true;
        if(keycode == 13){
            eval($('.cart_login .regist_btn a').attr('onclick'));
        }
    });
    $('.cart_login input').keypress(function(e){
        var keycode;
        if(window.e) keycode = window.e.keyCode;
        else if (e) keycode = e.which;
        else return true;
        if(keycode == 13){
            eval($('.cart_login .loginbtn a').attr('onclick'));
        }
    });
    $('#city').change(function(){
        var location_parent = $(this).val();
        $.post(web_address+'ajax/get_location',{
            location_parent:location_parent
        },function(data){
            $('#district').html(data);
        });
    });
    $('.del_prod').click(function(){
        

        var row_id = $(this).parent().attr('rel-data-product');
        var update_price = 1;
        var price_2_cur = $('#prod_'+row_id+' .title-subtotal').text();
        var rowid = $('#prod_'+row_id).attr('rel-data-product');
        price_2_cur = price_format(price_2_cur);
        var prod_weight_current = price_format($('#prod_'+row_id+' .title-weight').text());
        var prod_weight_all_current = price_format($('.cart_listitem .title-weightall').text());
        if(update_price == 1){
            var price_total_all = $('.cart_listitem .title-thanhtien').text();
            price_total_all = price_format(price_total_all);
            $('.cart_listitem .title-thanhtien').html(number_format(parseInt(price_total_all)-parseInt(price_2_cur)));
            $('.cart_listitem .title-weightall').text(number_format(prod_weight_all_current-prod_weight_current));
            update_price--;
            $.post(web_address+'ajax/load_cart',{
                action:'del_prod',
                rowid:rowid
            },function(data){
                $('#prod_'+row_id).fadeOut();
            });
            update_price++;
        }
    });

    $('.up_quantity').click(function(){
        var row_id = $(this).parent().attr('rel-data-product');
		
        var update_price2 = 1;
        var num_cur = $(this).parent().prev().text();
        var price_1_cur = $('#prod_'+row_id+' .title-price').text();
        price_1_cur = price_format(price_1_cur);
        var rowid = $('#prod_'+row_id).attr('rel-data-product');
        var prod_id = $('#prod_'+row_id).attr('rel-data-prod-id');
        var prod_model = $('#prod_'+row_id+' .title-product a font').text();
        var price_2_cur = $('#prod_'+row_id+' .title-subtotal').text();
        price_2_cur = price_format(price_2_cur);
        var price_2_next = number_format(parseInt(price_2_cur)+parseInt(price_1_cur));
        var prod_weight = price_format($('#prod_'+row_id+' .title-weight').attr('rel'));
        var prod_weight_current = price_format($('#prod_'+row_id+' .title-weight').text());
        var prod_weight_all_current = price_format($('.cart_listitem .title-weightall').text());
        if(update_price2 == 1){
            update_price2--;
            $.post(web_address+'ajax/load_cart',{
                action:'plus_prod',
                rowid:rowid,
                prod_model:prod_model,
                prod_id:prod_id
            },function(data){
                if(data != 1){
                    var price_total_all = $('.cart_listitem .title-thanhtien').text();
                    price_total_all = price_format(price_total_all);
                    $('.cart_listitem .title-thanhtien').html(number_format(parseInt(price_total_all)+parseInt(price_1_cur)));
                    $('#prod_'+row_id+ ' .title-soluong div span').html(parseInt(num_cur)+1);
                    $('#prod_'+row_id+' .title-subtotal').html(price_2_next);
                    $('#prod_'+row_id+' .title-weight').text(number_format(prod_weight+prod_weight_current));
                    $('.cart_listitem .title-weightall').text(number_format(prod_weight+prod_weight_all_current));
                }else{
                    quatang.box.show('<i>Sản phẩm không đủ số lượng!</i>',0,0,0,0,2);
                }
                update_price2++;
            });
        }else{
            quatang.box.show('<i>Lỗi trong quá trình xử lý. Bạn vui lòng kiểm tra lại!</i>',0,0,0,0,2);
        }
    });

    
    $('.down_quantity').click(function(){
        var row_id = $(this).parent().attr('rel-data-product');
        var update_price2 = 1;
        var num_cur = $(this).parent().prev().text();
        var price_1_cur = $('#prod_'+row_id+' .title-price').text();
        price_1_cur = price_format(price_1_cur);
        var rowid = $('#prod_'+row_id).attr('rel-data-product');
        var price_2_cur = $('#prod_'+row_id+' .title-subtotal').text();
        price_2_cur = price_format(price_2_cur);
        var price_2_next = number_format(parseInt(price_2_cur)-parseInt(price_1_cur));
        var prod_weight = price_format($('#prod_'+row_id+' .title-weight').attr('rel'));
        var prod_weight_current = price_format($('#prod_'+row_id+' .title-weight').text());
        var prod_weight_all_current = price_format($('.cart_listitem .title-weightall').text());
		
        if(update_price2 == 1){
            if(num_cur > 1){
                var price_total_all = $('.cart_listitem .title-thanhtien').text();
                price_total_all = price_format(price_total_all);
                $('.cart_listitem .title-thanhtien').html(number_format(parseInt(price_total_all)-parseInt(price_1_cur)));
                $(this).parent().prev().html(parseInt(num_cur)-1);
	            
                update_price2--;
                $.post(web_address+'ajax/load_cart',{
                    action:'min_prod',
                    rowid:rowid
                },function(data){
                    update_price2++;
                });
                $('#prod_'+row_id+' .title-subtotal').html(price_2_next);
                $('#prod_'+row_id+' .title-weight').text(number_format(prod_weight_current-prod_weight));
                $('.cart_listitem .title-weightall').text(number_format(prod_weight_all_current-prod_weight));
            }else{
                quatang.box.show('<i>Sản phẩm không thể giảm số lượng được nữa!</i>',0,0,0,0,2);
            }
            
        }else{
            quatang.box.show('<i>Lỗi trong quá trình xử lý. Bạn vui lòng kiểm tra lại!</i>',0,0,0,0,2);
        }
    });

    $('.updatecart_quantity').change(function() {
        var row_id = $(this).parent().attr('rel-data-product');
        var num_cur = $(this).val();
        var price_id = $('#price_'+row_id).val();
        var price_next = num_cur*price_id;
        $("#total_"+row_id ).html(number_format(price_next));
        var qty_old = $("#qty_"+row_id ).val();
        var prod_qty = (qty_old>num_cur)?qty_old-num_cur:num_cur-qty_old;

        var rowid       = $('#prod_'+row_id).attr('rel-data-product');
        var prod_model  = $('#prod_'+row_id+' .model').val();
        var prod_id     = $('#prod_'+row_id).attr('rel-data-prod-id');
        if(qty_old>num_cur){type_action = 'min_prod';}else{type_action = 'plus_prod';}

        $.post(web_address+'ajax/load_cart',{
            action:type_action,
            rowid:rowid,
            prod_model:prod_model,
            prod_id:prod_id,
            prod_qty:prod_qty,
        },function(data){
            if(data != 1){ 
                $("#qty_"+row_id ).val(num_cur);
                var total = getallpricecart();
                $('.order-summary #order-subtotal').html(total);
                $('.order-summary #order-total').html(total);
            }else{
                quatang.box.show('<i>Sản phẩm không đủ số lượng!</i>',0,0,0,0,2);
            }  
        });
    });

    function getallpricecart(){
        var total=0;
        $('.content-cart table td.total').each(function() {
            price = price_format($(this).text());
            total += parseInt(price);
        });
        return number_format(total);
    };


    Click_Order = function(){
        return $(".cart_login").show(), $(".cart_regist").show(), $.scrollTo(".cart_login", 500), $("#tbxEmail").focus(), !1;
    };
    blockNotNumber = function (n) {
        var n = window.event || n;
        window.event ? (n.keyCode < 48 || n.keyCode > 57) && (n.returnValue = !1) : n.which != 8 && (n.which < 48 || n.which > 57) && n.preventDefault();
    };
    check_email = function (n) {
        return n.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1 ? !0 : !1;
    };
    box_login = function(){
        var box_login = $('.cart_login');
        var user_login = $('#user_login',box_login).val();
        var user_pass = $('#user_pass',box_login).val();
        if(!user_login){
            //alert('Vui lòng nhập Email, tên đăng nhập hoặc Số điện thoại đăng nhập');
            $('#user_login',box_login).focus();
        }else if(!user_pass){
            alert('Vui lòng nhập mật khẩu đăng nhập');
            $('#user_pass',box_login).focus();
        }else{
            $('.loginbtn a').attr('onclick','alert("Đang xử lý dữ liệu")');
            $('.wrap_cart .loading img').show();
            $.post(web_address+'ajax/login',{
                user_login:user_login,
                user_pass:user_pass
            },function(data){
                if(data == 1){
                    window.location.href = web_address+'thong-tin-van-chuyen'+url_prefix; 
                }else{
                    alert(data);
                    $('.loginbtn a').attr('onclick','box_login()');
                    $('.wrap_cart .loading img').hide();
                }
            });
        }
    };
    change_pass = function(){
        var user_pass_old = $.trim($('#user_pass_old').val());
        var user_pass_new = $.trim($('#user_pass_new').val());
        var user_pass_new_repeat = $.trim($('#user_pass_new_repeat').val());
        if(!user_pass_old){
            alert('Bạn chưa nhập mật khẩu cũ');
            $('#user_pass_old').focus();
        }else if(!user_pass_new){
            alert('Bạn chưa nhập mật khẩu mới');
            $('#user_pass_new').focus();
        }else if(user_pass_new.length < 6 || user_pass_new.length > 20){
            alert('Mật khẩu tối thiểu từ 6 đến 20 ký tự');
            $('#user_pass_new').focus();
        }else if(!user_pass_new_repeat){
            alert('Bạn chưa nhập mật khẩu mới');
            $('#user_pass_new_repeat').focus();
        }else if(user_pass_new != user_pass_new_repeat){
            alert('Mật khẩu không trùng nhau');
            $('#user_pass_new_repeat').focus();
        }else {
            $.post(web_address+'ajax/change_pass',{
                user_pass_old:user_pass_old,
                user_pass_new:user_pass_new,
                user_pass_new_repeat:user_pass_new_repeat,
            },function(data){
                if(data == 1){
                    alert("Thay đổi thành công");
                    window.location.href = web_address+'quan-ly'+url_prefix; 
                }else{
                    alert(data);
                }
            });
        }
    }
    change_info = function(type='default'){
        
        //var user_sex = $.trim($('#user_sex').val());
        var user_fullname = $.trim($('#user_fullname').val());
        var user_phone = $.trim($('#user_phone').val());
        var user_address = $.trim($('#user_address').val());
        var city = $.trim($('#city').val());
        var district = $.trim($('#district').val());
        var ward = $.trim($('#ward').val());

        //var birth_day = $.trim($('#birth_day').val());
        //var birth_month = $.trim($('#birth_month').val());
        //var birth_year = $.trim($('#birth_year').val());
        
        //alert(user_fullname+user_phone);
        
        if(!user_fullname){
            alert('Vui lòng nhập họ tên');
            $('#user_fullname').focus();
        }else if(user_phone == ''){
            alert('Vui lòng nhập số điện thoại');
            $('#user_phone').focus();
        }else if(!user_address){
            alert('Vui lòng nhập địa chỉ giao hàng');
            $('#user_address').focus();
        }else if(user_address.length < 10 || user_address.length > 100){
            alert('Địa chỉ tối thiểu từ 10 đến 100 ký tự');
            $('#user_address').focus();
        }else if(city == 0){
            alert('Vui lòng chọn tỉnh, thành phố');
            $('#city').focus();
        }else if(district == 0){
            alert('Vui lòng chọn quận, huyện');
            $('#district').focus();
        }else if(ward == 0){
            alert('Vui lòng chọn xã, phường');
            $('#ward').focus();  
        }else{
            $.post(web_address+'ajax/change_info',{
                user_city:city,
                user_district:district,
				user_ward:ward,
                user_phone:user_phone,
                user_address:user_address,
                user_fullname:user_fullname,
            },function(data){
                console.log(data);
                if(data == 1){
                    $(".change_info").notify(
                        "Cập nhật thành công",
                        { position:"right", className: "success"}
                    );
                    setTimeout(function () {
                        window.location.reload();
                    },500);
                    //alert("Cập nhật thông tin chi tiết thành công");
                   // window.location.href = web_address+'quan-ly'+url_prefix; 
                }else{
                    
                    $(".change_info").notify(
                      "Không thành công",
                      { position:"right", className: "error"}
                    );
                }

            });
        }
    }
    user_change_info = function(){
        var user_email = $.trim($('#user_email').val());
        var user_phone = $.trim($('#user_phone').val());
        var user_birthday = $.trim($('#user_birthday').val());
        var user_sex = $.trim($('input[id=user_sex]:checked').val());
        
        //alert($('input[id=user_sex]:checked').val());
        if(!user_email){
            alert('Vui lòng nhập email');
        }else if(!user_phone){
            alert('Vui lòng nhập số điện thoại');
        }else if(!user_birthday){
            alert('Vui lòng nhập ngày/tháng/năm sinh');
        }else{
            console.log(user_email+user_phone+user_birthday+user_sex);
            $.post(web_address+'ajax/user_change_info',{
                user_email:user_email,
                user_phone:user_phone,
                user_birthday:user_birthday,
                user_sex:user_sex,
            },function(data){
                console.log(data);
                if(data == 1){
                    $(".change_info").notify(
                        "Cập nhật thành công",
                        { position:"right", className: "success"}
                    );
                    setTimeout(function () {
                        window.location.reload();
                    },500);
                }else{
                    $(".change_info").notify(
                      "Không thành công",
                      { position:"right", className: "error"}
                    );
                }
            });
        }
    }
    box_register = function(){
        var box_register = $('.cart_regist');
        var user_fullname = $.trim($('#user_fullname',box_register).val());
        var user_name = $.trim($('#user_name',box_register).val());
        var user_email = $.trim($('#user_email',box_register).val());
        var user_pass = $.trim($('#user_pass',box_register).val());
        var user_pass_repeat = $.trim($('#user_pass_repeat',box_register).val());
        var user_sex = $.trim($('#user_sex',box_register).val());
        var birth_day = $.trim($('#birth_day',box_register).val());
        var birth_month = $.trim($('#birth_month',box_register).val());
        var birth_year = $.trim($('#birth_year',box_register).val());
        var city = $.trim($('#city',box_register).val());
        var district = $.trim($('#district',box_register).val());
        var user_address = $.trim($('#user_address',box_register).val());
        var user_phone = $.trim($('#user_phone',box_register).val());
        if(!user_fullname){
            alert('Vui lòng nhập họ tên của bạn');
            $('#user_fullname',box_register).focus();
        }else if(user_fullname.length < 10 || user_fullname.length > 100){
            alert('Họ tên tối thiểu từ 10 đến 100 ký tự');
            $('#user_fullname',box_register).focus();
        }else if(!user_name){
            alert('Vui lòng nhập tên đăng nhập');
            $('#user_name',box_register).focus();
        }else if(user_name.length < 6){
            alert('Họ tên tối thiểu 6 ký tự');
            $('#user_name',box_register).focus();
        }/*else if(!user_email){
            alert('Vui lòng nhập địa chỉ email của bạn');
            $('#user_email',box_register).focus();
        }else if(check_email(user_email) == !1){
            alert('Địa chỉ email không hợp lệ');
            $('#user_email',box_register).focus();
        }*/else if(!user_pass){
            alert('Vui lòng nhập mật khẩu đăng nhập');
            $('#user_pass',box_register).focus();
        }else if(user_pass.length < 6 || user_pass.length > 10){
            alert('Mật khẩu tối thiểu từ 6 đến 20 ký tự');
            $('#user_pass',box_register).focus();
        }else if(!user_pass_repeat){
            alert('Vui lòng nhập mật khẩu đăng nhập');
            $('#user_pass_repeat',box_register).focus();
        }else if(user_pass != user_pass_repeat){
            alert('Mật khẩu không trùng nhau');
            $('#user_pass_repeat',box_register).focus();
        }else if(user_phone.length < 8 || user_phone.length > 15){
            alert('Số điện thoại tối thiểu 8 đến 15 ký tự');
            $('#user_phone',box_register).focus();
        }else if(user_sex == 0){
            alert('Vui lòng chọn giới tính');
        }/*else if(birth_day == 0){
            alert('Vui lòng chọn ngày sinh');
        }else if(birth_month == 0){
            alert('Vui lòng chọn tháng sinh');
        }else if(birth_year == 0){
            alert('Vui lòng chọn năm sinh');
        }*/else if(city == 0){
            alert('Vui lòng chọn tỉnh, thành phố');
        }else if(district == 0){
            alert('Vui lòng chọn quận, huyện');
        }else if(!user_address){
            alert('Vui lòng nhập địa chỉ liên hệ');
            $('#user_address',box_register).focus();
        }else if(user_address.length < 10 || user_address.length > 100){
            alert('Địa chỉ tối thiểu từ 10 đến 100 ký tự');
            $('#user_address',box_register).focus();
        }else if(!user_phone){
            alert('Vui lòng nhập số điện thoại');
            $('#user_phone',box_register).focus();
        }else{
            $('.regist_btn a').attr('onclick','alert("Đang xử lý dữ liệu")');
            $('.wrap_cart .fb_loading img').show();
            $.post(web_address+'ajax/register',{
                user_name:user_name,
                user_fullname:user_fullname,
                user_email:user_email,
                user_pass:user_pass,
                user_pass_repeat:user_pass_repeat,
                user_sex:user_sex,
                birth_day:birth_day,
                birth_month:birth_month,
                birth_year:birth_year,
                user_city:city,
                user_district:district,
                user_address:user_address,
                user_phone:user_phone,
            },function(data){
                if(data == 1){
                    window.location.href = web_address+'thong-tin-van-chuyen'+url_prefix; 
                }else{
                    alert(data);
                    $('.regist_btn a').attr('onclick','box_register()');
                    $('.wrap_cart .fb_loading img').hide();
                }
            });
        }
    };
    display_none_loading = function(){
        $('.loadding_show_each_cat').css('display','none');
    };
    show_ajax_homepage = function(cat_id,type){
        //var type_actived = $('#show_cat_id_'+cat_id).attr('rel');
       // if(type != type_actived ){
            //$('.loadding_show_each_cat').css('display','block');
            $.post('/show_product_homepage',{
                cat_id:cat_id,
                type:type,
            },function(data){
                $('.tab-'+type+'-'+cat_id).html(data);
               // $('.loadding_show_each_cat').css('display','none');
                //alert(data);die;
                //$('#show_cat_id_'+cat_id).fadeIn().html(data);
                //$('#show_cat_id_'+cat_id).attr('rel',type);
            });
       // }
    };
    $('.right_title_box').each(function(){
        var this_parent = $(this);
        $('ul',this_parent).addClass('type_prod_new');
        $('ul li a',this_parent).hoverIntent(function(){
            if(!$(this).hasClass('current')){
                $('ul li a',this_parent).removeClass('current');
                $(this).addClass('current');
                var class_current = $(this).parent().attr('class');
                if($('ul',this_parent).attr('class') != 'type_'+class_current){
                    $('ul',this_parent).removeClass().addClass('type_'+class_current);	
                    /*var pageurl = $(this).attr('href');
				if(pageurl!=window.location){
			      	window.history.pushState({path:pageurl},'',pageurl);
			    }*/
                    var function_eval = $(this).attr('onclick');
                    eval(function_eval.replace('return false;',''));
                }   
            }	
        },function(){
        	
            });
    });
    get_review_ajax = function(get_detail_id){
        $.post(web_address+'order-send-review',{
            get_detail_id:get_detail_id
        }, function(data){
            $('.description').html(data);
        });
    };
    order_send_review = function(detail_id){
        location.hash = 'order-send-review';
        setTimeout("get_review_ajax('"+detail_id+"')",100);
    };
    show_cart_animate = function(){
        get_rel = $('.slide_bottom .box_left').attr('rel');
        if(get_rel == 'no-active' ){
            item_product = $('.text_cart').attr('rel');
            var top_body = parseInt(item_product)*80;
            if(top_body >= 476){
                top_body = 426;
            }
            top_body = top_body+126;
            var height_body = top_body+16;
            $.post(web_address+'ajax/load_cart',{
                action: 'show_cart'
            }, function(data){
                $('.slide_bottom .box_left').attr('rel','active');
                $('.slide_bottom .box_body')
                .css({
                    "height":top_body+"px"
                })
                .animate({
                    top: "-"+height_body+"px"
                },500,function(){
                    $('.slide_bottom .box_body').html(data);
                });
            });
        }else{
            $('.slide_bottom .box_left').attr('rel','no-active');
            $('.slide_bottom .box_body').animate({
                'top': '0'
            },500,function(){
                $('.slide_bottom .box_left').animate({
                    'top': '0px'
                },500);
            });
        }
    };

    get_productcart = function(){ 
        $.post('/ajax/load_cart',{
            action: 'show_cart'
        }, function(data){
            $('.dropdown-cart').html(data);
        });  
    };

    // $('.header-cart .dropdown-toggle').click(function(){
    //     get_productcart();
    // });
    

    check_model_add = function(prod_id){
        var height_thick_box = Math.ceil($('#inline_model_add .show_item_model').length/6);
        tb_show('Vui lòng chọn mẫu sản phẩm bên dưới','#TB_inline?height='+((height_thick_box*165)+54)+'&width=830&inlineId=inline_model_add&modal=false');
    }
    check_model_buy = function(prod_id){
        var height_thick_box = Math.ceil($('#inline_model_buy .show_item_model').length/6);
        tb_show('Vui lòng chọn mẫu sản phẩm bên dưới','#TB_inline?height='+((height_thick_box*165)+54)+'&width=830&inlineId=inline_model_buy&modal=false');
    }
	
    $('.inline_model_add .show_item_model.uncheck .onclick, .inline_model_add .show_item_model.uncheck .select_item_model').click(function(){
        complete++;
        var images_id = $(this).attr('rel');
        var this_current = $('.inline_model_add #model_id_'+images_id +'');
        var that_current = $('.inline_model_add .show_item_model');
        if(complete == 1){
            that_current.removeClass('checked').addClass('uncheck');
            this_current.removeClass('uncheck').addClass('checked');
			
            $('input:checkbox',that_current).removeAttr('checked');
            $('input:checkbox',this_current).attr('checked',true);
			
            that_current.find('.select_item_model').css('display','none');
            this_current.find('.select_item_model').fadeIn();
			
            complete = 0;
        }
    });
    $('.inline_model_buy .show_item_model.uncheck .onclick, .inline_model_buy .show_item_model.uncheck .select_item_model').click(function(){
        complete++;
        var images_id = $(this).attr('rel');
        var this_current = $('.inline_model_buy #model_id_'+images_id +'');
        var that_current = $('.inline_model_buy .show_item_model');
        if(complete == 1){
            that_current.removeClass('checked').addClass('uncheck');
            this_current.removeClass('uncheck').addClass('checked');
			
            $('input:checkbox',that_current).removeAttr('checked');
            $('input:checkbox',this_current).attr('checked',true);
			
            that_current.find('.select_item_model').css('display','none');
            this_current.find('.select_item_model').fadeIn();
			
            complete = 0;
        }
    });
    $('.buttton_buy_model').click(function(){
        var prod_id  = $(this).attr('rel');
        var ajax_post = 0;
        var completed = 0;
        var images_id = $('.inline_model_buy input:checkbox:checked[name="check_box_select_model[]"]').val();
        if(images_id){
            if(check_user){
                window.location.href = web_address+'mua-hang-nhanh/p'+prod_id+'m'+images_id+url_prefix;
            }else{
                tb_remove();
                setTimeout("tb_show_inline(web_address+'ajax/box_login?buy_product="+prod_id+"&KeepThis=true&height=288&width=482')",300);
            }
        }else{
            if(confirm('Bạn vui lòng chọn mẫu sản phẩm cần mua!\r\nBạn muốn đóng bảng chọn mẫu??')){
                tb_remove();
            }
        }
    });
    $('.buttton_add_cart_model').click(function(){
        var prod_id  = $(this).attr('rel');
        var ajax_post = 0;
        var completed = 0;
        var images_id = $('.inline_model_add input:checkbox:checked[name="check_box_select_model[]"]').val();
        if(images_id){
            this_item_model = $('.inline_model_add #model_id_'+images_id);
            var prod_name = $('.show_info_view .title_prod a').html();
            var prod_price = $('p',this_item_model).html();
            var prod_qty = $('.update_prod_quantity',this_item_model).val();
            var prod_thumb = $('img',this_item_model).attr('src');
            var prod_url = $('.show_info_view .title_prod a').attr('href');
            var prod_model = $('img',this_item_model).attr('alt');
            var prod_weight = $('.show_info_view .prod_weight').text();
            $.post(web_address+'ajax/load_cart',{ 
                action:"add_prod",
                prod_model:prod_model,
                prod_weight:prod_weight,
                prod_qty:prod_qty,
                images_id:images_id,
                prod_id:prod_id,
                prod_name:prod_name,
                prod_price:prod_price,
                prod_thumb:prod_thumb,
                prod_url:prod_url
            },function(data){
                ajax_post++;
                if(data == 1){
                    quatang.box.show('<i>Sản phẩm không đủ số lượng!</i>',0,0,0,0,2);
                }else{
                    get_data = data.split('|||||');
                    prod_in_cart = get_data[0];
                    total_prod_in_cart = get_data[2];
                    total_price_prod_in_cart = get_data[1];
                    $('.text_cart span').html(prod_in_cart);
                    $('.text_cart').attr('rel',total_prod_in_cart);
                    $('.number_price').html(total_price_prod_in_cart);
                    $('.slide_bottom .box_left').attr("rel",'no-active');
                    $('.slide_bottom .box_body').attr("data-prod-cart",prod_in_cart);
                    $('.inline_model_add #model_id_'+images_id+' .select_item_model').attr('onclick','check_box_select_model('+images_id+',"inline_model_add")');
                    $('.inline_model_add #model_id_'+images_id+' .onclick').attr('onclick','check_box_select_model('+images_id+',"inline_model_add")');
                    $('.slide_bottom .box_body').animate({
                        'top': '-10px'
                    },500,function(){
                        show_cart_animate();
                        quatang.box.show('<i>Thêm sản phẩm vào giỏ hàng thành công!</i>',0,0,0,0,2);
                        tb_remove();
                        $('.select_item_model').css('display','none');
                        $('.inline_model.inline_model_add input:checkbox:checked[name="check_box_select_model[]"]').removeAttr('checked');
                    });
                }
            }
            );
        }else{
            if(confirm('Bạn vui lòng chọn mẫu sản phẩm cần mua!\r\nBạn muốn đóng bảng chọn mẫu??')){
                tb_remove();
            }
        }
    });
	
    $('.change_info_input input').keypress(function(e){
        var keycode;
        if(window.e) keycode = window.e.keyCode;
        else if (e) keycode = e.which;
        else return true;
        if(keycode == 13){
            change_info();
        }
    });
    $('.cart_address input').keypress(function(e){
        var keycode;
        if(window.e) keycode = window.e.keyCode;
        else if (e) keycode = e.which;
        else return true;
        if(keycode == 13){
            view_order();
        }
    });
    get_ship_from_city = function(city,district,val_ship){
        var total_weight = $('.title-weightall').attr('rel');
        var show_price_all = price_format($('#show_price_all').attr('rel'));
        $.post("?get_ship_from_city=true",{
            city:city, 
            district:district, 
            val_ship:val_ship, 
            prod_weight:total_weight
        },function(data){
			
            if(val_ship == 4){
                var array_data = data.split('||');
                data = array_data[0];
                data_cod = array_data[1];

                $('#ship_cod').css('display','inline-block');
                show_price_all = show_price_all+parseInt(data)+parseInt(data_cod);
                $('#show_price_all').html(number_format(show_price_all));
                $('[name=customer_ship_price]').val(data);
                $('[name=customer_ship]').val(val_ship);
                var data = number_format(data);
                $('#show_price_ship').html(data);
                $('[name=customer_price_cod]').val(data_cod);
                $('[name=customer_price_all]').val(show_price_all);
                $('#ship_cod_price').html(number_format(data_cod));
            }else{

                $('#ship_cod').css('display','none');
                show_price_all = show_price_all+parseInt(data);
                $('#show_price_all').html(number_format(show_price_all));
                $('[name=customer_ship_price]').val(data);
                $('[name=customer_ship]').val(val_ship);
                $('[name=customer_price_cod]').val(0);
                $('[name=customer_price_all]').val(show_price_all);
                var data = number_format(data);
                $('#show_price_ship').html(data);
                console.log(data);
            }
            $('[name=customer_ship_price_add]').val(price_format($('[name=customer_price_cod]').val())+price_format($('[name=customer_ship_price]').val()));
        });
    };
    finish_order = function(){
        $('#form-hidden-step5').submit();
    };
    view_order = function(){
        var customer_name = $('[name="customer_name"]').val();
        var customer_phone = $('[name="customer_phone"]').val();
        var customer_city = $('[name="city"]').val();
        var customer_district = $('[name="district"]').val();
		var customer_ward = $('[name="ward"]').val();
        var customer_address = $('[name="customer_address"]').val();
        var customer_note = $('[name="customer_note"]').val();
        var word = $('[name="word"]').val();
        if(!customer_name){
            alert('Vui lòng nhập họ tên người nhận hàng');
            $('[name="customer_name"]').focus();
            $.scrollTo('[name="customer_name"]', 500);
        }else if(customer_name.length < 10 || customer_name.length > 100){
            alert('Họ tên người nhận hàng tối thiểu từ 10 đến 100 ký tự');
            $('[name="customer_name"]').focus();
            $.scrollTo('[name="customer_name"]', 500);
        }else if(!customer_phone){
            alert('Vui lòng nhập số điện thoại liên lạc');
            $('[name="customer_phone"]').focus();
            $.scrollTo('[name="customer_phone"]', 500);
        }else if(customer_phone.length < 8 || customer_phone.length > 15){
            alert('Số điện thoại liên lạc tối thiểu 8 đến 15 ký tự');
            $('[name="customer_phone"]').focus();
            $.scrollTo('[name="customer_phone"]', 500);
        }else if(customer_city == 0){
            alert('Vui lòng chọn tỉnh, thành phố');
        }else if(customer_district == 0){
            alert('Vui lòng chọn quận, huyện');
        }else if(customer_ward == 0){
            alert('Vui lòng chọn phường xã');
        }else if(!customer_address){
            alert('Vui lòng nhập địa chỉ liên hệ');
            $('[name="customer_address"]').focus();
            $.scrollTo('[name="customer_address"]', 500);
        }else{
            $('#form-hidden-step3').submit();
        }
    };
    submit_order = function(){
        var customer_pay = $('input:radio[name="custom_pay"]:checked').val();
        if(customer_pay == 1)
            $('#form-hidden-step4').submit();
        else if(customer_pay == 2){
            showLoading(null,'Đang khởi tạo đơn hàng! Vui lòng không tắt trình duyệt');
            $.post(web_address+'ajax/customer_pay_NL',$('#form-hidden-step4').serialize(),function(data){
                if(data == 1){
                    alert('Bạn đã hết lượt truy cập. Vui lòng đăng nhập lại');
                }else if(data == 2){
                    alert('Hiện tại sản phẩm trong giỏ hàng của bạn là rỗng');
                }else if(!data ){
                    alert('Hệ thống lỗi. Vui lòng thử lại sau');
                } else{
                    setTimeout(function(){
                        window.location.href = data;
                    }, 3000);   
                }
                
            });
        }else if(customer_pay ==3){
            showLoading(null,'Đang khởi tạo đơn hàng! Vui lòng không tắt trình duyệt');
            $.post(web_address+'ajax/customer_pay_BK',$('#form-hidden-step4').serialize(),function(data){
                if(data == 1){
                    alert('Bạn đã hết lượt truy cập. Vui lòng đăng nhập lại');
                }else if(data == 2){
                    alert('Hiện tại sản phẩm trong giỏ hàng của bạn là rỗng');
                }else if(!data ){
                    alert('Hệ thống lỗi. Vui lòng thử lại sau');
                } else{
                    setTimeout(function(){
                        window.location.href = data;
                    }, 3000);   
                }
                
            });
        }
    }
    add_to_cart = function(prod_id, type, event){
        console.log('123');return false;
        event.preventDefault();
        complete++;
        if(complete == 1){
            //var imageX    =  $(".images_small_cloud a").offset().left;
            //var imageY    =  $(".images_small_cloud a").offset().top;
            //var basketX   = $(".slide_bottom").offset().left + 20;
            //var basketY   = $(".slide_bottom").offset().top - 50;
            //var gotoX 	= 10;//basketX - imageX;
            //var gotoY 	= 20;//basketY - imageY;   
            
           // var ImageWidth    =  $(".images_small_cloud a").width();
            //var ImageHeight    =  $(".images_small_cloud a").height();
            //var newImageWidth 	= ImageWidth / 7;
            //var newImageHeight	= ImageHeight / 7;
            var prod_price = $('.show_info_view .prod_price .price_value').html();
            console.log(prod_price, type);
            
            if(prod_price != 'Liên hệ'){
                var prod_name   = $('.show_info_view .title_prod a').html();
                var prod_qty    = $('.show_info_view .update_prod_quantity').val();
                var prod_thumb  = $(".images_small_cloud").val();
                var prod_url    = $('.show_info_view .title_prod a').attr('href');
                var prod_weight = $('.show_info_view .prod_weight').text();
                $.post('/ajax/load_cart',{ 
                    action:"add_prod",
                    prod_model:'Mặc định',
                    images_id:0,
                    prod_weight:prod_weight,
                    prod_qty:prod_qty,
                    prod_id:prod_id,
                    prod_name:prod_name,
                    prod_price:prod_price,
                    prod_thumb:prod_thumb,
                    prod_url:prod_url 
                },function(data){
                    complete=0;
                   // alert('co');
                    
                   // console.log(data);
                    //$('.header-cart .circle').html();

                    //get_productcart();
                    //addtocartdropdown();
                    if(data == 1){
                        quatang.box.show('<i>Sản phẩm không đủ số lượng!</i>',0,0,0,0,2);
                    //alert('Sản phẩm không đủ số lượng');
                    }else{
                        if(type != 'buy'){
                            setTimeout(get_productcart(), 500);
                            setTimeout(showincartdropdown(), 2000);
                            get_data = data.split('|||||');
                            prod_in_cart = get_data[0];
                            total_prod_in_cart = get_data[2];
                            total_price_prod_in_cart = get_data[1];
                            //$('.text_cart span').html(prod_in_cart);
                            $('.header-cart .circle').html(total_prod_in_cart);
                            //$('.number_price').html(total_price_prod_in_cart);
                            //$('.slide_bottom .box_left').attr("rel",'no-active');
                            //$('.slide_bottom .box_body').attr("data-prod-cart",prod_in_cart);
                           
                            //$(this).remove();

                           
                        }else{
                            // window.location.href = web_address+'gio-hang'+url_prefix;
                        }
                    }
                }
                );
            }else{
                quatang.box.show('<i>Sản phẩm chưa có giá!</i>',0,0,0,0,2);
                complete=0;
            }
        }else{
            quatang.box.show('<i>Dữ liệu đang được xử lý!</i>',0,0,0,0,2);
        }
        return false;
    };

    function showincartdropdown(){
        var windowsize = $(window).width();
        if (windowsize <= 768) { 
            $('.header-main .header-cart .dropdown-toggle').dropdown('toggle');
        }else{
            if($(".header-main").hasClass("on-scroll")) {
                $('.header-scroll .header-cart .dropdown-toggle').dropdown('toggle');            
            }else{
                $('.header-main .header-cart .dropdown-toggle').dropdown('toggle');
            }
        } 
    }

    $('.images_small_list li a img:first').css('border','1px solid red');
    $('.images_small_list li a').click(function(){
        var images_show_large = $(this).attr('data-image-large');
        $('.button_zoom_image').attr('href',images_show_large)
        $('.images_small_list li a img').css('border','1px solid #ccc');
        $('img',this).css('border','1px solid red');
    });
    $(".wrap_cart .info_box_content input[placeholder]").each(function(){
        $(this).attr('title',$(this).attr('placeholder'));
    });
    $(".wrap_cart .info_box_content input[title], .wrap_cart .info_box_content select[title]").tooltip({
        position: "center right",
        offset: [-2, 10],
        effect: "fade",
        opacity: 0.7,
        tipClass: "tooltip_input",
        events: {
            widget: "click,blur",
            input:   "click,blur",

        }
    }).dynamic({
        position: "center left"
    });
	
	
    load_info_cat = function(){
        if (!msie6) {
            if($('.filter_box').length){
				
                var height_of_current_box = $('.filter_box').height();
                var top = $('.filter_box').offset().top - parseFloat($('.filter_box').css('margin-top').replace(/auto/, 0));
                var stop_load_bar = $('#stop_load_bar').offset().top - parseFloat($('#stop_load_bar').css('margin-top').replace(/auto/, 0));	
                var x = $(this).scrollTop();
                if(x >= top){
                    $('.filter_box').addClass('fixed');
                    if(x >= stop_load_bar - height_of_current_box){
                        $('.filter_box').removeClass('fixed');
                    }
                }
                $(window).scroll(function (event) {
                    var y = $(this).scrollTop();
                    if (y >= top) {
                        $('.filter_box').addClass('fixed');
                        if(y >= stop_load_bar - height_of_current_box){
                            $('.filter_box').removeClass('fixed');
                        }
                    }else{
                        $('.filter_box').removeClass('fixed');
                    }
                });
            }
        }
    }
    load_box_relate_right = function(){
        if (!msie6) {
            if($('#view_product .view_right .box_relate_right').length){
                var height_of_current = $('#view_product .view_left').height()-40;
                var height_of_current_box = $('#view_product .view_right .box_relate_right').height()-40;
                $('#view_product .view_right').css('height',height_of_current);
                var top = $('#view_product .view_right .box_relate_right').offset().top - parseFloat($('#view_product .view_right .box_relate_right').css('margin-top').replace(/auto/, 0));
                var view_relate = $('#view_relate').offset().top - parseFloat($('#view_relate').css('margin-top').replace(/auto/, 0));
                var x = $(this).scrollTop();
                if(x >= top){
                    $('#view_product .view_right .box_relate_right').addClass('fixed');
                    $('#view_product .view_right .box_relate_right').removeClass('absolute');
                    if(x >= view_relate - height_of_current_box){
                        $('#view_product .view_right .box_relate_right').removeClass('fixed');
                        $('#view_product .view_right .box_relate_right').addClass('absolute');
                    }
                }
                $(window).scroll(function (event) {
                    var y = $(this).scrollTop();
                    if (y >= top) {
                        $('#view_product .view_right .box_relate_right').addClass('fixed');
                        $('#view_product .view_right .box_relate_right').removeClass('absolute');
                        if(y >= view_relate - height_of_current_box){
                            $('#view_product .view_right .box_relate_right').removeClass('fixed');
                            $('#view_product .view_right .box_relate_right').addClass('absolute');
                        }
                    }else{
                        $('#view_product .view_right .box_relate_right').removeClass('fixed');
                        $('#view_product .view_right .box_relate_right').removeClass('absolute');
                    }
                });
            }
        }
    };
    load_sideview_top  = function(){
        if (!msie6) {
            if($('#sidebar_top_view .tuan_detail_tabs').length){
                var top = $('#sidebar_top_view .tuan_detail_tabs').offset().top - parseFloat($('#sidebar_top_view .tuan_detail_tabs').css('margin-top').replace(/auto/, 0));
                var prod_spec = $('#prod_spec').offset().top - parseFloat($('#prod_spec').css('margin-top').replace(/auto/, 0));
                var prod_desc = $('#prod_desc').offset().top - parseFloat($('#prod_desc').css('margin-top').replace(/auto/, 0));
                var faq = $('#faq').offset().top - parseFloat($('#faq').css('margin-top').replace(/auto/, 0));
                var comment = $('#comment').offset().top - parseFloat($('#comment').css('margin-top').replace(/auto/, 0));
                var report = $('#report').offset().top - parseFloat($('#report').css('margin-top').replace(/auto/, 0));
                var guide = $('#guide').offset().top - parseFloat($('#guide').css('margin-top').replace(/auto/, 0));
                var view_relate = $('#view_relate').offset().top - parseFloat($('#view_relate').css('margin-top').replace(/auto/, 0));
                var address = $('#address').offset().top - parseFloat($('#address').css('margin-top').replace(/auto/, 0));
                var x = $(this).scrollTop();
                if(x >= top){
                    $('#sidebar_top_view .tuan_detail_tabs').addClass('fixed');
                    if(x >= prod_spec){
                        $('.tuan_detail_tabs li a').removeClass('active');
                        $('.tuan_detail_tabs li a.nav_1').addClass('active');
                    }
                    if(x >= prod_desc){
                        $('.tuan_detail_tabs li a').removeClass('active');
                        $('.tuan_detail_tabs li a.nav_2').addClass('active');
                    }
                    if(x >= faq){
                        $('.tuan_detail_tabs li a').removeClass('active');
                        $('.tuan_detail_tabs li a.nav_3').addClass('active');
                    }
                    if(x >= comment){
                        $('.tuan_detail_tabs li a').removeClass('active');
                        $('.tuan_detail_tabs li a.nav_4').addClass('active');
                    }
                    if(x >= report){
                        $('.tuan_detail_tabs li a').removeClass('active');
                        $('.tuan_detail_tabs li a.nav_5').addClass('active');
                    }
                    if(x >= guide){
                        $('.tuan_detail_tabs li a').removeClass('active');
                        $('.tuan_detail_tabs li a.nav_6').addClass('active');
                    }
                    if(x >= address){
                        $('.tuan_detail_tabs li a').removeClass('active');
                        $('.tuan_detail_tabs li a.nav_7').addClass('active');
                    }
                    if(x >= view_relate){
                        $('#sidebar_top_view .tuan_detail_tabs').removeClass('fixed');
                    }
                }
                $(window).scroll(function (event) {
                    var y = $(this).scrollTop();
				  	
                    if (y >= top) {
                        $('#sidebar_top_view .tuan_detail_tabs').addClass('fixed');	
                        if(y >= prod_spec){
                            $('.tuan_detail_tabs li a').removeClass('active');
                            $('.tuan_detail_tabs li a.nav_1').addClass('active');
                        }
                        if(y >= prod_desc){
                            $('.tuan_detail_tabs li a').removeClass('active');
                            $('.tuan_detail_tabs li a.nav_2').addClass('active');
                        }
                        if(y >= faq){
                            $('.tuan_detail_tabs li a').removeClass('active');
                            $('.tuan_detail_tabs li a.nav_3').addClass('active');
                        }
                        if(y >= comment){
                            $('.tuan_detail_tabs li a').removeClass('active');
                            $('.tuan_detail_tabs li a.nav_4').addClass('active');
                        }
                        if(y >= report){
                            $('.tuan_detail_tabs li a').removeClass('active');
                            $('.tuan_detail_tabs li a.nav_5').addClass('active');
                        }
                        if(y >= guide){
                            $('.tuan_detail_tabs li a').removeClass('active');
                            $('.tuan_detail_tabs li a.nav_6').addClass('active');
                        }
                        if(y >= address){
                            $('.tuan_detail_tabs li a').removeClass('active');
                            $('.tuan_detail_tabs li a.nav_7').addClass('active');
                        }
                        if(y >= view_relate){
                            $('#sidebar_top_view .tuan_detail_tabs').removeClass('fixed');
                        }
						
                    } else {
                        $('#sidebar_top_view .tuan_detail_tabs').removeClass('fixed');
                    }
                });
            }
        } 
    };

    load_sideview_left  = function(){
        if (!msie6) {
            if($('#div_sidebar_left').length){
                var top = $('#div_sidebar_left').offset().top - parseFloat($('#div_sidebar_left').css('margin-top').replace(/auto/, 0));
                var height_of_sidebar = $("#div_sidebar_left").height();
                var prod_spec = $('#prod_spec').offset().top - parseFloat($('#prod_spec').css('margin-top').replace(/auto/, 0));
                var prod_desc = $('#prod_desc').offset().top - parseFloat($('#prod_desc').css('margin-top').replace(/auto/, 0));
                var faq = $('#faq').offset().top - parseFloat($('#faq').css('margin-top').replace(/auto/, 0));
                var comment = $('#comment').offset().top - parseFloat($('#comment').css('margin-top').replace(/auto/, 0));
                var report = $('#report').offset().top - parseFloat($('#report').css('margin-top').replace(/auto/, 0));
                var guide = $('#guide').offset().top - parseFloat($('#guide').css('margin-top').replace(/auto/, 0));
                var view_relate = $('#view_relate').offset().top - parseFloat($('#view_relate').css('margin-top').replace(/auto/, 0));
                var view_product = $('#view_product').offset().top - parseFloat($('#view_product').css('margin-top').replace(/auto/, 0));
                var address = $('#address').offset().top - parseFloat($('#address').css('margin-top').replace(/auto/, 0));
                if($(this).scrollTop() >= view_product){
                    if($(this).scrollTop() >= view_product){
                        $('.mod_category_nav li a').removeClass('active');
                        $('.mod_category_nav li.nav_a_1 a').addClass('active');
                    }
                    if($(this).scrollTop() >= prod_spec){
                        $('.mod_category_nav li a').removeClass('active');
                        $('.mod_category_nav li.nav_a_2 a').addClass('active');
                    }
                    if($(this).scrollTop() >= prod_desc){
                        $('.mod_category_nav li a').removeClass('active');
                        $('.mod_category_nav li.nav_a_3 a').addClass('active');
                    }
                    if($(this).scrollTop() >= faq){
                        $('.mod_category_nav li a').removeClass('active');
                        $('.mod_category_nav li.nav_a_4 a').addClass('active');
                    }
                    if($(this).scrollTop() >= comment){
                        $('.mod_category_nav li a').removeClass('active');
                        $('.mod_category_nav li.nav_a_5 a').addClass('active');
                    }
                    if($(this).scrollTop() >= report){
                        $('.mod_category_nav li a').removeClass('active');
                        $('.mod_category_nav li.nav_a_6 a').addClass('active');
                    }
                    if($(this).scrollTop() >= guide){
                        $('.mod_category_nav li a').removeClass('active');
                        $('.mod_category_nav li.nav_a_7 a').addClass('active');
                    }
                    if($(this).scrollTop() >= address){
                        $('.mod_category_nav li a').removeClass('active');
                        $('.mod_category_nav li.nav_a_8 a').addClass('active');
                    }
                    if($(this).scrollTop() >= view_relate){
                        $('.mod_category_nav li a').removeClass('active');
                        $('.mod_category_nav li.nav_a_9 a').addClass('active');
                    }
                    $('#div_sidebar_left').addClass('fixed');
                }
                $(window).scroll(function (event) {
                    var y = $(this).scrollTop();
                    if (y >= top) {
                        if(y >= view_product){
                            $('.mod_category_nav li a').removeClass('active');
                            $('.mod_category_nav li.nav_a_1 a').addClass('active');
                        }
                        if(y >= prod_spec){
                            $('.mod_category_nav li a').removeClass('active');
                            $('.mod_category_nav li.nav_a_2 a').addClass('active');
                        }
                        if(y >= prod_desc){
                            $('.mod_category_nav li a').removeClass('active');
                            $('.mod_category_nav li.nav_a_3 a').addClass('active');
                        }
                        if(y >= faq){
                            $('.mod_category_nav li a').removeClass('active');
                            $('.mod_category_nav li.nav_a_4 a').addClass('active');
                        }
                        if(y >= comment){
                            $('.mod_category_nav li a').removeClass('active');
                            $('.mod_category_nav li.nav_a_5 a').addClass('active');
                        }
                        if(y >= report){
                            $('.mod_category_nav li a').removeClass('active');
                            $('.mod_category_nav li.nav_a_6 a').addClass('active');
                        }
                        if(y >= guide){
                            $('.mod_category_nav li a').removeClass('active');
                            $('.mod_category_nav li.nav_a_7 a').addClass('active');
                        }
                        if(y >= address){
                            $('.mod_category_nav li a').removeClass('active');
                            $('.mod_category_nav li.nav_a_8 a').addClass('active');
                        }
                        if(y >= view_relate){
                            $('.mod_category_nav li a').removeClass('active');
                            $('.mod_category_nav li.nav_a_9 a').addClass('active');
                        }
                        $('#div_sidebar_left').addClass('fixed');	
						
                    } else {
                        $('#div_sidebar_left').removeClass('fixed');
                    }
                });
            }
        } 
    };


    //alert($('#statusInSite').val());
});

////////////////////facebook login
var api_fb = {};
var aid = '497683337366803';


$(window).on('load', function() {
    window.fbAsyncInit = function() {
        FB.init({
        appId   : aid,
        cookie  : true,
        status  : true,
        xfbml   : true,
        oauth   : true,
        version    : 'v2.8'
        });
        //FB.AppEvents.logPageView();
        // FB.getLoginStatus(function(response) {
        //     if(response.status === 'connected'){
        //         statusChangeCallback(response);
        //     }
        // });
    };

    (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    handleClientLoad();

});

function statusChangeCallback(response){
    if(response.status === 'connected'){
        api_fb.token    = response.authResponse.accessToken;
        var statusInSite = $('#statusInSite').val();
        FB.api('/me?fields=id,email,name', function(response) {
            //console.log(response);
            if(response && !response.error){
                api_fb.id    = response.id;
                api_fb.email = response.email;
                api_fb.name  = response.name;
                checkLoginFacebook(true,statusInSite);
            } 
        });

        // FB.api('/me?fields=name,email', function(response){
        //     if(response && !response.error){
        //         api_fb.id    = response.id;
        //         api_fb.email = response.email;
        //         api_fb.name  = response.name;
        //         checkLoginFacebook(true,statusInSite);
        //     } 
        // });
    }
}

function checkLoginFacebook(isLoggedIn,statusInSite='on'){
    
    if(isLoggedIn){
        if(statusInSite=='off'){
            $.post(web_address+'ajax/loginsocial',{
                user_login:api_fb.id,
                user_pass:api_fb.token,
                user_fullname:api_fb.name,
                user_email:api_fb.email,
                user_type:'facebook'
            },function(data){
                if(data == 1){ window.location.reload(true); }
                console.log(data);
            });
        }   
    }
}

function fb_login() {
    $('.icon-usser').html($('<img>',{src:template_dir +'assets/images/loading-m.gif'}));
    //jQuery.fbInit(aid);
    FB.login(function(response) {
        if (response.status === 'connected') {
            statusChangeCallback(response);
        }
    },{scope: 'public_profile,email'});
}

function logoutsocial(types){
    if(types==1){
        FB.getLoginStatus(function(response) {
            if (response.status === "connected") {
                FB.logout(function(response) {
                    delete api_fb.name;
                    delete api_fb.id;
                    delete api_fb.token;
                    delete api_fb.email;  
                });

            }
        });
        $.post(web_address+'ajax/logout',function(data){
            if(data==1){ window.location.reload(true); }
        });
    }else{
        gapi.auth2.getAuthInstance().signOut();
        $.post(web_address+'ajax/logout',function(data){
            if(data==1){
                window.location.reload(true);
            }
        });
    }
    
}

// ///////////////////////google login

function handleClientLoad() {
    gapi.load('client:auth2', initClient);
}
function initClient() {
    gapi.client.init({
        apiKey: 'AIzaSyACeWPy5TGzUJdd-aDip8-Nsukb3DXaSPo',
        clientId: '417799154148-u44fng11nufu13bvh4a8qat3hr16l631.apps.googleusercontent.com',
        scope: 'profile'
    }).then(function () {
        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
        updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
    });
}

function updateSigninStatus(isSignedIn) {
    if (isSignedIn) {
        checkLoginGoogle(isSignedIn);
    }
}
function gg_login() {
    $('.icon-usser').html($('<img>',{src:template_dir +'assets/images/loading-m.gif'}));
    gapi.auth2.getAuthInstance().signIn();
}
function checkLoginGoogle(isLoggedIn){
    var statusInSite = $('#statusInSite').val();
    if(isLoggedIn && statusInSite=='off'){
        var profile = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
        $.post(web_address+'ajax/loginsocial',{
            user_login:profile.getId(),
            user_pass:'',
            user_fullname:profile.getName(),
            user_email:profile.getEmail(),
            user_type:'google'
        },function(data){
            if(data==1){ window.location.reload(true); }
            else{ console.log(data) }
        });
    }
}