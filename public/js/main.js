$(document).ready(function () {
    function h(a, b, c) {
        $(a).children(b).addClass("collapse"), $(a).children(c).on("click", function () {
            $(this).parent(a).toggleClass("open").children(b).collapse("toggle"), $(this).parent(a).siblings().removeClass("open").children(c1).hasClass("in").collapse("hide")
        })
    }
    var a = $(window).width(),
        c = $(location).attr("href"),
        d = c.indexOf("?"),
        e = c.substring(d + 1, d + 6);
    if (d != -1 && "route" != e) {
        var f = c.substring(0, d);
        $("[href]").each(function () {
            this.href == f && $(this).addClass("active")
        })
    } else $("[href]").each(function () {
        this.href == window.location.href && ($(this).addClass("active"), $(this).parent().addClass("active"))
    });
    a < 1025 && ($("#search_box > i").click(function () {
        $("#search_box").toggleClass("opened")
    }), $(document).mouseup(function (a) {
        var b = $("#search_box");
    b.is(a.target) || 0 !== b.has(a.target).length || $("#search_box").removeClass("opened")
    })), $(".footer-social .item-content a").attr("target", "_blank"), $(".slide_product .product-layout-custom").owlCarousel({
        navigation: !0,
        pagination: !1,
        items: 1,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [979, 1],
        itemsTablet: [767, 2],
        itemsMobile: [480, 1],
        navigationText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
        afterAction: function (a) {
            $(".slide_product .medium").removeClass("col-lg-4 col-md-4 col-sm-6 col-xs-12")
        }
    }), $(".list_product .medium").removeClass("col-lg-4 col-md-4 col-sm-6 col-xs-12"), a < 1025 && ($(".menu_horizontal .menu-content .menu ul").addClass("collapse"), $(".menu_horizontal .menu-content .menu").find("li").has("ul >li").children(".expander").on("click", function () {
        $(this).parent("li").toggleClass("open").children("ul").collapse("toggle"), $(this).parent("li").siblings().removeClass("open").children("ul.in").collapse("hide")
    })), a < 992 && $(".menu_horizontal .menu-content").addClass("collapse"), $(".menu_category .nav ul").addClass("collapse"), $(".menu_category .nav").find("li").has("ul").children(".icon_menu").on("click", function () {
        $(this).parent("li").toggleClass("open").children("ul").collapse("toggle"), $(this).parent("li").siblings().removeClass("open").children("ul.in").collapse("hide")
    }), "list" == localStorage.getItem("display") ? ($("#list-view").trigger("click"), $(".entry .list").addClass("active")) : ($("#grid-view").trigger("click"), $(".entry .grid").addClass("active")), $(".entry .grid").click(function () {
        $(".entry .list").removeClass("active"), $(this).addClass("active")
    }), $(".entry .list").click(function () {
        $(".entry .grid").removeClass("active"), $(this).addClass("active")
    }), cols = $("#column-right, #column-left").length, "list" == localStorage.getItem("display") ? ($("#list-view").trigger("click"), $(".entry .list").addClass("active")) : ($("#grid-view").trigger("click"), $(".entry .grid").addClass("active")), $(".entry .grid").click(function () {
        $(".entry .list").removeClass("active"), $(this).addClass("active")
    }), $(".entry .list").click(function () {
        $(".entry .grid").removeClass("active"), $(this).addClass("active")
    }), a < 481 && (h(".info_store", ".content_inner", ".title"), h(".footer-top .vertical-name", ".menu-content", ".title"), h(".newsletter-box", ".newsletter-box-content", ".title")), 
        $.scrollUp({
        scrollText: '<i class="fa fa-angle-up"></i>',
        easingType: "linear",
        scrollSpeed: 900,
        animation: "fade"
    })
}), jQuery, window.jQuery;
