show_ajax_homepage = function(cat_id,type){
    $.post('/show_product_homepage',{
        cat_id:cat_id,
        type:type,
    },function(data){
        $('.tab-'+type+'-'+cat_id).html(data);
    });
};

var cart = {
    add: function (type) {
        $.ajax({
            url: `/add-cart`,
            type: 'post',
            data: $(
                '#product select[name="quantity"], #product input[type="hidden"]'
            ),
            dataType: 'json',
            beforeSend: function () {
                $('#button-cart').button('loading');
            },
            complete: function () {
                $('#button-cart').button('reset');
            },
            success: function (json) {
                $('.form-group').removeClass('has-error');

                if (json.status) {
                    if (type) {
                        location.href = '/gio-hang';
                    } else {
                        let totalQuantity = json.data.reduce(function (total, itemObject) {
                            return (total + parseInt(itemObject.quantity))
                        }, 0);

                        $('.breadcrumb').after('<div class="container"><div class="alert alert-success">' + json.response + '<button type="button" class="close" data-dismiss="alert">&times;</button></div></div>');

                        setTimeout(function () {
                            $('.alert, .text-danger').remove();
                        }, 5000)

                        $('.header-cart .circle').html(totalQuantity);

                        $('html, body').animate({
                            scrollTop: 0
                        }, 'slow');

                        $('.header-cart ul').load('/cart-info ul li');
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                for (i in xhr.responseJSON.errors) {
                    var element = $('#input-option');

                    if (element.parent().hasClass('input-group')) {
                        element.parent().after('<div class="text-danger">' + xhr.responseJSON.errors[i] + '</div>');
                    } else {
                        $('.text-danger').parent().addClass('has-error');
                        element.after('<div class="text-danger">' + xhr.responseJSON.errors[i] + '</div>');
                    }
                }
            }
        });
    },
    update: function (key, $this) {
        $.ajax({
            url: `/cart-update`,
            type: "post",
            data: "key=" + key + "&quantity=" + $this.value,
            dataType: "json",
            beforeSend: function () {
                $("#cart > button").button("loading")
            },
            complete: function () {
                $("#cart > button").button("reset")
            },
            success: function (response) {
                // if (response.status == 202) {
                // setTimeout(function () {
                //     $('#cart > button').html('<i class="fa fa-shopping-cart"></i> <span class="num_product">' + response.totalQuantity + '</span>');
                // }, 100);

                (currentRoute == 'payment' || currentRoute == 'cart') ? window.location.reload(): $('.header-cart ul').load('/cart-info ul li');
                // }
            }
        })
    },
    remove: function (productId) {
        $.ajax({
            url: `/cart-remove`,
            type: "post",
            data: "productId=" + productId,
            dataType: "json",
            beforeSend: function () {
                $("#cart > button").button("loading")
            },
            complete: function () {
                $("#cart > button").button("reset")
            },
            success: function (response) {
                // setTimeout(function () {
                //     $('#cart > button').html('<i class="fa fa-shopping-cart"></i> <span class="num_product">' + response.totalQuantity + '</span>');
                // }, 100);
                $('.header-cart .circle').html(response.totalQuantity);

                (currentRoute == 'payment' || currentRoute == 'cart') ? window.location.reload(): $('.header-cart ul').load('/cart-info ul li');
            }
        })
    }
};

var locationSelection = {
    districtList: function ($this) {
        $.ajax({
            url: '/ajax/get-district-list',
            type: 'POST',
            data: {
                cityId: $this.value
            },
            dataType: 'json',
            beforeSend: function(){
                $('#district').prop('disabled', true);
            },
            success: function (response) {
                let options = '';
                response.map(function (district) {
                    options += `<option value="${district.maqh}">${district.name}</option>`;
                })
                $('#district').html(options);
                $('#district').prop('disabled', false);
                locationSelection.wardList($('#district')[0]);
            }
        })
    },
    wardList: function ($this) {
        $.ajax({
            url: '/ajax/get-ward-list',
            type: 'POST',
            data: {
                districtId: $this.value
            },
            dataType: 'json',
            beforeSend: function(){
                $('#ward').prop('disabled', true);
            },
            success: function (response) {
                let options = '';
                response.map(function (ward) {
                    options += `<option value="${ward.xaid}">${ward.name}</option>`;
                })
                $('#ward').prop('disabled', false);
                $('#ward').html(options);
            }
        })
    }
}
