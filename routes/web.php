<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

/* Views */
Route::get('/', 'HomeController@index')->name('home');
Route::post('post-subscription', 'HomeController@postSubscription');
Route::get(trans('routes.about'), 'HomeController@about')->name('about');
Route::get(trans('routes.contact'), 'HomeController@contact')->name('contact');
Route::post('post-contact', 'HomeController@postContact')->name('contact.post');

/* USER */
Route::group(['prefix' => 'user'], function () {
    Route::get('profile', 'Auth\UserController@showProfile')->name('user.profile');
    Route::post('update-profile', 'Auth\UserController@updateProfile')->name('user.update.profile');

    Route::get('address', 'Auth\UserController@showAddress')->name('user.address');
    Route::post('update-address', 'Auth\UserController@updateAddress')->name('user.update.address');

    Route::get('orders', 'Auth\UserController@showOrders')->name('user.orders');
    Route::get('orders-detail', 'Auth\UserController@ordersDetail')->name('user.orders.detail');

    Route::get('password-change', 'Auth\UserController@showPasswordChange')->name('user.password.change');
    Route::post('update-password-change', 'Auth\UserController@updatePasswordChange')->name('user.update.password.change');

    Route::get('password-forget', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('user.password.forget');
});

// Route::get(trans('routes.product') . "/xem-tat-ca", 'ProductController@list')->name('product.all'); //base on category
Route::get(trans('routes.product') . "/{categorySlug}", 'ProductController@list')->name('product.categorySlug'); //base on category
Route::get(trans('routes.product') . "/{categorySlug}/{id}", 'ProductController@detail')->name('product.categorySlug.id'); //base on category + product_id
Route::match(['get', 'post'], trans('routes.cart'), 'ProductController@cart')->name('cart');
Route::get(trans('routes.payment'), 'ProductController@payment')->name('payment');
Route::get(trans('routes.payment') . '-2', 'ProductController@payment2')->name('payment2');

/* AJAX */
Route::get("cart-info", 'ProductController@cartInfo');
Route::post("add-cart", 'ProductController@addCart')->name('addCart');
Route::post("cart-update", 'ProductController@cartUpdate');
Route::post("cart-remove", 'ProductController@cartRemove');
Route::post("show_product_homepage", 'AjaxController@showProductHomepage');
Route::post("change-order-status", 'AjaxController@changeOrderStatus');
Route::post('get-child-categories', 'AjaxController@getChildCategories');

/* PRODUCT */
Route::post('cart-checkout', 'ProductController@checkout')->name('checkout');
Route::post('cart-checkout-2', 'ProductController@checkout2')->name('checkout2');
Route::get("get-terms-and-policy", 'ProductController@termsAndPolicy');
Route::post("request-callback", 'ProductController@requestCallback')->name('request.callback');
Route::get('checkout-success', 'ProductController@checkoutSuccess')->name('checkout-success');
Route::get('quick-checkout-success', 'ProductController@quickCheckoutSuccess')->name('checkout-success.quick');

/* POSTS */
Route::get("huong-dan-va-tin-tuc", 'PostController@huongDanVaTinTuc')->name('post');
Route::get("danh-muc/{categorySlug}", 'PostController@baiVietTheoDanhMuc')->name('post.category');
Route::get("bai-viet/{slug}", 'PostController@baiViet')->name('post.detail');

/* Switch language */
Route::get("switch-language", 'HomeController@switchLanguage')->name('switchLanguage');

/* Search */
Route::match(['post', 'get'], trans("routes.search"), 'ProductController@search')->name('search');

/* AJAX */
Route::group(['prefix' => 'ajax'], function () {
    Route::post("get-district-list", 'AjaxController@getDistrictList');
    Route::post("get-ward-list", 'AjaxController@getWardList');
    Route::post("get-name", 'AjaxController@getName'); //city/district/ward

    Route::post('login', 'Auth\LoginController@login');
});

/* ADMIN */
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

/* GOOGLE */
Route::get('/redirect', 'Auth\LoginController@redirectToProvider');
Route::get('/callback', 'Auth\LoginController@handleProviderCallback');

/* NGAN LUONG */
Route::get('cancel', 'ProductController@cancel')->name('cancel');

// Route::get('test', function () {
//     //
// });
