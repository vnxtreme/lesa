<?php

return [
    //url section
    'about' => 'gioi-thieu',
    'contact' => 'lien-he',
    'product' => 'san-pham',
    'send-contact' => 'send-contact',
    'post-subscription' => 'post-subscription',
    'cart' => 'gio-hang',
    'payment' => 'thanh-toan',
    'search' => 'tim-kiem',
];