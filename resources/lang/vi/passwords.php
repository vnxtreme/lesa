<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Mật khẩu phải chứa ít nhất 6 ký tự và trùng với mật khẩu xác nhận.',
    'reset' => 'Mật khẩu đã được thay đổi!',
    'sent' => 'Email đổi mới mật khẩu đã được gửi tới email của bạn!',
    'token' => 'Email thay đổi mật khẩu không trùng khớp.',
    'user' => "Không tìm được email người dùng này.",

];
