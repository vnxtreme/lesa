<?php

return [
    'last_week' => 'Tuần trước',
    'last_year' => 'Năm ngoái',
    'this_week' => 'Tuần này',
    'this_year' => 'Năm nay',
];
