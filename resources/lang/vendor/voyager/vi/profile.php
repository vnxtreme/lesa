<?php

return [
    'avatar'           => 'Avatar',
    'edit'             => 'Sửa hồ sơ cá nhân',
    'edit_user'        => 'Edit User',
    'password'         => 'Mật khẩu',
    'password_hint'    => 'Để trống sẽ giữ nguyên',
    'role'             => 'Quyền',
    'roles'            => 'Quyền',
    'role_default'     => 'Quyền mặc định',
    'roles_additional' => 'Quyền bổ sung',
    'user_role'        => 'Quyền User',
];
