<?php

// DataTable translations from: https://github.com/DataTables/Plugins/tree/master/i18n
return [
    'sEmptyTable'     => 'Chưa có dữ liệu',
    'sInfo'           => 'Hiển thị từ _START_ đến _END_ trong tổng số _TOTAL_ mục',
    'sInfoEmpty'      => 'Hiển thị 0 to 0 of 0 mục',
    'sInfoFiltered'   => '(filtered from _MAX_ total entries)',
    'sInfoPostFix'    => '',
    'sInfoThousands'  => ',',
    'sLengthMenu'     => 'Hiển thị _MENU_ mục',
    'sLoadingRecords' => 'Đang tải...',
    'sProcessing'     => 'Đang xử lý...',
    'sSearch'         => 'Tìm kiếm:',
    'sZeroRecords'    => 'Không có kết quả trùng khớp',
    'oPaginate'       => [
        'sFirst'    => 'Đầu',
        'sLast'     => 'Cuối',
        'sNext'     => 'Tiếp theo',
        'sPrevious' => 'Trước đó',
    ],
    'oAria' => [
        'sSortAscending'  => ': activate to sort column ascending',
        'sSortDescending' => ': activate to sort column descending',
    ],
];
