<?php

return [
    'additional_fields'=> 'Additional Fields',
    'category'         => 'Post Category',
    'content'          => 'Post Content',
    'details'          => 'Post Details',
    'excerpt'          => 'Excerpt <small>Small description of this post</small>',
    'image'            => 'Post Image',
    'meta_description' => 'Meta Description',
    'meta_keywords'    => 'Meta Keywords',
    'new'              => 'Create New Post',
    'seo_content'      => 'SEO Content',
    'seo_title'        => 'SEO Title',
    'slug'             => 'URL slug',
    'status'           => 'Post Status',
    'status_draft'     => 'Bản nháp',
    'status_pending'   => 'Chờ duyệt',
    'status_published' => 'Đăng sản phẩm',
    'title'            => 'Post Title',
    'title_sub'        => 'The title for your post',
    'update'           => 'Update Post',
];
