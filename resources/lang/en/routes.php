<?php

return [
    //url section
    'about' => 'about-us',
    'contact' => 'contact-us',
    'product' => 'product',
    'payment' => 'payment',
    'search' => 'search',
];