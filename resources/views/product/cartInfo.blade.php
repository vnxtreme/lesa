<div id='cart' class='btn-group btn-block'>
    <button type='button' data-toggle='dropdown' data-loading-text='{{trans(' translation.processing') }}' class='btn btn-inverse btn-block btn-lg dropdown-toggle'>
        <i class='fa fa-shopping-cart'></i><span id='cart-total'><span class='num_product'>{{(123) }}</span> <span class='text-cart'>sản phẩm </span> <span class='price'>{{number_format($totalPrice, 0, ',' , '.' )}} VNĐ</span></span>
    </button>
    <ul class='dropdown-menu pull-right'>
        {{-- {$itemHtml} --}}
        <li>
            <div class='table-responsive nbd'>
                <table class='table tbl-total'>
                    <tr>
                        <td class='text-right'><strong>Thành tiền</strong></td>
                        <td class='text-right'>{{number_format($totalPrice, 0, ',' , '.' )}} VNĐ</td>
                    </tr>
                    <tr>
                        <td class='text-right'><strong>Tổng cộng </strong></td>
                        <td class='text-right'>{{number_format($totalPrice, 0, ',' , '.' ) }} VNĐ</td>
                    </tr>
                </table>
                <p class='btn-cart'>
                    <a class='vcart' href='{{url(app()->getLocale().'/'.trans('url.cart'))}}'><strong><i class='fa fa-shopping-cart'></i> Xem giỏ hàng</strong></a>
                    <a class='cout' href='{{url(app()->getLocale().'/'.trans('url.payment'))}}'> <strong><i class='fa fa-share'></i> Thanh toán</strong></a>
                </p>
            </div>
        </li>
    </ul>
</div>
