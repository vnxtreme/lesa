@extends('layouts.main')
@section('content')
<main>
    <div class="colum-one">
        <div class="container">

            <div class="content-full">
                <div class="box box-center">
                    <div class="box_header">
                        <h2 class="boxTitle"><i class="fa fa-check fa-lg text-success"></i> Đặt hàng thành công</h2>
                    </div>
                    <div class="box_body">
                        <div class="finish-box">
                            <h3 class="lineTitle">Kính gửi quý khách</h3>
                            <p></p>
                            <h5 style="padding: 0px; margin: 0px; border: 0px; font-size: 13px; font-family: Arial, Verdana, san-serif; vertical-align: baseline; color: rgb(0, 0, 0); line-height: 19.5px;">
                                &nbsp;</h5>
                            <h5 style="padding: 0px; margin: 0px; border: 0px; font-size: 13px; font-family: Arial, Verdana, san-serif; vertical-align: baseline; color: rgb(0, 0, 0); line-height: 19.5px;">
                                Đơn hàng của quý khách đã được gửi tới nhân viên của website {{setting('site.title')}}</h5>
                            <h2 style="padding: 0px; margin: 0px; border: 0px; font-size: 13px; font-family: Arial, Verdana, san-serif; vertical-align: baseline; color: rgb(0, 0, 0); line-height: 19.5px;">
                                Tổng đơn hàng của quý khách hết<span style="font-family:lucida sans unicode,lucida grande,sans-serif;"> <strong>{{number_format($totalPriceCheckout, 0, ',', '.')}}</strong><strong style="padding: 0px; margin: 0px;">&nbsp;VNĐ</strong></span>&nbsp;( bao gồm&nbsp;<strong>{{number_format($deliveryFee, 0, ',', '.')}}</strong>&nbsp;<strong>vnđ</strong> tiền phát hàng và
                                <strong>{{number_format($collectFee, 0, ',', '.')}}</strong>&nbsp;<strong>vnđ</strong> phí thu hộ cod )&nbsp;<br>
                                - Nhân Viên {{setting('site.title')}} sẽ gọi điện và xác nhận đơn hàng cũng như thông báo tổng chi phí cho quý khách trong thời gian sớm nhất .&nbsp;</h2>
                            <h5 style="padding: 0px; margin: 0px; border: 0px; font-size: 13px; font-family: Arial, Verdana, san-serif; vertical-align: baseline; color: rgb(0, 0, 0); line-height: 19.5px;">
                                Cảm ơn quý khách đã tin tưởng và ủng hộ {{setting('site.title')}}.&nbsp;</h5>
                            <p></p>
                        </div>
                        <div class="finish-box">
                            <ul class="list-unstyled">
                                <li>Đơn đặt hàng bạn đã gửi đến website thành công.</li>
                                <li>Chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất.</li>
                            </ul>
                        </div>
                    </div><!-- /.box-body -->
                </div>
            </div>

        </div>
    </div><!-- /. colum-one -->
</main>
@endsection
