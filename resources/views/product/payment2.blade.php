@extends('layouts.main')
@section('content')
@php
    $totalWeight = array_reduce($sessionCart, function($total, $item){
        return $total + $item->weight;
    }, 0);
    $collectFee = $totalPrice*0.03;
    $deliveryFee = 50000;
    // $city = (App\Models\City::orderBy('name')->get()->toArray());
    // dd($city);
@endphp

<main>
    <div class="colum-one">
        <div class="container">

            <div class="steps">
                <ol class="progress-steps">
                    <li data-step="1" class="is-complete"><a href="{{route('cart')}}">Giỏ hàng</a></li>
                    <li data-step="2" class="is-active">Thông Tin Giao Hàng</li>
                    <li data-step="3" class="progress__last"><span>Thanh Toán</span></li>
                </ol>
            </div>

            <div class="content-full wrap_cart">
                <div class="row">
                    <form id="billing-form" action="{{route('checkout2')}}" method="POST" class="will_edit form form-registration">
                    <div class="col-sm-8">

                            @csrf
                            <div class="box">
                                <div class="box_header">
                                    <h2 class="box_title">Địa chỉ nhận hàng</h2>
                                </div>
                                <div class="box_body box_body_form">
                                    <input type="hidden" name="customer_name" value="{{$user->name ?? old('customer_name')}}" required />
                                    <input type="hidden" name="customer_phone" value="{{$user->phone ?? old('customer_phone')}}" maxlength="12" required/>
                                    <input type="hidden" name="email" value="{{$user->email ?? old('email')}}" required/>
                                    <div class="row">
                                        <div class="col-sm-6">

                                            <div class="form-group {{$errors->has('customer_address') ? 'has-error has-danger' : ''}}">
                                                <label for="user_address" class="control-label">Địa chỉ nhận hàng <span class="text-color-red">(*)</span></label>
                                                <input class="form-control" autocomplete="off" type="text" id="customer_address" name="customer_address" value="{{old('customer_address')}}" required="">
                                                @if ($errors->has('customer_address'))
                                                    <div class="help-block with-errors">
                                                        <ul class="list-unstyled">
                                                            <li>{{ $errors->first('customer_address') }}</li>
                                                        </ul>
                                                    </div>
                                                @endif
                                            </div>

                                            <div class="form-group">
                                                <label for="user_address" class="control-label">Lời nhắn</label>
                                                <input class="form-control" autocomplete="off" type="text" id="customer_note" name="customer_note" value="{{old('customer_note')}}">
                                            </div>

                                        </div>
                                        <!--/.col -->
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="city" class="control-label">Tỉnh/thành phố <span class="text-color-red">(*)</span></label>
                                                <select id="city" name="city" class="form-control province" onchange="locationSelection.districtList(this);" required>
                                                    <option value="">Chọn Tỉnh/Thành phố</option>
                                                    @foreach ($cities as $city)
                                                        <option value="{{$city->matp}}" {{old('city') == $city->matp ? 'selected' : ''}}>{{$city->name}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('city'))
                                                    <div class="help-block with-errors">
                                                        <ul class="list-unstyled">
                                                            <li>{{ $errors->first('city') }}</li>
                                                        </ul>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label for="district" class="control-label">Quận/huyện <span class="text-color-red">(*)</span></label>
                                                <select class="form-control" id="district" name="district" onchange="locationSelection.wardList(this);" onclick="locationSelection.wardList(this);" required>
                                                    <option value="">Chọn Quận / Huyện</option>
                                                </select>
                                                @if ($errors->has('district'))
                                                    <div class="help-block with-errors">
                                                        <ul class="list-unstyled">
                                                            <li>{{ $errors->first('district') }}</li>
                                                        </ul>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label for="ward" class="control-label">Xã / Phường <span class="text-color-red">(*)</span></label>
                                                <select class="ward form-control" id="ward" name="ward" required>
                                                    <option value="999999999999">Khác</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!--/.col -->
                                    </div>
                                    <!--/.row -->

                                    <input type="hidden" name="total" value="{{$totalPrice + $collectFee + $deliveryFee}}" />
                                    <input type="hidden" name="subtotal" value="{{$totalPrice}}" />
                                    <input type="hidden" name="deliveryFee" value="{{$deliveryFee}}" />
                                    <input type="hidden" name="collectFee" value="{{$collectFee}}" />

                                </div>

                            </div>

                            <div class="box">
                                <div class="box_header">
                                    <h2 class="box_title">Thanh toán và Vận chuyển</h2>
                                </div>
                                <div class="box_body box_body_form">
                                    <div class="payment-shippings">

                                        <div class="payment-method">
                                            <label>
                                                <input type="radio" id="custom_ship_4" name="shipping_method" class="thanhtoan_radio" value="1" onclick="shippingMethod(this)" checked="checked" />
                                                <img src="{{asset('images/payment-6.png')}}" alt="Thanh toán khi nhận hàng" />
                                                Thanh toán khi nhận hàng
                                            </label>
                                        </div>
                                        <div class="payment-method">
                                            <label>
                                                <input type="radio" id="custom_ship_1" name="shipping_method" class="thanhtoan_radio" value="2" onclick="shippingMethod(this)" />
                                                <img src="{{asset('images/payment-13.png')}}" alt="Chuyển khoản qua Ngân lượng" />
                                                Chuyển khoản qua Ngân lượng
                                            </label>
                                        </div>
                                        <div class="payment-method">
                                            <label>
                                                <input type="radio" id="custom_ship_3" name="shipping_method" class="thanhtoan_radio" value="3" onclick="shippingMethod(this)" />
                                                <img src="{{asset('images/shipping-1.png')}}" alt="Mua hàng trực tiếp tại cửa hàng" />
                                                Mua hàng trực tiếp tại cửa hàng
                                            </label>
                                        </div>
                                        <p class="note">Lưu ý: (Đã bao gồm VAT 10%, Phụ phí xăng dầu 15%)</p>
                                    </div>

                                </div>
                                <div class="box_footer hidden-xs hidden-sm clearfix">
                                    <a href="{{route('home')}}" class="btn btn-back"><i class="fa fa-long-arrow-left"></i> Tiếp tục mua hàng</a>
                                    <button type="submit"  class="btn btn-buy-now-mini pull-right submit-button">Thanh toán</button>
                                </div>
                            </div>

                    </div><!-- /.col -->

                    <div class="col-sm-4">
                        <div class="box">
                            <div class="box_header">
                                <h2 class="box_title">Thông tin đơn hàng</h2>
                            </div>
                            <div class="box_body">
                                <div class="order-items">
                                    {{-- LIST --}}
                                    @foreach ($sessionCart as $item)
                                    <div class="order-item">
                                        <div class="name">
                                            <span class="quantity">{{$item->quantity}} x</span>
                                            <a href="{{$item->url}}">{{$item->name}}</a> ({{number_format($item->weight, 0, ',', '.')}}gr)
                                        </div>
                                        <div class="price">{{number_format($item->price, 0, ',', '.')}} đ</div>
                                    </div>
                                    @endforeach
                                    {{-- /.LIST --}}
                                </div>
                                <hr>
                                
                                <div class="order-summary">
                                    <div class="sum_item">
                                        <span class="k">Tổng sản phẩm</span>
                                        <span id="order-quantity" class="v">{{count($sessionCart)}}</span>
                                    </div>
                                    <div class="sum_item" id="sum_price">
                                        <div class="order_item">
                                            <span class="k">Trọng lượng (gr)</span>
                                            <span class="v title-weightall" rel="2000">{{$totalWeight}}</span>
                                        </div>
                                        <div class="order_item">
                                            <span class="k">Tổng tạm tính</span>
                                            <div class="v"><span id="sub_total">{{number_format($totalPrice, 0, ',', '.')}}</span><span> đ</span></div>
                                        </div>
                                    </div>
                                    <div class="sum_item" id="deliveryFee">
                                        <span class="k">Phí vận chuyển</span>
                                        <div class="v"><span class="title-thanhtien" >{{number_format($deliveryFee, 0, ',', '.')}}</span><span> đ</span></div>
                                    </div>
                                    <div class="sum_item" id="collectFee" style="display: inline-block;">
                                        <span class="k">Phí thu tiền hộ</span>
                                        <div class="v"><span class="title-thanhtien" >{{number_format($collectFee, 0, ',', '.')}}</span><span> đ</span></div>
                                    </div>
                                    <hr>
                                    <div class="sum_item total" id="total_price">
                                        <span class="k">Tổng tiền</span>
                                        <div class="v">
                                            <span class="title-thanhtien" id="total">{{number_format($totalPrice + $collectFee + $deliveryFee, 0, ',', '.')}}</span>
                                            <span> đ</span>
                                        </div>
                                    </div>

                                </div><!-- /.order-summary-->
                            </div>
                            <div class="box_footer visible-xs visible-sm clearfix">
                                <a href="{{route('home')}}" class="btn btn-back"><i class="fa fa-long-arrow-left"></i> Tiếp tục mua hàng</a>
                                {{-- <a href="javascript:void(0)" onclick="view_order()" class="btn btn-buy-now-mini pull-right">Thanh toán</a> --}}
                                <button type="submit"  class="btn btn-buy-now-mini pull-right submit-button">Thanh toán</button>
                            </div>

                        </div>
                    </div><!-- /.col-->
                    </form>
                </div><!-- /.row -->

            </div>
        </div>
        
    </div><!-- /. bg-gray -->
    
    <script>
        function shippingMethod(element){
            let shippingID = element.value,
                inputTotal = $('input[name="total"]'),
                inputSubtotal = $('input[name="subtotal"]'),
                inputDeleveryFee = $('input[name="deliveryFee"]'),
                inputCollectFee = $('input[name="collectFee"]');

            switch (shippingID) {
                case '2'://via bank
                    $('#deliveryFee').css('display', 'none');
                    
                    inputTotal.val( Number(inputSubtotal.val()) + Number(inputCollectFee.val()) );
                    
                    $('#total').html( Number(inputTotal.val()).toLocaleString('vi-VN') );
                    break;
                case '3'://direct
                    $('#deliveryFee').css('display', 'none');
                    $('#collectFee').css('display', 'none');
                    
                    inputTotal.val( Number(inputSubtotal.val())  );
                    
                    $('#total').html( Number(inputTotal.val()).toLocaleString('vi-VN') );
                    break;
            
                default://via cod
                    $('#deliveryFee').css('display', 'inline-block');
                    $('#collectFee').css('display', 'inline-block');
                    
                    inputTotal.val( Number(inputSubtotal.val()) + Number(inputCollectFee.val()) + Number(inputDeleveryFee.val()) );
                    
                    $('#total').html( Number(inputTotal.val()).toLocaleString('vi-VN') );
                    break;
            }
        }

        $('#billing-form').on('submit', function(){
            $('.submit-button').prop('disabled', true);
        })
    </script>
</main>
@endsection
