@extends('layouts.main') 
@section('content')
<div class="modal fade in" id="login-dialog" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <a class="close btn-close" data-dismiss="modal">×</a>
                <h4 class="modal-title text-center">ĐĂNG NHẬP</h4>
            </div>
            <div class="modal-body clearfix">

                <form class="form form-registration" method="post" id="login_popup" data-toggle="validator" novalidate="true">
                    <div class="form_inner">
                        <div class="alert alert-danger form-error"></div>

                        <div class="form-group finuser">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                <input class="form-control" autocomplete="off" type="text" id="user_login" name="user_login" value="" placeholder="Email / Số điện thoại"
                                    required="">
                            </div>
                        </div>

                        <div class="form-group finpass hideform" style="display: block;">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                <input class="form-control" autocomplete="off" type="password" id="user_pass" name="user_pass" value="" placeholder="Mật khẩu"
                                    required="">
                            </div>
                        </div>

                        <div class="form-group password-helper hideform" style="display: block;">
                            <label for="remember_me" class="remember-me"><input type="checkbox" name="remember_me" id="remember_me" value="Y" checked="checked"> Ghi nhớ đăng nhập</label>
                            <a class="pull-right" rel="nofollow" href="{{route('user.password.forget')}}">Quên mật khẩu?</a>
                        </div>

                        <div class="form-group">
                            <div class="box-radio-cop">
                                <div class="text-radio clearfix">
                                    <input name="is_guest" id="is_guest_1" type="radio" value="1">
                                    <label for="is_guest_1">Bạn là khách hàng mới</label>
                                </div>
                                <div class="text-radio clearfix">
                                    <input name="is_guest" id="is_guest_0" type="radio" value="0" checked="checked">
                                    <label for="is_guest_0">Bạn đã có tài khoản</label>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <button onclick="login_cart_ajax('login_popup');" type="button" class="btn-green btn-block" data-toggle="modal" data-target="#myLogin">ĐĂNG NHẬP</button>
                            <button onclick="fb_login();" type="button" class="btn-fb1 btn-block">ĐĂNG NHẬP BẰNG FACEBOOK</button>
                            <button onclick="gg_login();" type="button" class="btn-gg1 btn-block">ĐĂNG NHẬP BẰNG GOOGLE</button>
                        </div>
                        <p class="form-helper hideform" style="display: block;">Bạn chưa có tài khoản? </p>
                        <div class="form-group">
                            <button onclick="window.location.href='{{route('register')}}'" type="button" class="btn-dn1 btn-block">ĐĂNG KÝ TÀI KHOẢN MỚI</button>
                        </div>

                    </div>
                </form>
            </div>
            {{-- <div class="modal-footer">
                <p class="pull-left ortext">Nếu bạn không am hiểu về công nghệ hãy liên lạc với chúng tôi qua hotline <a id="callnowbutton" href="tel:0888881932">HN:088888.1932</a>                    <br>
                    <a id="callnowbutton" href="tel:0899991932">SG:089999.1932</a> <br>Để đặt hàng qua điện thoại </p>

            </div> --}}
        </div>
    </div>
</div>
<main>
    <div class="colum-one page-cart">
        <div class="container">

            <div class="steps">
                <ol class="progress-steps">
                    <li data-step="1" class="is-active"><a href="{{route('cart')}}">Giỏ hàng</a></li>
                    <li data-step="2" class=""><span>Thông Tin Giao Hàng</span></li>
                    <li data-step="3" class="progress__last"><span>Thanh Toán</span></li>
                </ol>
            </div>

            <div class="content-cart">
                <div class="content-cart-left">
                    <form id="checkout-step-1" class="form form-blocking" action="/checkout-step-2" method="post">
                        <div class="box box-no-padding">
                            <div class="box_header visible-xs visible-sm">
                                <h2 class="mTitle">Thông tin sản phẩm</h2>
                            </div>
                            <div class="box_body">
                                <table class="table table--listing table--checkout">
                                    <thead>
                                        <tr>
                                            <th class="items0count" colspan="2">Sản phẩm trong giỏ hàng: <span class="badge">{{count($sessionCart)}}</span></th>
                                            <th width="16%">Đơn giá</th>
                                            <th width="16%">Số lượng</th>
                                            <th width="16%">Thành tiền</th>
                                            <th class="action" width="1%">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($sessionCart as $key => $item)
                                        <tr>
                                            <td class="image">
                                                <input type="hidden" name="variants[product_id][]" value="28">
                                                <img src="{{$item->image}}" width="80">
                                            </td>
                                            <td class="name">
                                                <a href="{{$item->url}}">{{$item->name}}</a>
                                                <a>
                                                    <div class="code">Mã SP : {{$item->brand}}</div>
                                                </a>
                                            </td>
                                            <td class="unit">
                                                <label>{{number_format($item->price, 0, ',', '.')}}</label>
                                            </td>
                                            <td class="quantity">
                                                <input type="hidden" name="item[{{$item->product_id}}][price]" value="{{$item->price}}" />
                                                <select class="updatecart_quantity" name="item[{{$item->product_id}}][quantity]" onchange="cart.update({{$key}}, this)">
                                                    @for ($i = 1; $i <= 10; $i++) 
                                                        <option value="{{$i}}" {{$item->quantity == $i ? 'selected' : ''}}>{{$i}}</option>
                                                    @endfor
                                                </select>
                                            </td>
                                            <td id="total_c59180b5f757a367a49624c2bf5c2f09" class="total">{{number_format($item->quantity*$item->price, 0, ',', '.')}}</td>
                                            <td class="action">
                                                <button type="button" class="btn-none del_prod" onClick='cart.remove("{{$item->product_id}}");'>
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="box_footer hidden-xs hidden-sm clearfix">
                                <a href="{{route('home')}}" class="btn btn-back"><i class="fa fa-long-arrow-left"></i> Tiếp tục mua hàng</a>
                                <a href="{{route('payment')}}" class="btn-success btn-buy-now pull-right">ĐẶT HÀNG</a>
                            </div>

                        </div>
                    </form>
                </div>

                <div class="content-cart-sidebar">
                    <div class="box">
                        <div class="box_header">
                            <h2 class="box_title">THÔNG TIN CHUNG</h2>
                        </div>
                        <div class="box_body">
                            <ul class="order-summary">
                                <li>
                                    <span class="k">Tổng sản phẩm</span>
                                    <span id="order-quantity" class="v">{{count($sessionCart)}}</span>
                                </li>
                                <li>
                                    <span class="k">Tổng tạm tính</span>
                                    <span id="order-subtotal" class="v">{{number_format($totalPrice, 0, ',', '.')}}đ</span>
                                </li>
                                <li>
                                    <span class="k">Phí giao hàng (<a target="_blank" href="javascript:;" title="Chi tiết chính sách giao hàng">?</a>)</span>
                                    <span class="v"></span>
                                </li>
                                <li class="sep"></li>
                                <li class="total">
                                    <span class="k">Tổng cộng</span>
                                    <div class="v"><span id="order-total">{{number_format($totalPrice, 0, ',', '.')}}</span><span>đ</span></div>

                                </li>
                            </ul>
                        </div>
                        <div class="box_footer">
                            <div class="visible-xs visible-sm clearfix">
                                {{-- <a href="javascript:;" class="btn btn-buy-now-mini pull-right" data-toggle="modal" data-target="#login-dialog">ĐẶT HÀNG</a> --}}
                                <a href="{{route('payment')}}" class="btn btn-buy-now-mini pull-right">ĐẶT HÀNG</a>
                                <a href="{{route('home')}}" class="btn btn-back"><i class="fa fa-long-arrow-left"></i> Tiếp tục mua hàng</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection