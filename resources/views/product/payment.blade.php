@extends('layouts.main')
@section('content')
@php
    $totalWeight = array_reduce($sessionCart, function($total, $item){
        return $total + $item->weight;
    }, 0);
    $collectFee = $totalPrice*0.03;
    $deliveryFee = 50000;
@endphp

<main>
    <div class="colum-one">
        <div class="container">

            <div class="steps">
                <ol class="progress-steps">
                    <li data-step="1" class="is-complete"><a href="{{route('cart')}}">Giỏ hàng</a></li>
                    <li data-step="2" class="is-active">Thông Tin Giao Hàng</li>
                    <li data-step="3" class="progress__last"><span>Thanh Toán</span></li>
                </ol>
            </div>

            <div class="content-full wrap_cart">
                <div class="row">
                    <form action="{{route('checkout')}}" method="POST" class="will_edit form form-registration">
                    <div class="col-sm-8">
                            @csrf
                            <div class="box">
                                <div class="box_header">
                                    <h2 class="box_title">Thông tin khách hàng</h2>
                                </div>
                                <div class="box_body box_body_form">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group {{$errors->has('customer_name') ? 'has-error has-danger' : ''}} ">
                                                <label for="fullname" class="control-label">Họ và tên <span class="text-color-red">(*)</span></label>
                                                <input class="form-control" autocomplete="off" type="text" name="customer_name" value="{{old('customer_name')}}" required="">
                                                @if ($errors->has('customer_name'))
                                                    <div class="help-block with-errors">
                                                        <ul class="list-unstyled">
                                                            <li>{{ $errors->first('customer_name') }}</li>
                                                        </ul>
                                                    </div>
                                                @endif
                                            </div>

                                            <div class="form-group {{$errors->has('email') ? 'has-error has-danger' : ''}}">
                                                <label for="user_address" class="control-label">Email <span class="text-color-red">(*)</span></label>
                                                <input class="form-control" autocomplete="off" type="text" name="email" value="{{old('email')}}" required="">
                                                @if ($errors->has('email'))
                                                    <div class="help-block with-errors">
                                                        <ul class="list-unstyled">
                                                            <li>{{ $errors->first('email') }}</li>
                                                        </ul>
                                                    </div>
                                                @endif
                                            </div>

                                        </div>
                                        <!--/.col -->
                                        <div class="col-sm-6">
                                            <div class="form-group {{$errors->has('customer_phone') ? 'has-error has-danger' : ''}}">
                                                <label for="email" class="control-label">Số Điện Thoại <span class="text-color-red">(*)</span></label>
                                                <input class="form-control" type="text" name="customer_phone" value="{{old('customer_phone')}}" maxlength="12" onkeypress="blockNotNumber(event)" required="">
                                                @if ($errors->has('customer_phone'))
                                                    <div class="help-block with-errors">
                                                        <ul class="list-unstyled">
                                                            <li>{{ $errors->first('customer_phone') }}</li>
                                                        </ul>
                                                    </div>
                                                @endif
                                            </div>
                                            
                                            <div class="form-group {{$errors->has('customer_phone_confirmation') ? 'has-error has-danger' : ''}}">
                                                <label for="user_address" class="control-label">Nhập lại số điện thoại <span class="text-color-red">(*)</span></label>
                                                <input class="form-control" autocomplete="off" type="text" name="customer_phone_confirmation" value="" required="">
                                            </div>
                                            
                                        </div>
                                        <!--/.col -->
                                    </div>
                                    <!--/.row -->

                                    <input type="hidden" name="total" value="{{$totalPrice + $collectFee + $deliveryFee}}" />
                                    <input type="hidden" name="subtotal" value="{{$totalPrice}}" />
                                    <input type="hidden" name="deliveryFee" value="{{$deliveryFee}}" />
                                    <input type="hidden" name="collectFee" value="{{$collectFee}}" />
                                </div>
                            </div>

                            <div class="box">
                                <div class="box_footer hidden-xs hidden-sm clearfix">
                                    <a href="{{route('home')}}" class="btn btn-back"><i class="fa fa-long-arrow-left"></i> Tiếp tục mua hàng</a>
                                    <button type="submit" class="btn btn-buy-now-mini pull-right">Bước tiếp theo</button>
                                    <button type="submit" class="btn btn-buy-now-mini pull-right" formaction="{{route('request.callback')}}" style="margin-right: 10px;">Đặt hàng nhanh</button>
                                </div>
                            </div>

                    </div><!-- /.col -->

                    <div class="col-sm-4">
                        <div class="box">
                            <div class="box_header">
                                <h2 class="box_title">Thông tin đơn hàng</h2>
                            </div>
                            <div class="box_body">
                                <div class="order-items">
                                    {{-- LIST --}}
                                    @foreach ($sessionCart as $item)
                                    <div class="order-item">
                                        <div class="name">
                                            <span class="quantity">{{$item->quantity}} x</span>
                                            <a href="{{$item->url}}">{{$item->name}}</a> ({{number_format($item->weight, 0, ',', '.')}}gr)
                                        </div>
                                        <div class="price">{{number_format($item->price, 0, ',', '.')}} đ</div>
                                    </div>
                                    @endforeach
                                    {{-- /.LIST --}}
                                </div>
                                <hr>
                                
                                <div class="order-summary">
                                    <div class="sum_item">
                                        <span class="k">Tổng sản phẩm</span>
                                        <span id="order-quantity" class="v">{{count($sessionCart)}}</span>
                                    </div>
                                    <div class="sum_item" id="sum_price">
                                        <div class="order_item">
                                            <span class="k">Trọng lượng (gr)</span>
                                            <span class="v title-weightall" rel="2000">{{$totalWeight}}</span>
                                        </div>
                                        <div class="order_item">
                                            <span class="k">Tổng tạm tính</span>
                                            <div class="v"><span id="sub_total">{{number_format($totalPrice, 0, ',', '.')}}</span><span> đ</span></div>
                                        </div>
                                    </div>

                                </div><!-- /.order-summary-->
                            </div>
                            <div class="box_footer visible-xs visible-sm clearfix">
                                <a href="/" class="btn btn-back"><i class="fa fa-long-arrow-left"></i> Tiếp tục mua hàng</a>
                                <button id="request-callback" type="submit" class="btn btn-buy-now-mini pull-right" formaction="{{route('request.callback')}}" style="margin-right: 10px;">Đặt hàng nhanh</button>
                                <button type="submit"  class="btn btn-buy-now-mini pull-right">Bước tiếp theo</button>
                            </div>

                        </div>
                    </div><!-- /.col-->
                    </form>
                </div><!-- /.row -->

            </div>
        </div>
        
    </div><!-- /. bg-gray -->
</main>
@endsection
