@extends('layouts.main')
@section('content')
<main>
    <div class="colum-one">
        <div class="container">

            <div class="content-full">
                <div class="box box-center">
                    <div class="box_header">
                        <h2 class="boxTitle"><i class="fa fa-check fa-lg text-success"></i>Đặt hàng nhanh thành công</h2>
                    </div>
                    <div class="box_body">
                        <div class="finish-box">
                            <h3 class="lineTitle">Kính gửi quý khách</h3>
                            <h5 style="padding: 0px; margin: 0px; border: 0px; font-size: 13px; font-family: Arial, Verdana, san-serif; vertical-align: baseline; color: rgb(0, 0, 0); line-height: 19.5px;">
                                Đơn hàng của quý khách đã được gửi tới website {{setting('site.title')}}</h5>
                            <h2 style="padding: 0px; margin: 0px; border: 0px; font-size: 13px; font-family: Arial, Verdana, san-serif; vertical-align: baseline; color: rgb(0, 0, 0); line-height: 19.5px;">
                                Nhân Viên của chúng tôi sẽ gọi điện xác nhận đơn hàng cũng như thông báo tổng chi phí cho quý khách trong thời gian sớm nhất .&nbsp;</h2>
                            <h5 style="padding: 0px; margin: 0px; border: 0px; font-size: 13px; font-family: Arial, Verdana, san-serif; vertical-align: baseline; color: rgb(0, 0, 0); line-height: 19.5px;">
                                Cảm ơn quý khách đã tin tưởng và ủng hộ {{setting('site.title')}}.&nbsp;</h5>
                        </div>
                        <div class="finish-box">
                            <ul class="list-unstyled">
                                <li>Chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất.</li>
                            </ul>
                            <br/>
                            <a href="{{route('home')}}" class="btn btn-primary" style="display:block; margin: 0 auto;">Trở về trang chủ</a>
                        </div>
                    </div><!-- /.box-body -->
                </div>
            </div>

        </div>
    </div><!-- /. colum-one -->
</main>
@endsection
