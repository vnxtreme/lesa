@extends('layouts.main')
@section('content')
<main>
    <div class="pmain">
        <div class="incategory">
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="{{route('home')}}">Home</a></li>
                    <li>Tìm kiếm: "{{$keywords}}"</li>
                </ol>
            </div><!-- /.container -->
        </div><!-- /.incategory -->

        <div class="container">
            <div class="incategory-list">

                <div class="info-page">
                    <h2 class="mTitle inline-block">Từ khóa: "{{$keywords}}"</h2>
                    <div class="block__sorting hidden-sm hidden-xs" id="block-main-sortings">
                        <div class="product-sortings">
                            @include('components.filter')
                        </div>
                    </div>
                    <a class="btn btn-sm btn-default visible-xs visible-sm filters-toggle category-header-filter-button">Bộ lọc <i class="fa fa-angle-double-right"></i></a>
                </div>

                <div class="list-product">
                    <div class="row">

                        @forelse($products as $product)
                        <div class="col-xs-6 col-sm-3">
                            <div class="item">
                                <div class="cover imgproduct">
                                    <a href="{{route('product.categorySlug.id', ['categorySlug' => $product->category->slug, 'id'=> $product->id])}}" title="{{$product->name}}">
                                        <img alt="{{$product->name}}" src="{{asset("storage/{$product->image}")}}" class="imgresponsive" />
                                    </a>
                                </div>
                                <h3>
                                    <a href="{{route('product.categorySlug.id', ['categorySlug' => $product->category->slug, 'id'=> $product->id])}}" title="{{$product->name}}">{{$product->name}}</a>
                                </h3>

                                @include('components.price')
                            </div>
                        </div>
                        @empty
                            <h5>Không tìm thấy</h5>
                        @endforelse

                    </div>

                    <div id="results" class="text-center">{{$products->links()}}</div>
                </div>
            </div>
        </div><!-- /.container -->

    </div><!-- /.pmain -->




</main>
@endsection
