@extends('layouts.main') 
@section('content')
<main>
    <div class="pmain">
        <div class="incategory">
            <div class="container">
                <ol class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
                    <li typeof="v:Breadcrumb">
                        <a href="{{route('home')}}" title="Home">Home</a>
                    </li>
                    @include('components.scrumb')
                </ol>
            </div>
            <!-- /.container -->
        </div>
        <!-- /.incategory -->

        <div class="position-banners">
            <div class="container">
                <div class="item">
                    <a target="_blank" href="{{route('home')}}" title="{{setting('site.title')}} ">
                        <img src="{{asset('images/lesa-company.jpg')}}" class="imgresponsive" alt="{{setting('site.title')}}" />
                    </a>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="incategory-list">
                <div class="row">
                    <div class="col-md-9 col-md-push-3">

                        <div class="info-page">
                            <h2 class="pTitle">{{$scrumbs[0]['name'] ?? ''}}<span>Tìm thấy {{$products->isNotEmpty() ? $products->total() : 0 }} sản phẩm</span></h2>
                            <div class="block__sorting hidden-sm hidden-xs" id="block-main-sortings">
                                <div class="product-sortings">
                                    @include('components.filter')
                                </div>
                            </div>
                            <a class="btn btn-sm btn-default visible-xs visible-sm filters-toggle category-header-filter-button">
                                Bộ lọc <i class="fa fa-angle-double-right"></i>
                            </a>
                        </div>

                        <div class="list-product">
                            <div class="row">
                                @if($products->isNotEmpty())
                                @foreach($products as $product)
                                <div class="col-xs-6 col-sm-4">
                                    <div class="item">
                                        <div class="cover imgproduct">
                                            <a href="{{route('product.categorySlug.id', ['categorySlug' => $product->category->slug, 'id' => $product->id])}}" title="{{$product->name}}">
                                                <img src="{{asset("images/lazy.jpg")}}" data-src="{{asset("storage/{$product->image}")}}" class="imgresponsive lazy" alt="{{$product->name}}" />
                                            </a>
                                        </div>
                                        <h3>
                                            <a href="{{route('product.categorySlug.id', ['categorySlug' => $product->category->slug, 'id' => $product->id])}}" title="{{$product->name}}">
                                                {{$product->name}}
                                            </a>
                                        </h3>

                                        @include('components.price')
                                    </div>
                                </div>
                                @endforeach
                                @endif
                                
                            </div>
                            {{$products->links()}}
                        </div>
                    </div>
                    <!-- /.col-sm-9 -->

                    <div class="col-md-3 col-md-pull-9">
                        <div class="sidebar-container sidebarmb">

                            <div class="sidebar-bottom-fix visible-xs visible-sm ">
                                <button class="btn-sort" data-toggle="modal" data-target="#modalSort"><i class="fa fa-sort"></i><span>Sắp xếp</span></button>
                                <button class="btn-sort btn-filter"><i class="fa fa-filter"></i><span>Bộ lọc</span></button>
                            </div>
                            <div class="sidebarmb-title ">
                                <a class="btn sidebarmb-close"><i class="fa fa-angle-double-left"></i> Quay lại</a> Bộ lọc
                            </div>
                            <div class="sidebarmb-content">

                                @if(isset($thisCategory) and $thisCategory)
                                <div class="sidebar sidebar-navleft">
                                    <div class="sidebar-box active">
                                        <h3 class="sidebar-title">{{$thisCategory->name}}</h3>
                                        
                                        @if($thisCategory->childCategories->isNotEmpty())
                                            <div class="sidebar-box-content">
                                                <ul class="sidebar-nav">
                                                    @foreach($thisCategory->childCategories as $child)
                                                        <li @if($child->slug == $categorySlug) class="active" @endif>
                                                            <a class="navleft-none" href="{{route('product.categorySlug', ['categorySlug' => $child->slug])}}">{{$child->name}}</a>
                                                        <li/>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                @endif

                            </div>
                            <!-- /.sidebarmb-content -->



                            <div class="nav-bottom visible-sm visible-xs">
                                <a class="btn btn-success btn-apply">Áp dụng</a><a class="btn btn-default btn-reset">Xóa tất cả</a>
                            </div>
                        </div>
                        <!-- /.sidebar-container -->
                    </div>
                    <!-- /.col-sm-3 -->

                </div>
            </div>
        </div>
        <!-- /.container -->


    </div>
    <!-- /.pmain -->

    <div class="modal fade in" id="modalSort" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <a class="close btn-close" data-dismiss="modal">×</a>
                    <h4 class="modal-title">Sắp xếp theo:</h4>
                </div>
                <div class="modal-body clearfix">
                    <form>
                    <div class="filter">

                        <input type="hidden" name="filter-href" value="{{request()->url()}}">
                        <input type="hidden" name="filter-type" value="default">
                        <input type="hidden" name="filter-sort" value="asc">
                        <input type="hidden" name="filter-price-start" value="0">
                        <input type="hidden" name="filter-price-end" value="0">

                        <div class="filter-item">
                            <a class="sorting" href="{{request()->url()}}">Mặc định</a>
                            {{-- <input type="radio" value="id" data-sort="asc" id="filter_id_asc" name="filter-radio" checked="checked" />
                            <label for="filter_id_asc">Mặc định</label> --}}
                        </div>
                        <div class="filter-item">
                            <a class="sorting" href="{{request()->url()."?type=price&sort=asc"}}">Giá: thấp tới cao</a>
                            {{-- <input type="radio" value="price" data-sort="asc" id="filter_price_asc" name="filter-radio">
                            <label for="filter_price_asc">Giá: thấp tới cao</label> --}}
                        </div>
                        <div class="filter-item">
                            <a class="sorting" href="{{request()->url()."?type=price&sort=desc"}}">Giá: cao tới thấp</a>
                            {{-- <input type="radio" value="price" data-sort="desc" id="filter_price_desc" name="filter-radio">
                            <label for="filter_price_desc">Giá: cao tới thấp</label> --}}
                        </div>
                        <div class="filter-item">
                            <a class="sorting" href="{{request()->url()."?type=view&sort=desc"}}">Lượt xem</a>
                            {{-- <input type="radio" value="view" data-sort="asc" id="filter_view_asc" name="filter-radio">
                            <label for="filter_view_asc">Lượt xem</label> --}}
                        </div>
                        <div class="filter-item">
                            <a class="sorting" href="{{request()->url()."?type=bestseller&sort=desc"}}">Mua nhiều</a>
                            {{-- <input type="radio" value="sell" data-sort="asc" id="filter_sell_asc" name="filter-radio">
                            <label for="filter_sell_asc">Mua nhiều</label> --}}
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var posLoad = $('#posID');
        var id_cat = $('#catID').val();
        var pages_info={};
        var loading  = false;
        var end_record = false;
        pages_info.num = 2;
        pages_info.id = id_cat;


        $(document).ready(function() {
            $(window).on('scroll', function() {
                i = $(window).scrollTop();
                var x = posLoad.position();

                if($(window).width()<=480){
                    $('.pagination').hide();
                    if (i>x.top-300 && loading==false) {
                        contentsload(); 
                    }
                }
            });
        });

        function contentsload(){
            if(loading == false && end_record == false){
                $('.loading').show();
                loading = true;
                $.post(web_address+'ajax/show_product_ajax',{ 
                    page: pages_info.num,
                    cat_id: pages_info.id
                },function(data){
                    //console.log(data);
                    if(data != 0){ 
                        $('#results').append(data);
                        $('.loading').hide();
                        end_record = true;
                        loading = false;
                        pos_cat = $('#posID').offset();
                        pages_info.num++;
                    }else{
                        $('.loading').hide();
                        loading = true;
                    }
                    end_record = false;
                });
            }
        }
    </script>

</main>
@endsection