@extends('layouts.main')
@section('content')
@php
    if($product->old_price > $product->current_price):
        $percentage = round(($product->old_price - $product->current_price )*100/$product->old_price);
    endif
@endphp
<main>
    <div class="pmain" id="view_product">
        <div class="in-products-detail show_info_view">
            <div class="container">
                <ol class="breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a itemprop="item" href="{{route('home')}}">
                            <span itemprop="name">Home</span>
                            <meta itemprop="position" content="1">
                        </a>
                    </li>
                    @if(isset($scrumbs))
                        @foreach($scrumbs as $key => $scrumb)
                        <li>
                            <a href="{{route('product.categorySlug', ['categorySlug' => $scrumb['slug']])}}">
                                <span>{{$scrumb['name']}}</span>
                            </a>
                        </li>
                        @endforeach
                    @endif
                </ol>

                {{-- BANNER --}}
                <div class="position-banners">
                    <div class="item">
                        <a target="_blank" href="{{route('home')}}" title="{{setting('site.title')}}">
                            <img src="{{asset('images/lesa-company.jpg')}}" class="imgresponsive" alt="{{setting('site.title')}}">
                        </a>
                    </div>
                </div>
                {{-- /.BANNER --}}

                <div id="product">
                    <div class="in-product product-details">
                        <div class="row">
                            <div class="col-md-5">
                                <input class="images_small_cloud" type="hidden" value="{{asset("storage/{$arrayImages[0]}")}}" />

                                <div class="in-product-img gallery">
                                    <div class="product_image  owl-carousel">
                                        @foreach($arrayImages as $image)
                                        <img src="{{asset("storage/{$image}")}}" alt="{{$product->name}}" />
                                        @endforeach
                                    </div>
                                </div>

                                <div class="in-product_thumbnails">
                                    <div class="product_thumbnails owl-carousel">
                                        @foreach($arrayImages as $image)
                                        <img src="{{asset("storage/{$image}")}}" alt="{{$product->name}}" />
                                        @endforeach
                                    </div>
                                    <a class="prev"><i class="fa fa-chevron-left animation"></i></a>
                                    <a class="next"><i class="fa fa-chevron-right animation"></i></a>
                                </div>
                            </div>

                            <div class="col-md-7">
                                <div class="in-product-info">

                                    <div itemprop="name">
                                        <h1 class="title_prod">
                                            <a href="{{route('product.categorySlug.id', ['categorySlug' => $product->category->slug, 'id' => $product->id])}}">
                                                {{$product->name}}
                                            </a>
                                        </h1>
                                    </div>

                                    <div class="fb-like" data-href="{{url()->current()}}" data-layout="button" data-action="like" data-size="large" data-show-faces="false" data-share="true" ></div>

                                    @php
                                        $excerpt = nl2br($product->excerpt);
                                    @endphp
                                    <div class="product_description">{!!$excerpt!!}</div>

                                    <div class="clearfix">
                                        <div class="product-code">Mã sản phẩm: <span>{{$product->brand}}</span> </div>
                                        <div class="info-field">
                                            <ul class="list-inline">
                                                <li>
                                                    Trọng lượng (Gr): <span>{{$product->weight}}</span>
                                                </li>
                                                <li>
                                                    Bảo hành: <span>{{$product->warranty}} tháng</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    {{-- PRICE --}}
                                    <div class="info-of product_price_info clearfix">
                                        @if($product->old_price)
                                            <div class="product_price_old">
                                                <span>Giá gốc:</span>
                                                <span class="price_value">{{number_format($product->old_price, 0, ',', '.')}}</span>
                                                <span class="price_symbol">đ</span>
                                            </div>
                                        @endif

                                        <div class="product_price prod_price">
                                            <span class="price_value">{{number_format($product->current_price, 0, ',', '.')}}</span>
                                            <span class="price_symbol">đ</span>
                                            @if(isset($percentage))
                                                <span class="price_discount">-{{$percentage}}%</span>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- .PRICE --}}

                                    <div class="info-of product_add-to-cart">
                                        <form id="form-add-to-cart" method="POST">
                                            <input type="hidden" name="product_id" value="{{$product->id}}">
                                            <div class="clearfix">
                                                <div class="qty quantity box-quantity">
                                                    <span class="quantity__text">Số lượng</span>
                                                    {{-- <select class="update_prod_quantity" name="uquantity"> --}}
                                                        <select class="update_prod_quantity" name="quantity">
                                                            <option value="1" selected="selected">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                        </select>
                                                </div>
                                                {{-- <input type="text" name="quantity" value="1" id="input-quantity" /> --}}
                                            </div>

                                            <div class="add-to-cart__actions add-to-cart-buttons">
                                                <span class="btn-srcoll-mb">
                                                    <a type="button" class="btn btn-success btn-buy-now" onclick="cart.add('buy')">MUA NGAY <i class="fa fa-long-arrow-right"></i></a>
                                                </span>
                                                <a id="button-cart" href="javascript:;" class="btn btn-add-to-cart" onclick="cart.add()">
                                                    <i class="fa fa-shopping-basket"></i> THÊM VÀO GIỎ HÀNG
                                                </a>

                                            </div>
                                        </form>
                                    </div>

                                    <div class="info-of product__stats border-top">
                                        <div class="product__purchases">
                                            <i class="fa fa-eye"></i> {{$product->view}} đã xem
                                        </div>
                                    </div>

                                </div>
                            </div><!-- /.pb-center-column -->

                        </div><!-- /.row -->
                    </div><!-- /.in-product -->

                    {{-- DESCRIPTION --}}
                    <div class="in-more">
                        <div class="row">
                            <div class="col-md-9">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab-info" data-toggle="tab">THÔNG TIN CHI TIẾT</a></li>
                                    <li><a href="#tab-address" data-toggle="tab">ĐỊA ĐIỂM MUA HÀNG</a></li>
                                    <li><a href="#tab-rate" data-toggle="tab">Tài khoản ngân hàng</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab-info">
                                        <h3 class="mTitle">THÔNG TIN CHI TIẾT</h3>
                                        <div class="in-more-content">{!! $product->body !!}</div>
                                    </div>

                                    <div class="tab-pane" id="tab-address">
                                        <h3 class="mTitle">ĐỊA ĐIỂM MUA HÀNG</h3>
                                        <div class="in-more-content">
                                            <div class="row product-address">
                                                <div class="col-md-7">
                                                    <a class="media-link" data-type="map" target="_blank" href="https://goo.gl/maps/v9EEQBYkn9P2">
                                                        <!-- <img class="img-responsive" src=" https://maps.googleapis.com/maps/api/staticmap?center=10.7526943,106.6801208&zoom=15&markers=color:red|label:B|10.7526943,106.6801208&size=600x350&key=AIzaSyDL9CpwIk6P5O57udv9mCUuLCoB4aB3Dvc" class="image_map">                                        
                                                </a> -->
                                                    </a></div><a class="media-link" data-type="map" target="_blank" href="https://goo.gl/maps/v9EEQBYkn9P2">
                                                </a>
                                                <div class="col-md-5 maps-address"><a class="media-link" data-type="map" target="_blank" href="https://goo.gl/maps/v9EEQBYkn9P2">
                                                    </a>
                                                    <div class="panel-group"><a class="media-link" data-type="map" target="_blank" href="https://goo.gl/maps/v9EEQBYkn9P2">
                                                        </a>
                                                        <div class="panel panel-default"><a class="media-link" data-type="map" target="_blank" href="https://goo.gl/maps/v9EEQBYkn9P2">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">{{ setting('site.title') }} giao hàng tận nơi</h4>
                                                                </div>
                                                            </a>
                                                            <div class="panel-collapse"><a class="media-link" data-type="map" target="_blank" href="https://goo.gl/maps/v9EEQBYkn9P2">
                                                                </a>
                                                                <div class="panel-body">
                                                                    <a class="media-link" data-type="map" target="_blank" href="https://goo.gl/maps/v9EEQBYkn9P2">Xem thông tin </a>
                                                                    <a class="alinks" target="_blank" href="{{route('post.category', ['categorySlug' => 'chinh-sach-giao-hang'])}}">Chính sách giao hàng</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="address-store">

                                                <h1 style="color: red; text-align: center;">
                                                    <img alt="79" height="53" src="https://www.zippozippo.com/libs/ckeditor/plugins/smiley/images/all/79.gif" title="79" width="52" />
                                                    &nbsp; TRỤ SỞ CHÍNH
                                                    {{-- <span style="background-color: rgb(248, 248, 248); color: rgb(0, 0, 0); font-family: &quot;Helvetica Neue&quot;, sans-serif; font-size: 13px;">:</span> --}}
                                                    <br>
                                                    <span style="background-color: rgb(248, 248, 248); color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, sans-serif; font-size: 13px;">{{$addresses[0]->address}}</span>&nbsp;&nbsp;</h1>
                                                <p style="text-align: center;">
                                                    <iframe allowfullscreen="" frameborder="0" height="333" scrolling="no" src="{{$addresses[0]->google_map}}" style="border:0" width="830"></iframe>
                                                </p>
                                            </div><!-- /.addres-store-->
                                        </div>
                                    </div>

                                    @if( $banks->isNotEmpty() )
                                    <div class="tab-pane" id="tab-rate">
                                        <h3 class="mTitle">Tài khoản ngân hàng</h3>
                                        <div class="in-more-content">
                                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 50px">
                                                <tbody>
                                                    
                                                    @foreach($banks as $bank)
                                                    <tr style="background:#F2F2F0; color: #000000; font-family: Consolas, 'Andale Mono', 'Lucida Console', 'Lucida Sans Typewriter', Monaco, 'Courier New', monospace;border: 1px solid #EEEEEE;">
                                                        <td width="15%">
                                                            <img alt="{{$bank->bank_name}}" height="69px" src="{{asset("storage/{$bank->logo}")}}" style="width: 110px; height: 69px;  margin: 5px;padding-right:10px;" width="110px" />
                                                        </td>
                                                        <td width="85%">
                                                            {{$bank->bank_name}}&nbsp;&nbsp;<br>
                                                            Chủ tài khoản: {{$bank->account_name}}<br>
                                                            Số tài khoản: {{$bank->account_number}} {{$bank->atm_number ? "- Số thẻ ATM: {$bank->atm_number}" : ''}}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    @endforeach

                                                </tbody>
                                            </table>
                                            <div>&nbsp;</div>
                                        </div>
                                    </div>
                                    @endif

                                    <div class="box-info product-short-info">

                                        <div class="product_cell product_cell5">
                                            <div class="product_cell_name">Số tiền thanh toán</div>
                                            <div class="product_price">
                                                <span class="price_value">{{number_format($product->current_price, 0, ',', '.')}}</span>
                                                <span class="price_symbol">đ</span>
                                            </div>
                                        </div>

                                        <div class="product_cell">
                                            <div class="product_add-to-cart clearfix">
                                                <div class="add-to-cart__actions add-to-cart-buttons">
                                                    <span class="btn btn-success btn-buy-now" onclick="cart.add('buy')">
                                                        MUA NGAY <i class="fa fa-long-arrow-right"></i>
                                                    </span>
                                                    <a class="btn btn-default btn-add-to-cart" onclick="cart.add()"><i class="fa fa-shopping-basket"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                    </div><!-- /.product-short-info -->

                                    @if($product->meta_keywords)
                                    @php
                                    $tags = explode(',', $product->meta_keywords);
                                    @endphp
                                    <div class="tags">
                                        <h3 class="mTitle">Tags</h3>

                                        @foreach($tags as $keywords)
                                        <a href="#" title="{{$keywords}}">{{$keywords}}</a>
                                        @endforeach
                                    </div>
                                    @endif

                                    {{-- <div class="in-comments">
                                        <h3 class="mTitle">BÌNH LUẬN</h3>
                                        <div class="single_article_content show_info_detail">
                                            <ul class="commentlist">
                                            </ul>
                                            <div id="respond">
                                                <div class="add_comment">


                                                    <div id="infouser">
                                                        <img src="https://lh3.googleusercontent.com/-8jP269Yj-uQ/AAAAAAAAAAI/AAAAAAAAAAA/O7SkLNw1ng8/s64-c/109338539225851225443.jpg" class="imgresponsive" alt="avatar">
                                                    </div>
                                                    <div id="commentform">
                                                        <form method="post" onsubmit="return false;">
                                                            <textarea name="comment_content" id="comment_content" cols="58" rows="3" tabindex="4"></textarea>
                                                            <p>
                                                                <button onclick="send_comment_ajax();" class="send_comment" name="send_comment" type="button">Gửi bình luận</button>

                                                                <button id="cancel_reply" onclick="reply_comment(0)" class="send_comment hide_reply" type="button">Hủy trả lời</button>
                                                                <input type="hidden" name="comment_prod_id" value="173" id="comment_prod_id">
                                                                <input type="hidden" name="comment_parent" id="comment_parent" value="0">
                                                            </p>
                                                        </form>
                                                    </div>

                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}

                                    {{-- <div class="in-comments">
                                        <h3 class="mTitle">BÌNH LUẬN TRÊN FACEBOOK</h3>
                                        <div class="in-more-content">
                                        </div>
                                    </div><!-- .comments_facebook --> --}}

                                </div><!-- .tab-content -->

                                @if($relatedProducts->isNotEmpty())
                                <div class="product-more">
                                    <h3 class="mTitle">
                                        {{-- <a href="#"> --}}Sản phẩm cùng loại{{-- </a> --}}
                                    </h3>
                                    <div class="list-product">
                                        <div class="row">

                                            @foreach($relatedProducts as $relatedProduct)
                                            <div class="col-xs-6 col-md-3">
                                                <div class="item">
                                                    <div class="cover related-imgproduct">
                                                        <a href="{{route('product.categorySlug.id', ['categorySlug' => $relatedProduct->category->slug, 'id' => $relatedProduct->id])}}" title="{{$relatedProduct->name}}">
                                                            <img src="{{asset('images/lazy.jpg')}}" data-src="{{asset("storage/{$relatedProduct->image}")}}" class="imgresponsive lazy" alt="{{$relatedProduct->name}}" />
                                                        </a>
                                                    </div>
                                                    <h3><a href="{{route('product.categorySlug.id', ['categorySlug' => $relatedProduct->category->slug, 'id' => $relatedProduct->id])}}" title="{{$relatedProduct->name}}">{{$relatedProduct->name}}</a></h3>

                                                    @include('components.price', ['product' => $relatedProduct])
    
                                                </div>
                                            </div>
                                            @endforeach

                                        </div>
                                    </div><!-- /.list-row-3 -->
                                </div>
                                @endif

                            </div><!-- .col -->

                            <div class="col-md-3 hidden-sm hidden-xs">
                                <div class="sidebar">
                                    <div class="sidebar-address">
                                        <h3 class="sidebar-title"><i class="fa fa-location-arrow"></i> Địa điểm Mua Hàng</h3>
                                        <div class="sidebar-box-content">
                                            <ul class="sidebar-address-list">

                                                @if($addresses->isNotEmpty())
                                                @foreach($addresses as $address)
                                                <li>
                                                    <i class="fa fa-map-marker"></i>
                                                    <div class="content">
                                                        <p><strong>{{$address->location}}</strong></p>
                                                        <p>{{$address->address}}</p>
                                                        <p><i class="fa fa-phone-square"></i> <strong>{{$address->telephone}} - {{$address->hotline ?? ''}}</strong></p>
                                                    </div>
                                                </li>
                                                @endforeach
                                                @endif

                                            </ul>
                                        </div>
                                    </div>

                                </div>

                                <div class="sidebar"></div>

                                @if($hotProducts->isNotEmpty())
                                <div class="sidebar product-involve view_right">
                                    <h2 class="sidebar-title"><a href="#"><i class="fa fa-bars"></i> Sản Phẩm Hot</a></h2>
                                    <div class="show_relate_right list-product">
                                        @foreach($hotProducts as $hot)
                                        <div class="item">
                                            <div class="cover imgproduct">
                                                <a href="{{route('product.categorySlug.id', ['categorySlug' => $hot->category->slug, 'id' => $hot->id])}}" title="{{$hot->name}}">
                                                    <img alt="{{$hot->name}}" src="{{asset("storage/{$hot->image}")}}" class="imgresponsive" />
                                                </a>
                                            </div>
                                            <h3>
                                                <a href="{{route('product.categorySlug.id', ['categorySlug' => $hot->category->slug, 'id' => $hot->id])}}" title="{{$hot->name}}">
                                                    {{$hot->name}} </a>
                                            </h3>


                                            <div class="out-of-stock">NEW</div>

                                            <div class="price">
                                                <div class="pd-price">
                                                    <span class="price_value" itemprop="price">9.800.000</span>
                                                    <span class="price_symbol">đ</span>
                                                    <span class="price_discount">-18%</span>
                                                </div>
                                                <div class="pd-price pd-price-old">
                                                    <span class="price_value" itemprop="price">12.000.000</span>
                                                    <span class="price_symbol">đ</span>
                                                </div>
                                                <div class="pd-views"><i class="fa fa-user-o"></i> {{$hot->view}}</div>
                                            </div>
                                        </div>
                                        @endforeach

                                    </div>

                                </div>
                                @endif
                            </div><!-- ./col -->
                        </div><!-- .row -->
                    </div><!-- .in-more -->
                    {{-- /.DESCRIPTION --}}

                </div><!-- .itemscope -->
            </div><!-- .container -->
        </div><!-- .incategory -->
    </div><!-- .pmain -->

    <script type="text/javascript" src="{{asset('js/jquery.rateyo.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.rateyo.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.fancybox.min3.0.css')}}">
    <script type="text/javascript" src="{{asset('js/jquery.fancybox.min3.0.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/common.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#rate-product").rateYo({
                starWidth: "27px",
                spacing: "3px",
                fullStar: true,
                onChange: function (rating, rateYoInstance) {
                    $('#rate_prod_star').val(rating);
                }
            });
            var readOnly = $("#rate-product").rateYo("option", "readOnly");

            // $("#rate-product").rate({
            //     selected_symbol_type: 'fontawesome_star',
            //     max_value: 5,
            //     step_size: 1,
            // });

            // $("#rate-product").on("afterChange", function(ev, data){
            //     console.log(data.to);
            // });

            $('.group_images').click(function () {
                return false;
            });
            $("[data-fancybox]").fancybox({
                slideShow: false,
                fullScreen: false,
                thumbs: false,
                closeBtn: true,
            });

            var g_img = $('.product_image');
            var g_thumb = $('.product_thumbnails');
            var item_active = 0;
            g_img.on('initialized.owl.carousel', function (e) {
                var index = e.item.index;
                item_active = index;
            });
            g_img.owlCarousel({
                loop: false,
                nav: false,
                margin: 10,
                lazyLoad: true,
                dots: false,
                navSpeed: 1000,
                smartSpeed: 1000,
                items: 1,
            });
            g_img.on('changed.owl.carousel', function (event) {
                var item = event.item.index;
                g_thumb.find('.owl-item').removeClass('showactive');
                g_thumb.find('.owl-item').eq(item).addClass('active showactive');
                g_thumb.trigger('to.owl.carousel', item);
            });
            g_thumb.on('initialized.owl.carousel', function (e) {
                g_thumb.find('.owl-item').eq(item_active).addClass('showactive');
            });
            g_thumb.owlCarousel({
                loop: false,
                nav: false,
                margin: 10,
                dots: false,
                navSpeed: 1000,
                smartSpeed: 1000,
                responsive: {
                    0: {
                        items: 3
                    },
                    380: {
                        items: 4
                    },
                    600: {
                        items: 5
                    }
                }
            });
            // setTimeout("get_relate_right()", 500);

            $(".in-product_thumbnails .prev").click(function () {
                g_img.trigger('prev.owl.carousel');
            });
            $(".in-product_thumbnails .next").click(function () {
                g_img.trigger('next.owl.carousel');
            });
            $(".product_thumbnails .item").on('click', function (e) {
                var index = $('.product_thumbnails .item').index($(this));
                g_img.trigger('to.owl.carousel', index);
            });
        });

        // function get_relate_right() {
        //     $.post(web_address + 'ajax/change_relate_right', {
        //         type: "id"
        //     }, function (data) {
        //         $('.show_relate_right').html(data);
        //     });
        // }

    </script>
</main>
@endsection
