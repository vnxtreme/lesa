@component('mail::message')
# Đặt hàng thành công

Đơn hàng của quý khách đã được gửi!<br>
Tổng đơn hàng của quý khách hết <b>{{number_format($data['totalPrice'], 0, ',', '.')}} VND</b>.<br>
Nhân viên của chúng tôi sẽ gọi điện và xác nhận đơn hàng cũng như thông báo tổng chi phí cho quý khách trong thời gian sớm nhất.<br>
Cảm ơn quý khách đã tin tưởng và ủng hộ.

Ban quản trị,<br>
{{setting('site.title')}}
@endcomponent