@component('mail::message')
# Order received

New order has been created!
Total value of: {{number_format($data['totalPrice'], 0, ',', '.')}} VND
@component('mail::button', ['url' => $data['url']])
View Order
@endcomponent

Thanks,<br>
{{setting('site.title')}}
@endcomponent