@component('vendor.mail.markdown.message')
Xin chào {{$data['name']}}!

Cám ơn bạn đã đặt mua sản phẩm của chúng tôi tại {{url('/')}}.

Dưới đây là thông tin chi tiết về đơn hàng của bạn:

Mã đơn hàng: {{$data['orderId']}} (vui lòng ghi chú mã đơn hàng này khi thanh toán)

Các sản phẩm đã đặt mua:

<table style="width:100%">
  <tr>
    <th style="text-align:left;">Tên sản phẩm</th>
    <th style="text-align:center;">Đơn giá</th> 
    <th style="text-align:center;">Số lượng</th>
  </tr>
  @foreach ($data['products'] as $product)
  <tr>
    <td width="60">{{$product['productName']}}</td>
    <td width="30" style="text-align:center;">{{number_format($product['productPrice'], 0, ',', '.')}}</td> 
    <td width="10" style="text-align:center;">{{$product['productQuantity']}}</td>
  </tr>
  @endforeach
</table>
<br>
Tổng giá:	{{number_format($data['subTotal'], 0, ',', '.')}} vnđ

Tổng giá trị đơn hàng (gồm phí): {{number_format($data['totalPrice'], 0, ',', '.')}} vnđ

Hình thức thanh toán: {{$data['paymentMethod']}}


Thông tin người thanh toán:
- Họ và Tên: {{$data['name']}}
- Địa chỉ: {{$data['address']}}
- Điện thoại: {{$data['phone']}}
- Email: {{$data['email']}}

Ghi chú:
Khi thanh toán, bạn nhớ ghi rõ MÃ ĐƠN HÀNG và TÊN của mình để tránh nhầm lẫn.

Ban quản trị,<br>
{{setting('site.title')}}
@endcomponent