@extends('layouts.main') 
@section('content')
<main>
    <div class="pmain">
        <div class="incategory">
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="{{route('home')}}">Home</a></li>
                    
                    @include('components.scrumb')
                </ol>
            </div>
            <!-- .container -->
        </div>
        <!-- .incategory -->

        {{-- STAFF BANNER --}}
        @include('components.staff-banner')
        {{-- /.STAFF BANNER --}}

        <div class="container">
            <div class="incategory-list">
                <div class="row">
                    <div class="col-md-9 col-md-push-3">
                        @if(isset($posts))
                        <div class="in-whitle">
                            <div class="list-post">

                                @forelse($posts as $post)
                                    <div class="item">
                                        <a class="cover" href="{{route('post.detail', ['slug'=>$post->slug])}}" title="{{$post->title}}">
                                            <img src="{{asset("storage/{$post->image}")}}" alt="{{$post->title}}">
                                        </a>
                                        <div class="item-content">
                                            <h2>
                                                <a href="{{route('post.detail', ['slug'=>$post->slug])}}" title="{{$post->title}}">
                                                    {{$post->title}}
                                                </a>
                                            </h2>
                                            <p class="timestamp">{{$post->created_at->format('d-m-Y H:i')}}</p>
                                            <p class="summary">
                                                {{$post->excerpt}}
                                            </p>
                                        </div>
                                    </div>
                                @empty
                                    <h5>Chưa có bài viết</h5>
                                @endforelse

                            </div>
                            <div class="text-center">
                                {{$posts->links()}}
                            </div>

                        </div>
                        @elseif(isset($post))
                        <div class="in-detail-post">
                            <h1 class="mTitle">{{$post->title}}</h1>
                            <div class="post-content empty-content">
                                {!! $post->body !!}
                            </div>
                        </div>
                        @endif
                    </div>
                    
                    {{-- CATEGORY --}}
                    <div class="col-md-3 col-md-pull-9">
                        <div class="sidebar bg-white">
                            <div class="sidebar_widget_news">
                                <h3 class="sidebar_widget_title">
                                    <a href="{{route('post')}}">Hỏi Đáp Hướng Dẫn Và Tin Tức</a>
                                </h3>
                                @if($postCategories->isNotEmpty())
                                <ul class="side-menu">
                                    @foreach($postCategories  as $category)
                                    <li class="{{(request()->segment(2) == $category->slug) ? 'active' : ''}}">
                                        <a href="{{route('post.category', ['categorySlug' => $category->slug])}}">{{$category->name}}</a>
                                    </li>
                                    @endforeach
                                </ul>
                                @endif
                            </div>

                        </div>

                        <div class="sidebar">
                        </div>

                    </div>
                    {{-- .CATEGORY --}}

                </div>
            </div>
        </div>
        <!-- .container -->
    </div>
    <!-- .pmain -->
</main>
@endsection