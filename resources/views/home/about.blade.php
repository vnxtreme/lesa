@extends('layouts.main') 
@section('content')
<div class="title-box">
    <div class="container">
        <h1 class="title-style">@lang('translation.about-us')</h1>
        <ul class="breadcrumb">
            <li>
                <a href="{{url('/')}}" title="@lang('translation.menu.home')">@lang('translation.menu.home')</a>
            </li>
            <li>
                <a href="#" title="@lang('translation.about-us')">@lang('translation.about-us')</a>
            </li>
        </ul>
    </div>
</div>
<div class="container">
    <div class="row">
        <div id="content" class="col-sm-12"  style="line-height: 2.2rem; margin: 6rem auto;">
            {!!nl2br($introduction->content)!!}
        </div>
    </div>
</div>

@endsection
