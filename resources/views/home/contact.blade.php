@extends('layouts.main') @section('content')
@push('styles')
    <style type="text/css">
        #google_map_div_0 {
            width: 100%;
            height: 400px;
            border: 6px solid #f4f4f4;
            padding: 0;
            margin: 0 0 10px;
        }
    </style>
@endpush

@push('scripts')
@php
    $protocol = 'http';
    Request::secure() ? $protocol = 'https' : $protocol;
@endphp
    <script async defer src="{{$protocol}}://maps.google.com/maps/api/js?key=AIzaSyCoOO6FO1LGjd3lt70JbgrHvD5CJolskv4" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var geocoder;
            var address = "{{$information->address}}";

            function codeAddress(geocoder, map) {
                geocoder.geocode({'address': address}, function(results, status) {
                    if (status === 'OK') {
                        map.setCenter(results[0].geometry.location);
                        var marker = new google.maps.Marker({
                            map: map,
                            position: results[0].geometry.location,
                            // title: 'Uluru (Ayers Rock)'
                        });

                        var infowindow = new google.maps.InfoWindow({
                            content: '<div>{{$information->address}}</div>'
                        });

                        infowindow.open(map, marker);
                        marker.addListener('click', function(){
                            infowindow.open(map, marker);
                        })
                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }

            (function initMap() {   
                var map = new google.maps.Map(document.getElementById('google_map_div_0'), {
                    zoom: 17,
                    center: {lat: 0, lng: 0}
                });

                geocoder = new google.maps.Geocoder();
                codeAddress(geocoder, map);
            })();
        });
    </script>
@endpush

@push('scripts-bottom')
    <script>
        function onSubmiting(){
            let data = $('#contactForm').serialize();
            
            $.ajax({
                method: 'POST',
                url: url+'/post-contact',
                data: data,
                beforeSend: function(){
                    $('.formSubmit').find('i').removeClass('hidden');
                },
                success: function(result){
                    $('.display-message').html(`<p class="alert alert-success">${result}</p>`);
                    $('.formSubmit').find('i').addClass('hidden');
                },
                error: function(jqXHR, textStatus, errorThrown){
                    let html = "";
                    if(jqXHR.status == 422){
                        for (const error in jqXHR.responseJSON.errors) {
                            if (jqXHR.responseJSON.errors.hasOwnProperty(error)) {
                                const element = jqXHR.responseJSON.errors[error];
                                html += `<li>${element[0]}</li>`;
                            }
                        }
                    } 
                    else {
                        html = `<li>${errorThrown}</li>`;
                    }
                    $('.display-message').html(`<ul class="alert alert-danger">${html}</ul>`);
                    $('.formSubmit').find('i').addClass('hidden');
                }
            })
        }
    </script>
@endpush

<div class="title-box">
    <div class="container">
        <h1 class="title-style">Liên hệ với chúng tôi</h1>
        <ul class="breadcrumb">
            <li>
                <a href="http://3268.chilishop.net/index.php?route=common/home" alt="@lang('translation.menu.home')" title="@lang('translation.menu.home')" >@lang('translation.menu.home')</a>
            </li>
            <li>
                <a href="http://3268.chilishop.net/index.php?route=information/contact" alt="@lang('translation.menu.contact')" title="@lang('translation.menu.contact')" >@lang('translation.menu.contact')</a>
            </li>
        </ul>
    </div>
</div>
<div class="container">
    <div class="row">
        <div id="content" class="col-sm-12">
            <div class="position-display">
                
                <div id="google_map_div_0"></div>
                
                <div class="content_info add_contact">
                    <div class="row content-column">
                        <div class="item-content col-md-5">
                            <div class="content_inner">
                                <div class="item-description">@lang('translation.address'): {{$information->address}}</div>
                            </div>
                        </div>
                        <div class="item-content col-md-4">
                            <div class="content_inner">
                                <div class="item-title">
                                    <h3>Email: {{$information->email1}}</h3>
                                </div>
                                <div class="item-description">Hotline: {{$information->hotline}} </div>
                            </div>
                        </div>
                        <div class="item-content col-md-3">
                            <div class="content_inner">
                                <div class="item-title">
                                    <h3>{!!$information->working_hours!!}</h3>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="position-display">
            </div>
            <form id="contactForm" action="" method="POST" enctype="multipart/form-data" class="form-horizontal">
                <fieldset>
                    <div class="title">
                        <h3>@lang('translation.your-info'):</h3>
                    </div>
                    <div class="display-message">
                    </div>
                    <div class="form-group required">
                        <label class="col-md-2  col-sm-3 control-label" for="input-name">Họ & Tên:</label>
                        <div class="col-md-10 col-sm-9">
                            <input type="text" name="name" value="" id="input-name" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-md-2  col-sm-3 control-label" for="input-email">Địa chỉ E-Mail :</label>
                        <div class="col-md-10 col-sm-9">
                            <input type="text" name="email" value="" id="input-email" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-md-2  col-sm-3 control-label" for="input-enquiry">Nội dung:</label>
                        <div class="col-md-10 col-sm-9">
                            <textarea name="enquiry" rows="5" id="input-enquiry" class="form-control"></textarea>
                        </div>
                    </div>
                </fieldset>
                <div class="buttons">
                    <div class="pull-right">
                        <a href="javascript:;" class="btn btn-primary formSubmit" onclick="onSubmiting()">@lang('translation.send') <i class="fa fa-spinner fa-spin fa-fw hidden" aria-hidden="true"></i></a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
