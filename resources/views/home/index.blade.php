@extends('layouts.main')
@section('content')
<main>
    <div class="pmain">
        {{-- SLIDER --}}
        @include('components.slider')
        <!-- /.SLIDER -->

        {{-- STAFF BANNER --}}
        @include('components.staff-banner')
        {{-- /.STAFF BANNER --}}

        {{-- LIST CATEGORY AND PRODUCT LIST --}}
        @if($categories->isNotEmpty())
            @foreach ($categories as $key => $category)
                @if($products[$key]->isNotEmpty()))
                <div class="section-product">
                    <div class="container">
                        <div class="tab-menu clearfix">
                            <h2 class="name"><a href="{{route('product.categorySlug', ['categorySlug' => $category->slug])}}">{{$category->name}}</a></h2>
                            <div class="tab-menu-content">
                                <ul class="tab-menu-width">
                                    <li><a href="#tab-z1-{{$category->id}}" data-toggle="tab" onclick="show_ajax_homepage({{$category->id}}, 'latest'); return false;">Mới nhất</a></li>

                                    <li><a href="#tab-z2-{{$category->id}}" data-toggle="tab" onclick="show_ajax_homepage({{$category->id}}, 'bestseller'); return false;">Bán Chạy</a></li>

                                    <li><a href="#tab-z3-{{$category->id}}" data-toggle="tab" onclick="show_ajax_homepage({{$category->id}}, 'bestprice'); return false;">Giá tốt</a></li>

                                    <li class="active"><a href="#tab-z4-{{$category->id}}" data-toggle="tab" onclick="show_ajax_homepage({{$category->id}}, 'hot'); return false;">Chọn lọc</a></li>

                                    <li><a href="#tab-z5-{{$category->id}}" data-toggle="tab" onclick="show_ajax_homepage({{$category->id}}, 'mostview'); return false;">Xem Nhiều</a></li>

                                    {{-- <li><a href="#tab-z6-{{$category->id}}" data-toggle="tab" onclick="show_ajax_homepage({{$category->id}}, 'mostlike'); return false;">Yêu Thích</a></li> --}}
                                </ul>
                            </div>
                            <a href="{{route('product.categorySlug', ['categorySlug' => $category->slug])}}" class="right viewall">Xem tất cả</a>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane active tab-latest-{{$category->id}}" id="tab-z1-{{$category->id}}">
                                <div class="row list-product">
                                    @foreach($products[$key] as $product)
                                        @if(isset($product->category->slug))
                                        <div class="col-xs-6 col-sm-4 col-md-3">
                                            <div class="item">
                                                <div class="cover imgproduct">
                                                    <a href="{{route('product.categorySlug.id', ['categorySlug' => $product->category->slug, 'id' => $product->id])}}" title="{{$product->name}}">
                                                        <img src="{{asset('images/lazy.jpg')}}" data-src="{{asset("storage/{$product->thumbnail('medium', 'image')}")}}" class="imgresponsive lazy" alt="{{$product->name}}" />
                                                    </a>
                                                </div>
                                                <h3>
                                                    <a href="{{route('product.categorySlug.id', ['categorySlug' => $product->category->slug, 'id' => $product->id])}}" title="{{$product->name}}">
                                                        {{$product->name}}
                                                    </a>
                                                </h3>

                                                @include('components.price')

                                            </div>
                                        </div>
                                        @endif
                                    @endforeach
                                </div>
                                
                                @if(count($products[$key]) > 8)
                                    <div class="view-all"><a href="{{route('product.categorySlug', ['categorySlug' => $category->slug])}}">Xem tất cả</a></div>
                                @endif
                            </div>
                            <div class="tab-pane  tab-bestseller-{{$category->id}}" id="tab-z2-{{$category->id}}">
                            </div>
                            <div class="tab-pane  tab-bestprice-{{$category->id}}" id="tab-z3-{{$category->id}}">
                            </div>
                            <div class="tab-pane  tab-hot-{{$category->id}}" id="tab-z4-{{$category->id}}">
                            </div>
                            <div class="tab-pane  tab-mostview-{{$category->id}}" id="tab-z5-{{$category->id}}">
                            </div>
                            {{-- <div class="tab-pane  tab-like-1" id="tab-z6-1">
                            </div> --}}

                        </div>

                    </div>
                </div>
                @endif
            @endforeach
        @endif
    </div>
</main>
@endsection
