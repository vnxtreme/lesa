<div id="column-left" class="col-md-3 col-lg-3 col-xs-12 hidden-xs hidden-sm">
    <div class="menu_category">
        <div class="navbar-header">
            <h3 id="category">@lang('translation.category')</h3>
        </div>
        <div class="">
            <ul class="nav">
                @foreach ($categories as $category)
                    <li>
                        <a href="{{route("product.categorySlug", ['categorySlug' => $category->slug] )}}">{{$category->name}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>

    @if(count($latestProducts) > 0)
    <div class="dv-builder-full">
        <div class="dv-builder list_product product-sidebar">
            <div class="dv-module-content">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                        <div class="dv-item-module ">
                            <div class="product_module latest_product">
                                <h3 class="title title_latest">
                                    <span>@lang('translation.latest')</span>
                                </h3>
                                <div class="product_module_content">
                                    <div class="row product-layout-custom">
                                        @foreach ($latestProducts as $latest)
                                        <div class="medium col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="product-thumb transition">
                                                    <div class="image">
                                                        <a href="{{route("product.categorySlug.id", ['categorySlug' => $latest->categorySlug, 'id' => $latest->id])}}">
                                                            <img src="{{asset("storage/{$latest->image}")}}" alt="{{$latest->name}}" title="{{$latest->name}}" class="img-responsive" />
                                                        </a>
                                                        @if($latest->old_price > 0)
                                                            <div class="status-sale"> {{round(($latest->current_price*100/$latest->old_price) - 100)}}%</div>
                                                        @endif
                                                    </div>
                                                    <div class="caption">
                                                        <h4>
                                                            <a href="{{route("product.categorySlug.id", ['categorySlug' => $latest->categorySlug, 'id' => $latest->id])}}">{{$latest->name}}</a>
                                                        </h4>
                                                        @if($latest->old_price > 0)
                                                            <div class="price">
                                                                <span class="price-old">{{number_format($latest->old_price, 0, ',', '.')}} VNĐ</span>
                                                                <span class="price-new">{{number_format($latest->current_price, 0, ',', '.')}} VNĐ</span>
                                                            </div>
                                                        @else
                                                            <div class="price">{{number_format($latest->current_price, 0, ',', '.')}} VNĐ </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>    
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if(isset($banners->product_ads))
    <div class="banner-box">
        <div id="banner0" class="banner_main">
            <div class="banner-item">
                <div class="banner-inner">
                    <a href="{{$banners->product_ads_link}}" target="_blank">
                        <img src="{{asset("storage/{$banners->product_ads}")}}" alt="{{$banners->product_ads_title}}" class="img-responsive" />
                    </a>
                    <span class="promo-text">
                        <span class="title">{{$banners->product_ads_title}}</span>
                    </span>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
