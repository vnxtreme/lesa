<div class="box box-no-padding">
    <div class="box_header">
        <h2 class="box_title">Thông tin chung</h2>
    </div>
    <div class="box_body">
        <div class="widget widget-profile">
            <div class="profile clearfix">
                <div class="profile_avatar">

                    <img src="{{asset("storage/{$user->avatar}")}}" class="imgresponsive" alt="{{$user->name}}">
                </div>
                <div class="profile_info">
                    <div class="profile_name"></div>
                </div>
            </div>
        </div>
        <div class="sidebar_widget widget">
            <ul class="side-menu">
                <li class="{{Route::currentRouteName() == 'user.profile' ? 'active' : ''}}"><a href="{{route('user.profile')}}">Thông tin tài khoản</a></li>
                <li class="{{Route::currentRouteName() == 'user.address' ? 'active' : ''}}"><a href="{{route('user.address')}}">Địa chỉ giao hàng</a></li>
                <li class="{{(Route::currentRouteName() == 'user.orders' OR Route::currentRouteName() == 'user.orders.detail') ? 'active' : ''}}"><a href="{{route('user.orders')}}">Thông tin đơn hàng</a></li>
                {{-- <li><a href="/san-pham-ghi-nho.html">Sản phẩm ghi nhớ</a></li> --}}
                {{-- <li><a href="/danh-gia-san-pham-da-mua.html">Đánh giá sản phẩm đã mua</a></li> --}}
                <li class="{{Route::currentRouteName() == 'user.password.change' ? 'active' : ''}}"><a href="{{route('user.password.change')}}">Thay đổi mật khẩu</a></li>
                <li class="show-in-checkout">
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.querySelector('.logout-form').submit();">Đăng xuất</a>
                </li>
            </ul>
            <form class="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>

    </div>
</div>