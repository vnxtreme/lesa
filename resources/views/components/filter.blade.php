<div class="filter">
    <span class="filter_title"><i class="fa fa-sort-amount-asc"></i> <span>SẮP XẾP THEO</span></span>
    <a class="sorting {{(!$type OR $type == 'default') ? 'sorting-active' : ''}} {{($type == 'default' and $sort=='desc') ? 'asc' : 'desc'}}" href="{{request()->url()."?type=default&sort=".($sort=='desc' ? 'asc' : 'desc')}}&keywords={{$keywords ?? ''}}">Mặc định</a>
    <a class="sorting {{($type == 'price') ? 'sorting-active' : ''}} {{($type == 'price' and $sort=='desc') ? 'asc' : 'desc'}}" href="{{request()->url()."?type=price&sort=".($sort=='desc' ? 'asc' : 'desc')}}&keywords={{$keywords ?? ''}}">Giá tiền</a>
    <a class="sorting {{($type == 'view') ? 'sorting-active' : ''}} {{($type == 'view' and $sort=='desc') ? 'asc' : 'desc'}}" href="{{request()->url()."?type=view&sort=".($sort=='desc' ? 'asc' : 'desc')}}&keywords={{$keywords ?? ''}}">Lượt xem</a>
    <a class="sorting {{($type == 'bestseller') ? 'sorting-active' : ''}} {{($type == 'bestseller' and $sort=='desc') ? 'asc' : 'desc'}}" href="{{request()->url()."?type=bestseller&sort=".($sort=='desc' ? 'asc' : 'desc')}}&keywords={{$keywords ?? ''}}">Mua nhiều</a>
</div>