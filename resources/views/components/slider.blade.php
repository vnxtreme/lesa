<div class="section-slider">
    <div class="container">
        <div class="promo-slider">
            <div class="promo-left">
                <div class="slider-home">
                    <div class="owl-carousel">
                        @if($carousels)
                            @foreach ($carousels as $carousel)
                            <div class="item">
                                <a href="{{$carousel->link ? $carousel->link : 'javascript:;'}}">
                                    <img src="{{"storage/$carousel->image"}}" />
                                </a>
                            </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                @if($bottomImages)
                <div class="promo-banners-bottom">
                    <div class="owl-carousel">
                        @foreach ($bottomImages as $bottom)
                            <div class="item">
                                <a target="_blanks" href="{{$bottom->link ?? 'javascript:;'}}" title="{{$bottom->name}}">
                                    <img src="{{asset("storage/{$bottom->image}")}}" alt="{{$bottom->name}}">
                                </a>
                            </div>    
                        @endforeach
                    </div>
                </div>
                @endif
            </div>

            <div class="promo-banners">
                @if($rightImages)
                <div class="row">
                    @foreach ($rightImages as $right)
                    <div class="col-xs-6 col-sm-6 col-md-12">
                        <div class="item">
                            <a target="_blank" href="{{$right->link ?? 'javascript:;'}}" title="{{$right->name}}">
                                <img src="{{asset("storage/$right->image")}}" class="imgresponsive" alt="{{$right->name}}" />
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endif
            </div>
        </div>
        <!-- /.promo-slider -->
    </div>
</div>