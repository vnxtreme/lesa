@php 
$sort = request()->input('sort'); 
$order = request()->input('order'); 
$limit = request()->input('limit'); 

function createLimitUrl ($value, $limit, $sort, $order){
    if(!$limit): 
        return request()->fullUrl()."&limit={$value}";
    else:
        return request()->url()."?sort={$sort}&order={$order}&limit={$value}";
    endif;
}
@endphp


<div class="shop-grid-controls">
    <div class="entry hidden-md hidden-sm hidden-xs">
        <button type="button" id="grid-view" class="view-button grid active" data-toggle="tooltip" title="@lang('translation.grid')">
        <i class="fa fa-th"></i>
    </button>
        <button type="button" id="list-view" class="view-button list" data-toggle="tooltip" title="@lang('translation.list')">
        <i class="fa fa-list"></i>
    </button>
    </div>

    <div class="entry hidden-md hidden-sm hidden-xs">
        <div class="inline-text">@lang('translation.arrange'):</div>
        <div class="simple-drop-down">
            <select id="input-sort" onchange="location = this.value">
            <option {{$sort && $order ? '' : 'selected'}} value="">@lang('translation.default')</option>
            <option {{ $sort == 'name' && $order == 'asc' ? 'selected' : '' }} 
                value="{{  $sort == 'name' && $order == 'asc' ? request()->fullUrl() : request()->url().'?sort=name&order=asc' }}"
            >
                @lang('translation.filter_name_a_z')
            </option>
            <option {{ $sort == 'name' && $order == 'desc' ? 'selected' : '' }}
                value="{{  $sort == 'name' && $order == 'desc' ? request()->fullUrl() : request()->url().'?sort=name&order=desc' }}"
            >
                @lang('translation.filter_name_a_z')
            </option>
            <option {{ ($sort == 'current_price' && $order == 'asc') ? 'selected' : '' }}
                value="{{ $sort == 'current_price' && $order == 'asc' ? request()->fullUrl() : request()->url().'?sort=current_price&order=asc' }}"
            >
                @lang('translation.filter_price_low_high')
            </option>
            <option {{ ($sort == 'current_price' && $order == 'desc') ? 'selected' : '' }}
                value="{{ $sort == 'current_price' && $order == 'desc' ? request()->fullUrl() : request()->url().'?sort=current_price&order=desc' }}"
            >
                @lang('translation.filter_price_high_low')
            </option>
        </select>
        </div>
    </div>

    <div class="entry">
        <div class="inline-text">@lang('translation.display'):</div>
        <div class="simple-drop-down display_number_c">
            <select id="input-limit" onchange="location = this.value">
                <option {{ $limit ? '' : 'selected' }} value="{{ !$sort ? request()->url().'?limit=12' : createLimitUrl(12, $limit, $sort, $order) }}">12</option>
                <option {{ $limit == 25 ? 'selected' : '' }} value="{{ !$sort ? request()->url().'?limit=25' : createLimitUrl(25, $limit, $sort, $order) }}">25</option>
                <option {{ $limit == 50 ? 'selected' : '' }} value="{{ !$sort ? request()->url().'?limit=50' : createLimitUrl(50, $limit, $sort, $order) }}">50</option>
                <option {{ $limit == 75 ? 'selected' : '' }} value="{{ !$sort ? request()->url().'?limit=75' : createLimitUrl(75, $limit, $sort, $order) }}">75</option>
                <option {{ $limit == 100 ? 'selected' : '' }} value="{{ !$sort ? request()->url().'?limit=100' : createLimitUrl(100, $limit, $sort, $order) }}">100</option>
            </select>
        </div>
    </div>
</div>