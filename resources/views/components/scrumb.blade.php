@if(isset($scrumbs))
    @foreach ($scrumbs as $scrumb)
        <li class="no-hover">
            {{ isset($scrumb['name']) ? $scrumb['name'] : $scrumb}}
        </li>    
    @endforeach
@endif