<div class="main-menu" id="main-menu">
    <div class="container">
        <div class="position-display">
            <div class="dv-builder-full" id='logo_menu'>
                <div class="dv-builder logo-menu">
                    <div class="dv-module-content">
                        <div class="row">
                            <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                <div class="dv-item-module ">
                                    <div id="logo">
                                        <a href="{{route('home')}}">
                                            <img src="{{asset('storage/'.setting('site.logo'))}}" title="{{setting('site.title')}}" alt="{{setting('site.title')}}" class="img-responsive "/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-md-10 col-lg-10 col-xs-12">
                                <div class="dv-item-module ">

                                    <div class="close-header-layer"></div>
                                    <div class="menu_horizontal" id="menu_id_MB01">
                                        <div class="navbar-header">
                                            <button type="button" data-toggle="collapse" data-target="#navbar-collapse-MB01" class="navbar-toggle">
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>
                                        <div class="mask-container"></div>
                                        <div id="navbar-collapse-MB01" class="menu-content">
                                            <div class="close-menu"></div>
                                            <ul class="menu">
                                                <li>
                                                    <a href="{{route('home')}}">
                                                        <span>@lang('translation.menu.home')</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{route( 'about' )}}">
                                                        <span>@lang('translation.menu.about')</span>
                                                    </a>
                                                </li>
                                                <li class="hchild">
                                                    <a>
                                                        <span>@lang('translation.menu.products')</span>
                                                    </a>
                                                    <span class="expander "></span>
                                                    <ul class="sub-menu ">
                                                        @foreach ($categories as $category)
                                                            <li>
                                                                <a href="{{route('product.categorySlug', ['categorySlug' => $category->slug] )}}">{{$category->name}}</a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="{{route('contact')}}">
                                                        <span>@lang('translation.menu.contact')</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{route('switchLanguage')}}" >
                                                        <img src="{{asset("images/icons/".config('app.switch')[$locale].".png")}}" alt="{{config('app.languages.'.config('app.switch')[$locale])}}" title="{{config('app.languages.'.config('app.switch')[$locale])}}">
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <script>
                                        $(function () {
                                            window.prettyPrint && prettyPrint()
                                            $(document).on('click', '.navbar .dropdown-menu',
                                                function (e) {
                                                    e.stopPropagation()
                                                })
                                        })

                                    </script>
                                </div>

                                <div class="dv-item-module ">
                                    <div id="search_box">
                                        <i class="fa fa-search"></i>
                                            <form method="POST" action="{{route('search')}}">
                                                @csrf
                                                <div id="search" class="input-group">
                                                    <input type="text" name="search" value="" placeholder="@lang('translation.search')" class="form-control input-lg" />
                                                    <span class="button_search">
                                                        <button type="submit" class="btn btn-default btn-lg">
                                                            <i class="fa fa-search"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                
                                <div class="dv-item-module ">
                                    <div class="cart" class="btn-group btn-block">
                                        <button type="button" data-toggle="dropdown" data-loading-text="@lang('translation.processing')" class="btn btn-inverse btn-block btn-lg dropdown-toggle">
                                            <i class="fa fa-shopping-cart"></i>
                                            <span id="cart-total">
                                                <span class="num_product">{{$totalQuantity ?? 0}}</span>
                                            </span>
                                        </button>

                                        <ul class="dropdown-menu pull-right">
                                            @if($sessionCart and count($sessionCart)>0)
                                                @foreach ($sessionCart as $cart)
                                                    @include('components.list-of-added-product')
                                                @endforeach
                                                <li>
                                                    <div class='table-responsive nbd'>
                                                        <table class='table tbl-total'>
                                                            <tr>
                                                                <td class='text-right'><strong>@lang('translation.sub_total')</strong></td>
                                                                <td class='text-right'>{{number_format($totalPrice, 0, ',', '.')}} VNĐ</td>
                                                            </tr>
                                                            <tr>
                                                                <td class='text-right'><strong>@lang('translation.total') </strong></td>
                                                                <td class='text-right'>{{number_format($totalPrice, 0, ',', '.')}} VNĐ</td>
                                                            </tr>
                                                        </table>
                                                        <p class='btn-cart'>
                                                            <a class='cout' href='{{route('payment')}}'><strong><i class='fa fa-share'></i> @lang('translation.payment')</strong></a>
                                                        </p>
                                                    </div>
                                                </li>
                                            @else
                                            <li>
                                                <p class="empty">@lang('translation.empty_cart')</p>
                                            </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>