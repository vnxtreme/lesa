<div class="position-banners">
    <div class="container">
        <div class="item">
            <a target="_blank" href="{{route('home')}}" title="{{setting('site.title')}}">
                <img src="{{asset('images/lesa-company.jpg')}}" class="imgresponsive" alt="{{setting('site.title')}}" />
            </a>
        </div>
    </div>
</div>