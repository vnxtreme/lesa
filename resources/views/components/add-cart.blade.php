<div class="button-group">
    <div class="btng-inner">
        <button class="bcart" type="button" title="Thêm vào giỏ" onclick="cart.add(this, {{$id}});">
            <i class="fa fa-shopping-cart"></i>
            <span>Thêm vào giỏ</span>
            <input type="hidden" name="unique_id" value="{{$unique_id ?? 0}}">
        </button>
    </div>
</div>