<header>
    <div id="pheader">
        <div class="head-topbar">
            <div class="container">
                <div class="topbar-container">
                    <div class="right">
                        <ul class="topbar-support list-unstyled left">
                            <li><a href="tel:{{ str_replace(' ','',$information['hotline']) }}"><i class="fa fa-phone"></i> Hotline: <span class="color-redphone">{{$information['hotline']}}</span></a></li>
                            <li><a href="{{$information['facebook']}}"><i class="fa fa-question-circle"></i> Hỗ trợ trực tuyến</a></li>
                        </ul>
                        <div class="topbar-account left">
                            @guest
                                <span>
                                    <span class="icon-usser"><i class="fa fa-user-o"></i></span>
                                    <span class="left"><b>Đăng nhập</b><br><small>Tài khoản và đơn hàng</small></span>
                                </span>
                                <input type="hidden" id="statusInSite" value="off">
                                <div class="box-account">
                                    <ul class="account-list">
                                        <li><a href="#" data-toggle="modal" data-target="#myLogin">Đăng nhập</a></li>
                                        <li><a href="{{ route('register') }}">Tạo tài khoản</a></li>
                                    </ul>
                                </div>
                            @else
                                <div class="dropdown dropdown-arrow login-active">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                        <i class="fa fa-user-o"></i> {{Auth::user()->name}} <i class="fa fa-caret-down"></i>
                                    </a>
                                    <input type="hidden" id="statusInSite" value="on">
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{route('user.profile')}}" class="underlined">Thông tin tài khoản</a></li>
                                        <li><a href="{{route('user.address')}}">Địa chỉ giao hàng</a></li>
                                        <li><a href="{{route('user.orders')}}" class="underlined">Thông tin đơn hàng</a></li>
                                        <li><a href="{{route('user.password.change')}}">Thay đổi mật khẩu</a></li>
                                        <li class="show-in-checkout">
                                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Đăng xuất</a>
                                        </li>
                                    </ul>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            @endguest
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container -->
        </div><!-- /.head-topbar -->

        <div class="header-main">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="logo">
                            <h1>
                                <a href="{{route('home')}}">
                                    <img src="{{asset('storage/'.setting('site.logo'))}}" class="hvr-forward" alt="{{setting('site.description')}}" height="80" />
                                </a>
                            </h1>
                        </div>
                        <a class="animated-mobile"><span></span></a>
                        <div class="header-loginbox">
                            <span class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"><i class="fa fa-user-o"></i></a>
                                <div class="dropdown-menu">
                                    <div class="box-account ">
                                        <ul class="account-list">
                                            @guest
                                                <li><a href="#" data-toggle="modal" data-target="#myLogin">Đăng nhập</a></li>
                                                <li><a href="{{ route('register') }}">Tạo tài khoản</a></li>
                                            @else
                                                <li><a href="{{route('user.profile')}}" class="underlined">Thông tin tài khoản</a></li>
                                                <li><a href="{{route('user.address')}}">Địa chỉ giao hàng</a></li>
                                                <li><a href="{{route('user.orders')}}" class="underlined">Thông tin đơn hàng</a></li>
                                                <li><a href="{{route('user.password.change')}}">Thay đổi mật khẩu</a></li>
                                                <li class="show-in-checkout">
                                                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Đăng xuất</a>
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </span>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <div class="header-search">
                            <div id="mallSearch">
                                <form action="{{route('search')}}" method="get">
                                    <div class="header_search_form">
                                        <input class="fomr_field" type="text" name="keywords" id="keyword" placeholder="Tìm kiếm sản phẩm..." autocomplete="off">
                                        <button class="btn btn-submit" type="submit"></button>
                                    </div>
                                    <a class="btn btn-cancel">Hủy</a>
                                </form>
                            </div>
                            <div id="autocomplete"></div>
                        </div>
                    </div>
                    <div class="col-sm-2 cart-move">

                        <span class="header-cart dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true">
                                <i class="lnr lnr-cart lnr-lg"></i>
                                <span class="circle">{{$totalQuantity}}</span>
                                <span class="hidden-xs hidden-sm">GIỎ HÀNG</span>
                            </a>
                            {{-- POPUP CART --}}
                            <ul class="dropdown-menu dropdown-cart">
                                <li class="minicart_wrapper">
                                    @if($sessionCart and count($sessionCart)>0)
                                        @foreach ($sessionCart as $cart)
                                            @include('components.list-of-added-product')
                                        @endforeach
                                    @else
                                        <h4 class="empty text-center">@lang('translation.empty_cart')</h4>
                                    @endif
                                </li>
                            
                                <li class="minicart_summary">
                                    Tổng cộng:
                                    <span class="price">
                                        <span class="price_value">{{number_format($totalPrice, 0, ',', '.')}}</span>
                                        <span class="price_symbol">đ</span>
                                    </span>
                                </li>
                                <li class="minicart_actions clearfix">
                                    <a class="btn btn-success btn-buy-now btn-buy-now-new" href="{{route('cart')}}" rel="nofollow">Đặt hàng</a>
                                </li>
                            </ul>
                            {{-- POPUP CART --}}

                        </span>
                        <!-- /.header-cart -->
                    </div>
                </div>
            </div>
            <!-- /.container -->
        </div><!-- /.header-main -->

        {{-- NAVIGATION --}}
        <div class="header-wsmenu">
            <div class="container clearfix">

                <div class="nav-main">
                    <nav class="wsmenu clearfix">
                        <ul class="mobile-sub wsmenu-list">

                            <li class="red-cate {{Route::currentRouteName() == 'home' ? 'active' : '' }}">
                                <a class="bg-red" href="#">Danh mục</a>
                                <ul class="wsmenu-submenu">
                                    @foreach ($categories as $parentCategory)
                                        <li>
                                            <a href="{{route('product.categorySlug', ['categorySlug' => $parentCategory->slug])}}">{{$parentCategory->name}}</a>
                                            @if (count($parentCategory->childCategories) > 0 )
                                                <div class="wsmenu-submenu-sub">
                                                    <div class="mega-sub-menu">
                                                        <ul>
                                                            @foreach ($parentCategory->childCategories as $childCategory)
                                                                <li class="submenu-heading">
                                                                    <a href="{{route('product.categorySlug', ['categorySlug' => $childCategory->slug])}}">{{mb_strtoupper($childCategory->name, 'UTF-8')}}</a>
                                                                </li>
                                                                @if(count($childCategory->childCategories) > 0)
                                                                    @foreach ($childCategory->childCategories as $child)
                                                                        <li>
                                                                            <a href="{{route('product.categorySlug', ['categorySlug' => $child->slug])}}">{{$child->name}}</a>
                                                                        </li>
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            <li><a href="{{route('home')}}">Trang Chủ</a></li>
                            <li><a href="{{route('product.categorySlug', ['categorySlug' => 'xem-tat-ca'])}}">Tất Cả Sản Phẩm</a></li>
                            <li><a href="{{route('post')}}">Hỏi Đáp Hướng Dẫn Và Tin Tức</a></li>

                        </ul>
                    </nav>
                </div>
            </div>
            <!-- /.container -->
        </div><!-- /.header-wsmenu -->
        {{-- ./NAVIGATION --}}

        <div class="header-scroll wsmenu sticky-header">
            <div class="container">
                <ul class="main-nav mobile-sub wsmenu-list">
                    <li class="red-cate">
                        <a class="bg-red" href="#">Danh mục</a>
                        <ul class="wsmenu-submenu">
                            
                            @foreach ($categories as $parentCategory)
                                <li>
                                    <a href="{{route('product.categorySlug', ['categorySlug' => $parentCategory->slug])}}">{{$parentCategory->name}}</a>
                                    @if (count($parentCategory->childCategories) > 0 )
                                        <div class="wsmenu-submenu-sub">
                                            <div class="mega-sub-menu">
                                                <ul>
                                                    @foreach ($parentCategory->childCategories as $childCategory)
                                                        <li class="submenu-heading">
                                                            <a href="{{route('product.categorySlug', ['categorySlug' => $childCategory->slug])}}">{{mb_strtoupper($childCategory->name, 'UTF-8')}}</a>
                                                        </li>
                                                        @if(count($childCategory->childCategories) > 0)
                                                            @foreach ($childCategory->childCategories as $child)
                                                                <li>
                                                                    <a href="{{route('product.categorySlug', ['categorySlug' => $childCategory->slug])}}">{{$child->name}}</a>
                                                                </li>
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    @endif
                                </li>
                            @endforeach

                        </ul>
                    </li>
                    <li>
                        <div class="header-logo">
                            <a href="{{route('home')}}">
                                <img src="{{asset('storage/'.setting('site.logo'))}}" class="{{-- imgresponsive --}}" height="25" alt="{{setting('site.title')}}"></a>
                        </div>
                    </li>
                    <li>
                        <div class="header-search search-top">
                            <form action="{{route('search')}}" method="get">
                                <div class="header_search_form">
                                    <input class="fomr_field search-key" type="text" name="keywords" placeholder="Tìm kiếm sản phẩm..." autocomplete="off">
                                    <button class="btn btn-submit" type="submit"></button>
                                </div>
                            </form>
                            <div class="search-result"></div>
                        </div>
                    </li>
                    <li>
                        <div class="header-cart dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="lnr lnr-cart lnr-lg"></i>
                                <span class="circle">{{$totalQuantity}}</span>
                                <span class="hidden-xs hidden-sm">GIỎ HÀNG</span>
                            </a>
                            {{-- POPUP CART --}}
                            <ul class="dropdown-menu dropdown-cart">
                                <li class="minicart_wrapper">
                                    @if($sessionCart and count($sessionCart)>0)
                                        @foreach ($sessionCart as $cart)
                                            @include('components.list-of-added-product')
                                        @endforeach
                                    @else
                                        <h4 class="empty text-center">@lang('translation.empty_cart')</h4>
                                    @endif
                                </li>
                            
                                <li class="minicart_summary">
                                    Tổng cộng:
                                    <span class="price">
                                        <span class="price_value">{{number_format($totalPrice, 0, ',', '.')}}</span>
                                        <span class="price_symbol">đ</span>
                                    </span>
                                </li>
                                <div class="minicart_actions clearfix">
                                    {{-- <a class="btn-view-cart" href="{{route('payment')}}" rel="nofollow">Xem giỏ hàng</a> --}}
                                    <a class="btn btn-success btn-buy-now btn-buy-now-new" href="{{route('payment')}}" rel="nofollow">Đặt hàng</a>
                                </div>
                            </ul>
                            {{-- POPUP CART --}}
                        </div>
                        <!-- /.header-cart -->
                    </li>
                </ul>
            </div>
        </div>

        <div class="header-mobile">
            <div class="container clearfix">
                <div class="menu-container">

                    <div class="m-register">
                        <ul class="list-inline">
                            <li><a href="#" data-toggle="modal" data-target="#myLogin">Đăng nhập</a></li>
                            <li><a href="{{ route('register') }}">Đăng ký</a></li>
                        </ul>
                    </div>

                    <div class="m-nav">
                        <h3 class="mbtitle">Danh Mục</h3>
                        <ul class="m-category-list clearfix">
                            <li><a href="{{route('product.categorySlug', ['categorySlug' => 'xem-tat-ca'])}}">Tất Cả Sản Phẩm</a></li>
                            @foreach ($categories as $category)
                                <li>
                                    <a href="{{route('product.categorySlug', ['categorySlug' => $category->slug] )}}">{{$category->name}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="m-forsubject">
                        <h3 class="mbtitle">Nhóm chủ đề</h3>
                        <div class="m-forsubject-list">

                        </div>
                    </div>
                </div>
            </div><!-- /.container -->
        </div><!-- /.haeder-mobile-->

    </div>
</header>
