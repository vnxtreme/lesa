<div class="header-cart dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="lnr lnr-cart lnr-lg"></i>
        <span class="circle">{{count($sessionCart)}}</span>
        <span class="hidden-xs hidden-sm">GIỎ HÀNG</span>
    </a> 
    <ul class="dropdown-menu dropdown-cart">
        <li class="minicart_wrapper">
            {!!$listTtemHtml!!}
        </li>

        <li class="minicart_summary">
            Tổng cộng:
            <span class="price">
                <span class="price_value">{{number_format($totalPrice, 0, ',', '.')}}</span>
                <span class="price_symbol">đ</span>
            </span>
        </li>
        <li class="minicart_actions clearfix">
            <a class="btn btn-success btn-buy-now btn-buy-now-new" href="{{route('cart')}}" rel="nofollow">Xem giỏ hàng</a>
        </li>
    </ul>
</div>
