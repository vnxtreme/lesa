@php
    if($product->old_price > $product->current_price):
        $percentage = round(($product->old_price - $product->current_price )*100/$product->old_price);
    endif
@endphp
<div class="price">
    <div class="pd-price">
        <span class="price_value" itemprop="price">{{number_format($product->current_price, 0, ',', '.')}}</span>
        <span class="price_symbol">đ</span>
        @if(isset($percentage))
        <span class="price_discount">-{{ $percentage }}%</span>
        @endif
    </div>
    @if(isset($percentage))
    <div class="pd-price pd-price-old">
        <span class="price_value" itemprop="price">{{number_format($product->old_price, 0, ',', '.')}}</span>
        <span class="price_symbol">đ</span>
    </div>
    @endif
    <div class="pd-views"><i class="fa fa-user-o"></i> {{$product->view}}</div>
</div>