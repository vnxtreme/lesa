<div class="minicart_item">
    <a href="{{$cart->url}}" title="{{$cart->name}}">
        <img src="{{$cart->image}}" alt="{{$cart->name}}" class="imgresponsive">
    </a>
    <div class="minicart_item_name">
        <a href="{{$cart->url}}" title="{{$cart->name}}">
            {{$cart->name}}
        </a>
        <span class="minicart_item_price price">
            <span class="price_value">{{number_format($cart->price, 0, ',', '.')}}</span>
            <span class="price_symbol">đ</span>
            <span class="price_qty">x {{$cart->quantity}}</span>
        </span>
    </div>
    <div class="minicart_item_actions">
        <a onClick='cart.remove("{{$cart->product_id}}");' class="btn-close show-on-hover"><i class="fa fa-times"></i></a>
    </div>
</div>