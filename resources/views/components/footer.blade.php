<footer>
    {{-- MODAL --}}
    <div id="myLogin" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Đăng nhập</h4>
                </div>
                <div class="modal-body">
                    <form class="form form-registration" method="post" action="{{ route('login') }}">
                        @csrf
                        <div class="form_inner">
                            
                            <div class="form-group finuser {{$errors->has('email') ? 'has-error has-danger' : ''}}">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                    <input class="form-control" autocomplete="off" type="text" id="email" name="email" value="" placeholder="Email" data-error="Email không được để trống!" required>
                                </div>
                                @if ($errors->has('email'))
                                    <div class="help-block with-errors">
                                        <ul class="list-unstyled">
                                            <li>{{ $errors->first('email') }}</li>
                                        </ul>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group finpass">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                    <input class="form-control" autocomplete="off" type="password" id="password" name="password" placeholder="Mật khẩu" required>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group password-helper">
                                <label for="remember_me" class="pull-left remember-me">
                                    <input type="checkbox" name="remember_me" id="remember_me" value="Y" checked="checked" /> Ghi nhớ đăng nhập
                                </label>
                                <a class="pull-right" rel="nofollow" href="{{route('user.password.forget')}}">Quên mật khẩu?</a>
                            </div>
                            <div class="form-group form-group-lg">
                                <button type="submit" class="btn-green btn-block">ĐĂNG NHẬP</button>
                            </div>
                            
                            <p class="login-register-helper">Bạn chưa có tài khoản? <a rel="nofollow" href="{{route('register')}}">Ðăng ký ngay</a></p>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- .MODAL --}}

    <div id="footer">
        <div class="footer-info">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <div class="footer-address clk-footer">
                            <h3 class="name-heading clk-btn">THÔNG TIN LIÊN HỆ</h3>
                            <div class="clk-content" style="display: block;">
                                @foreach ($addresses as $address)
                                    <div class="cn-shop">
                                        <p>@if($address->headquarter) TRỤ SỞ CHÍNH: @endif {{$address->location}} </p>
                                        <p><b>Địa Chỉ</b> : {{$address->address}}</p>
                                        <p><b>Free-call:</b> {{$address->telephone}} @if($address->hotline)- Hotline : {{$address->hotline}} @endif</p>
                                        <p><b>E-mail:</b> {{$address->email}}</p>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-2">
                        <div class="footer-nav clk-footer">
                            <h3 class="name-heading clk-btn">Giới thiệu</h3>
                            <ul class="list-unstyled clk-content">
                                <li><a href="javascript:;">Về Chúng Tôi</a></li>
                                <li><a href="javascript:;">Hệ thống cửa hàng</a></li>
                                <li><a href="javascript:;">Tuyển dụng</a></li>
                                <li><a href="javascript:;">Tài Khoản Ngân Hàng</a></li>
                                <li><a href="javascript:;">Liên hệ</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-2">
                        <div class="footer-nav clk-footer">
                            <h3 class="name-heading clk-btn">HỖ TRỢ KHÁCH HÀNG</h3>
                            <ul class="list-unstyled clk-content">
                                <li><a target="_blank" href="{{route('post.category', ['categorySlug' => 'h-ong-dan-thanh-toan-va-mua-hang'])}}">Hướng dẫn mua hàng</a></li>
                                <li><a target="_blank" href="{{route('post.category', ['categorySlug' => 'chinh-sach-giao-hang'])}}">Chính sách giao hàng</a></li>
                                <li><a target="_blank" href="{{route('post.category', ['categorySlug' => 'chinh-sach-bao-mat-thong-tin-khach-hang'])}}">Chính sách bảo hành</a></li>
                                <li><a target="_blank" href="{{route('post.category', ['categorySlug' => 'hinh-thuc-thanh-toan'])}}">Hình thức thanh toán</a></li>
                                <li><a target="_blank" href="{{route('post.category', ['categorySlug' => 'chinh-sach-bao-mat-thong-tin-khach-hang'])}}">Chính sách bảo mật</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="footer-vc clk-footer">
                            <h3 class="name-heading clk-btn">Phương thức thanh toán</h3>
                            <div class="nav-other payment clk-content">
                                <span><img src="{{asset('images/bank-mastercard.png')}}"></span>
                                <span><img src="{{asset('images/bank-visa.png')}}"></span>
                                <span><img src="{{asset('images/bank-acb.png')}}"></span>
                                <span><img src="{{asset('images/bank-agribank.jpg')}}"></span>
                                <span><img src="{{asset('images/bank-baokim.jpg')}}"></span>
                                <span><img src="{{asset('images/bank-donga.png')}}"></span>
                                <span><img src="{{asset('images/bank-bidv.png')}}"></span>
                                <span><img src="{{asset('images/bank-techcombank.png')}}"></span>
                                <span><img src="{{asset('images/bank-vietcombank.png')}}"></span>
                                <span><img src="{{asset('images/bank-nganluong.jpg')}}"></span>
                            </div>
                        </div>
                        <div class="footer-vc clk-footer">
                            <h3 class="name-heading mar_bottom_0 clk-btn">ĐỐI TÁC VẬN CHUYỂN</h3>
                            <div class="nav-other clk-content">
                                <span><img src="{{asset('images/vc-ttc.png')}}"></span>
                                <span><img src="{{asset('images/vc-ems.png')}}"></span>
                                <span><img src="{{asset('images/vc-waranty_bot.png')}}"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                <div class="cpy-right-info">
                    <h5>Copyright © 2018. All rights reserved. {{setting('site.title')}} </h5>
                </div>

            </div>
        </div>
    </div>
</footer>

@if($errors->has('email') and !Auth::user() and Route::currentRouteName() !== 'user.password.forget')
    @push('js')
            <script>
                $( document ).ready(function() {
                    $('#myLogin').modal('show');
                });
            </script>
    @endpush
@endif