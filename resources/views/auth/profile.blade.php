@extends('layouts.main')
@section('content')
<main>
    <div class="colum-one">
        <div class="container">
            <div class="content-full">

                <div class="row">
                    <div class="col-sm-3">
                        @include('components.profile-sidebar')
                    </div><!-- /.col-sm-4 -->
                    <div class="col-sm-9">
                        <div class="box">
                            <div class="box_header">
                                <h2 class="box_title">Thông tin tài khoản</h2>
                            </div>
                            <div class="box_body box_body_form">
                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <form class="form form-registration" method="post" action="{{route('user.update.profile')}}">
                                    @csrf
                                    <div class="form-group {{$errors->has('email') ? 'has-error has-danger' : ''}}">
                                        <label for="email" class="control-label">Email <span class="text-color-red">(*)</span></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                            <input class="form-control" autocomplete="off" type="text" name="email" placeholder="Email" required="" value="{{$user->email}}">
                                        </div>
                                        @if ($errors->has('email'))
                                            <div class="help-block with-errors">
                                                <ul class="list-unstyled">
                                                    <li>{{ $errors->first('email') }}</li>
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group {{$errors->has('phone') ? 'has-error has-danger' : ''}}" >
                                        <label for="email" class="control-label">Số Điện Thoại <span class="text-color-red">(*)</span></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                            <input class="form-control" type="text" name="phone" placeholder="Số Điện Thoại" required="" value="{{$user->phone}}">
                                        </div>
                                        @if ($errors->has('phone'))
                                            <div class="help-block with-errors">
                                                <ul class="list-unstyled">
                                                    <li>{{ $errors->first('phone') }}</li>
                                                </ul>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-group {{$errors->has('birthday') ? 'has-error has-danger' : ''}}" >
                                        <label for="email" class="control-label">Ngày sinh <span class="text-color-red">(*)</span></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input class="form-control" type="text" name="birthday" placeholder="Ngày/Tháng/Năm" required="" value="{{$user->birthday ? $user->birthday : ''}}">
                                        </div>
                                        @if ($errors->has('birthday'))
                                            <div class="help-block with-errors">
                                                <ul class="list-unstyled">
                                                    <li>{{ $errors->first('birthday') }}</li>
                                                </ul>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-group form-group-lg">
                                        <label for="gender" class="control-label">Giới tính <span class="text-color-red">(*)</span></label>
                                        <div class="radio-list">
                                            <label><input type="radio" name="gender" value="1" {{$user->gender ? 'checked' : ''}}> Nam</label>
                                            <label><input type="radio" name="gender" value="2" {{$user->gender==2 ? 'checked' : ''}}> Nữ</label>
                                            <label><input type="radio" name="gender" value="0" {{!$user->gender ? 'checked' : ''}}> Không khai báo</label>
                                        </div>
                                    </div>

                                    <div class="form-group form-group-lg">
                                        <button type="submit" class="change_info btn btn-lg btn-success">Cập Nhật</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div><!-- /.col-sm-8 -->
                </div><!-- /.row -->
            </div>
        </div>
    </div><!-- /. bg-gray -->
</main>
@endsection
