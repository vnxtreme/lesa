@extends('layouts.main')
@section('content')
<main>
    <div class="colum-one">
        <div class="container">
            <div class="content-full">
                <div class="row">
                    <div class="col-sm-3">
                        @include('components.profile-sidebar')
                    </div>
                    <div class="col-sm-9">

                        <div class="box">
                            <div class="box_header"><h2 class="box_title">Thông tin tài khoản</h2></div>
                            <div class="box_body box_body_form">        
                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <form class="form form-registration" method="post" action="{{ route('user.update.password.change') }}">
                                    @csrf
                                    <div class="form-group {{$errors->has('current_password') ? 'has-error has-danger' : ''}}">
                                        <label for="email" class="control-label">Mật khẩu cũ <span class="text-color-red">(*)</span></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                            <input class="form-control" type="password" name="current_password" placeholder="Mật khẩu" required="" autofocus>
                                        </div>
                                        @if ($errors->has('current_password'))
                                            <div class="help-block with-errors">
                                                <ul class="list-unstyled">
                                                    <li>{{ $errors->first('current_password') }}</li>
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group {{$errors->has('password') ? 'has-error has-danger' : ''}}">
                                        <label for="email" class="control-label">Mật khẩu mới <span class="text-color-red">(*)</span></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                            <input class="form-control" type="password" name="password" placeholder="Mật khẩu" required="">
                                        </div>
                                        @if ($errors->has('password'))
                                            <div class="help-block with-errors">
                                                <ul class="list-unstyled">
                                                    <li>{{ $errors->first('password') }}</li>
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group {{$errors->has('password_confirmation') ? 'has-error has-danger' : ''}}">
                                        <label for="email" class="control-label">Nhập lại mật khẩu mới <span class="text-color-red">(*)</span></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                            <input class="form-control" type="password" name="password_confirmation" placeholder="Nhập Lại Mật khẩu" required="">
                                        </div>
                                        @if ($errors->has('password_confirmation'))
                                            <div class="help-block with-errors">
                                                <ul class="list-unstyled">
                                                    <li>{{ $errors->first('password_confirmation') }}</li>
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                    <input id="email" type="hidden" class="form-control" name="email" value="{{ $user->email ?? old('email') }}">
                                    <div class="form-group form-group-lg">
                                        <button title="Cập nhật" class="change_info btn btn-lg btn-success" type="submit">
                                            Sửa mật khẩu
                                        </button>
                                    </div>
                                </form>
                                
                            </div>
                        </div>
                        
                    </div>
                </div><!-- /.row -->
            </div>
        </div>
    </div><!-- /. bg-gray -->
</main>
@endsection
