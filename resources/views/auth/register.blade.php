@extends('layouts.main')
@section('content')
<main>
    <div class="colum-one">
        <div class="container">
            <div class="content-full forms-register">

                <div class="box box-center">
                    <div class="box_header">
                        <h2 class="box_title">Đăng ký tài khoản</h2>
                    </div>
                    <div class="box_body">

                        <form class="form form-registration" action="" method="post" data-toggle="validator" role="form" novalidate="true">
                            <div class="form_inner2">
                                @csrf
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group {{$errors->has('name') ? 'has-error has-danger' : ''}}">
                                            <label for="email" class="control-label">Họ và tên <span class="text-color-red">(*)</span></label>
                                            <input class="form-control" type="text" id="userName" name="name" placeholder="Họ và tên" value="{{ old('name') }}" required="">
                                            @if ($errors->has('name'))
                                                <div class="help-block with-errors">
                                                    <ul class="list-unstyled">
                                                        <li>{{ $errors->first('name') }}</li>
                                                    </ul>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group {{$errors->has('email') ? 'has-error has-danger' : ''}}">
                                            <label for="email" class="control-label">Email <span class="text-color-red">(*)</span></label>
                                            <input class="form-control" type="email" id="userEmail" name="email" placeholder="Email" value="{{ old('email') }}" required="">
                                            @if ($errors->has('email'))
                                                <div class="help-block with-errors">
                                                    <ul class="list-unstyled">
                                                        <li>{{ $errors->first('email') }}</li>
                                                    </ul>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group {{$errors->has('phone') ? 'has-error has-danger' : ''}}">
                                            <label for="email" class="control-label">Số Điện Thoại <span class="text-color-red">(*)</span></label>
                                            <input class="form-control" type="text" id="userPhone" name="phone" placeholder="Số Điện Thoại" value="{{ old('phone') }}" required="">
                                            @if ($errors->has('phone'))
                                                <div class="help-block with-errors">
                                                    <ul class="list-unstyled">
                                                        <li>{{ $errors->first('phone') }}</li>
                                                    </ul>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group {{$errors->has('password') ? 'has-error has-danger' : ''}}">
                                            <label for="email" class="control-label">Mật khẩu <span class="text-color-red">(*)</span></label>
                                            <input class="form-control" type="password" id="userPass" name="password" placeholder="Mật khẩu" data-error="Mật khẩu ít nhất từ 6 ký tự!" data-minlength="6" required="">
                                            @if ($errors->has('password'))
                                                <div class="help-block with-errors">
                                                    <ul class="list-unstyled">
                                                        <li>{{ $errors->first('password') }}</li>
                                                    </ul>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group {{$errors->has('password') ? 'has-error has-danger' : ''}}">
                                            <label for="email" class="control-label">Nhập Lại Mật khẩu <span class="text-color-red">(*)</span></label>
                                            <input class="form-control" type="password" id="userPassConfirm" name="password_confirmation" placeholder="Nhập Lại Mật khẩu" data-match="#userPass" data-error="Nhập lại mật khẩu không được để trống!" data-match-error="Nhập lại mật khẩu chưa chính xác!" required="">
                                            @if ($errors->has('password'))
                                                <div class="help-block with-errors">
                                                    <ul class="list-unstyled">
                                                        <li>{{ $errors->first('password') }}</li>
                                                    </ul>
                                                </div>
                                            @endif
                                        </div>

                                        <div class="form-group form-group-lg">
                                            <label for="gender" class="control-label">Giới tính <span class="text-color-red">(*)</span></label>
                                            <div class="radio-list">
                                                <label><input type="radio" id="user_sex" name="gender" value="1" {{ old('gender') ? 'checked' : '' }}> Nam</label>
                                                <label><input type="radio" id="user_sex" name="gender" value="2" {{ old('remember')==2 ? 'checked' : '' }}> Nữ</label>
                                                <label><input type="radio" id="user_sex" name="gender" value="0" {{ !old('remember') ? 'checked' : '' }} checked="checked"> Không khai báo</label>
                                            </div>
                                            @if ($errors->has('gender'))
                                                <div class="help-block with-errors">
                                                    <ul class="list-unstyled">
                                                        <li>{{ $errors->first('gender') }}</li>
                                                    </ul>
                                                </div>
                                            @endif
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group form-group-lg">
                                    <button value="submit" type="submit" name="submit" class="btn-red btn-block disabled">ĐĂNG KÝ</button>
                                </div>

                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div><!-- /. bg-gray -->
</main>
@endsection
