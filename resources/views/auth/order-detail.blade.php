@extends('layouts.main')
@section('content')
@php
    $shipingMethods = ['-', 'Thanh toán khi nhận hàng', 'Chuyển khoản Ngân lượng', 'Mua trực tiếp tại cửa hàng'];
    $subTotal = $order->total - $order->collect_fee - $order->delivery_fee;
@endphp
<main>
    <div class="colum-one">
        <div class="container">
            <div class="content-full">
                <div class="row">
                    <div class="col-sm-3">
                        @include('components.profile-sidebar')
                    </div><!-- /.col-sm-4 -->
                    <div class="col-sm-9">
                        <div class="box">
                            <div class="box_header">
                                <h2 class="box_title">Xem chi tiết đơn hàng</h2>
                            </div>
                            <div class="box_body_order">

                                <div class="box_order_step steps">
                                    <ol class="progress-steps">
                                        <li data-step="1" class="is-active">
                                            <span>Đặt hàng<br></span>
                                        </li>
                                        <li data-step="2">
                                            <span>Xác nhận đơn hàng<br></span>
                                        </li>
                                        <li data-step="3">
                                            <span>Chuyển hàng<br></span>
                                        </li>
                                        <li data-step="4">
                                            <span>Hoàn tất<br></span>
                                        </li>
                                    </ol>
                                </div>

                                <h3 class="mTitle rowTitle">Thông tin đơn hàng</h3>
                                <div class="box_body_detail">
                                    <table class="table table-none">
                                        <tbody>
                                            <tr>
                                                <th>Đơn hàng</th>
                                                <td>#{{$order->id}}</td>
                                            </tr>
                                            <tr>
                                                <th>Ngày</th>
                                                <td>{{$order->created_at->format('d/m/Y H:i')}}</td>
                                            </tr>
                                            <tr>
                                                <th>Trạng thái</th>
                                                <td class="orderStatusText">Đặt hàng</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <h3 class="box_order_heading">Tóm tắt</h3>
                                    <table class="table table-none">
                                        <tbody>
                                            <tr>
                                                <th>Hình thức thanh toán</th>
                                                <td>{{$shipingMethods[$order->shipping_method]}}</td>
                                            </tr>
                                            <tr>
                                                <th>Thành tiền</th>
                                                <td>{{number_format($subTotal, 0, ',', '.')}} đ</td>
                                            </tr>
                                            <tr>
                                                <th>Phí vận chuyển</th>
                                                <td>{{number_format($order->delivery_fee, 0, ',', '.')}} đ</td>
                                            </tr>
                                            <tr>
                                                <th>Phí thu hộ</th>
                                                <td>{{number_format($order->collect_fee, 0, ',', '.')}} đ</td>
                                            </tr>
                                            <tr>
                                                <th>Tổng cộng</th>
                                                <td>{{number_format($order->total, 0, ',', '.')}} đ</td>
                                            </tr>
                                            <tr>
                                                <th>Ghi chú cho bộ phận giao hàng</th>
                                                <td class="orderNotes">{{$order->comment}}</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <h3 class="box_order_heading">Thông tin khách hàng</h3>
                                    <table class="table table-none">
                                        <tbody>
                                            <tr>
                                                <th>Họ tên</th>
                                                <td>{{$order->payment_name}}</td>
                                            </tr>
                                            <tr>
                                                <th>Số điện thoại</th>
                                                <td>{{$order->payment_telephone}}</td>
                                            </tr>
                                            <tr>
                                                <th>Email</th>
                                                <td>{{$order->email}}</td>
                                            </tr>
                                            <tr>
                                                <th>Địa chỉ</th>
                                                <td>{{$order->payment_address}}, {{$order->getWard->name}}, {{$order->getDistrict->name}}, {{$order->getCity->name}}</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <h3 class="box_order_heading">Thông tin sản phẩm</h3>
                                    <table class="table table-none">
                                        <thead>
                                            <tr>
                                                <th>Sản phẩm</th>
                                                <th width="15%" align="left">Giá bán</th>
                                                <th width="15%" align="left">Số lượng</th>
                                                <th width="20%" align="right" class="text-right">Thành tiền</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($order->order as $item)
                                                <tr>
                                                    <td>
                                                        <a class="alink" href="{{route('product.categorySlug.id', ['categorySlug' => $item->product->slug, 'id' => $item->product_id])}}" target="_blank" title="">{{$item->product->name}}</a>
                                                    </td>
                                                    <td>{{number_format($item->price, 0, ',', '.')}}</td>
                                                    <td align="center">{{$item->quantity}}</td>
                                                    <td align="right">{{number_format($item->price*$item->quantity, 0, ',', '.')}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                </div>


                            </div>
                        </div>
                    </div>
                </div><!-- /.row -->
            </div>
        </div>
    </div><!-- /. bg-gray -->
</main>
@endsection
