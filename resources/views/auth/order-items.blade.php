@extends('layouts.main')
@section('content')
<main>
    <div class="colum-one">
        <div class="container">
            <div class="content-full">
                <div class="row">
                    <div class="col-sm-3">
                        @include('components.profile-sidebar')
                    </div><!-- /.col-sm-4 -->
                    <div class="col-sm-9">
                        <div class="box">
                            <div class="box_header">
                                <h2 class="box_title">Xem đơn hàng đã đặt tại website</h2>
                            </div>
                        </div>

                        <div class="box">
                            <div class="box_body_carts">

                                <table class="table table--listing table--orders">
                                    <thead>
                                        <tr>
                                            <th width="14%">ID</th>
                                            <th>Sản phẩm</th>
                                            <th width="14%">Ngày</th>
                                            <th width="15%">Tổng cộng</th>
                                            <th width="16%">Trạng thái</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($orderInfo as $bill)
                                        <tr>
                                            <td class="order-id">
                                                <a href="{{route('user.orders.detail', ['orderID' => $bill->id])}}" class="blink">#{{$bill->id}}</a>
                                            </td>
                                            <td class="name">
                                                @foreach($bill->order as $item)
                                                    <div style="width: 100%; margin-bottom: 7px;">
                                                        <a href="{{route('product.categorySlug.id', ['categorySlug' => $item->product->category->slug, 'id' => $item->product_id])}}" target="_blank">{{$item->product->name}}</a>
                                                    </div>
                                                @endforeach
                                            </td>
                                            <td class="date">{{$bill->created_at->format('d/m/Y H:i')}}</td>
                                            <td class="total">{{number_format($bill->total, 0, ',', '.')}} đ</td>
                                            <td class="status">Đang xử lý</td>
                                        </tr>
                                        @endforeach

                                        <tr>
                                            <td colspan="5"></td>
                                        </tr>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div><!-- /.row -->
            </div>
        </div>
    </div><!-- /. bg-gray -->
</main>
@endsection
