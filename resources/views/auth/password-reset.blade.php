@extends('layouts.main')
@section('content')
<main>
    <div class="bg-gray">
        <div class="container">
            <div class="content-full">

                <div class="box box-center">
                    <div class="box_header">
                        <h2 class="box_title">Đổi mật khẩu</h2>
                    </div>
                    <div class="box_body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form class="form form-registration" action="{{route('password.request')}}" method="post">
                            @csrf
                            <div class="form_inner">
                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="form-group {{$errors->has('email') ? 'has-error has-danger' : ''}}">
                                    <label for="email" class="control-label">Email <span class="text-color-red">(*)</span></label>
                                    <div class="input-group {{$errors->has('email') ? 'has-error has-danger' : ''}}">
                                        <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                        <input class="form-control" autocomplete="off" type="email"placeholder="Email" name="email" value="{{ $email ?? old('email') }}" required autofocus />
                                    </div>
                                    @if ($errors->has('email'))
                                        <div class="help-block with-errors">
                                            <ul class="list-unstyled">
                                                <li>{{ $errors->first('email') }}</li>
                                            </ul>
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group {{$errors->has('password') ? 'has-error has-danger' : ''}}">
                                    <label for="email" class="control-label">Mật khẩu mới<span class="text-color-red">(*)</span></label>
                                    <input class="form-control" type="password" name="password" placeholder="Mật khẩu" required="">
                                    @if ($errors->has('password'))
                                        <div class="help-block with-errors">
                                            <ul class="list-unstyled">
                                                <li>{{ $errors->first('password') }}</li>
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                               

                                <div class="form-group {{$errors->has('password_confirmation') ? 'has-error has-danger' : ''}}">
                                    <label for="email" class="control-label">Nhập Lại Mật khẩu <span class="text-color-red">(*)</span></label>
                                    <input class="form-control" type="password" name="password_confirmation" placeholder="Nhập Lại Mật khẩu" required="">
                                </div>

                                <div class="form-group form-group-lg">
                                    <button type="submit" class="btn btn-lg btn-success btn-block">GỬI</button>
                                </div>

                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div><!-- /. bg-gray -->

</main>
@endsection
