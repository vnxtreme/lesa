@extends('layouts.main')
@section('content')
<main>
    <div class="bg-gray">
        <div class="container">
            <div class="content-full">

                <div class="box box-center">
                    <div class="box_header">
                        <h2 class="box_title">Gửi email xác nhận</h2>
                    </div>
                    <div class="box_body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form class="form form-registration" action="{{route('password.email')}}" method="post">
                            @csrf
                            <div class="form_inner">

                                <div class="form-group {{$errors->has('email') ? 'has-error has-danger' : ''}}">
                                    <label for="email" class="control-label">Email <span class="text-color-red">(*)</span></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                        <input class="form-control" autocomplete="off" type="email"placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
                                    </div>
                                    @if ($errors->has('email'))
                                        <div class="help-block with-errors">
                                            <ul class="list-unstyled">
                                                <li>{{ $errors->first('email') }}</li>
                                            </ul>
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group form-group-lg">
                                    <button type="submit" class="btn btn-lg btn-success btn-block">GỬI</button>
                                </div>

                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div><!-- /. bg-gray -->

</main>
@endsection
