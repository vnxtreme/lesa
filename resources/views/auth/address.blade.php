@extends('layouts.main')
@section('content')
<main>
    <div class="colum-one">
        <div class="container">
            <div class="content-full">

                <div class="row">
                    <div class="col-sm-3">
                        @include('components.profile-sidebar')
                    </div><!-- /.col-sm-4 -->
                    <div class="col-sm-9">
                        <div class="box">
                            <div class="box_header">
                                <h2 class="box_title">Địa chỉ giao hàng</h2>
                            </div>
                            <div class="box_body box_body_form">
                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <form class="form form-registration" action="{{route('user.update.address')}}" method="post" >
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group {{$errors->has('name') ? 'has-error has-danger' : ''}}">
                                                <label for="fullname" class="control-label">Họ và tên <span class="text-color-red">(*)</span></label>
                                                <input class="form-control" autocomplete="off" type="text" name="name" placeholder="Họ và tên" required="" value="{{$user->name}}">
                                            </div>
                                            <div class="form-group {{$errors->has('address') ? 'has-error has-danger' : ''}}">
                                                <label for="user_address" class="control-label">Địa chỉ <span class="text-color-red">(*)</span></label>
                                                <input class="form-control" autocomplete="off" type="text" name="address" placeholder="Địa chỉ" required="" value="{{$user->address}}">
                                            </div>
                                        </div>
                                        <!--/.col -->
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="city" class="control-label">Tỉnh/thành phố <span class="text-color-red">(*)</span></label>
                                                <select id="city" name="city" class="form-control province" onchange="locationSelection.districtList(this)" required>
                                                    <option value="" selected="">Chọn tỉnh thành</option>
                                                    @foreach ($cities as $city)
                                                        <option value="{{$city->matp}}" {{$user->city == $city->matp ? 'selected' : ''}}>{{$city->name}}</option>
                                                    @endforeach
                                                </select>
                                                @if(isset($errorArray))
                                                    <div class="small col-xs-12 text-danger">{{$errorArray['city'][0] ?? ''}}</div>
                                                @endif
                                            </div>

                                            <div class="form-group">
                                                <label for="district" class="control-label">Quận/huyện <span class="text-color-red">(*)</span></label>
                                                <select class="form-control district" id="district" name="district" onclick="locationSelection.wardList(this);" required>
                                                    @if($user->district)
                                                        @foreach ($user->districtList()->get() as $district)
                                                            <option value="{{$district->maqh}}" {{$district->maqh == $user->district ? 'selected' : ''}}>{{$district->name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="">Huyện / Quận</option>
                                                    @endif
                                                </select>
                                                @if(isset($errorArray))
                                                    <div class="small col-xs-12 text-danger">{{$errorArray['district'][0] ?? ''}}</div>
                                                @endif
                                            </div>

                                            <div class="form-group">
                                                <label for="ward" class="control-label">Xã / Phường <span class="text-color-red">(*)</span></label>
                                                <select class="ward form-control" id="ward" name="ward" required>
                                                    @if($user->district)
                                                        @foreach ($user->wardList()->get() as $ward)
                                                            <option value="{{$ward->maqh}}" {{$ward->xaid == $user->ward ? 'selected' : ''}}>{{$ward->name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option>Xã / Phường</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <!--/.col -->
                                    </div>
                                    <!--/.row -->

                                    <div class="form-group form-group-lg">
                                        <button title="Cập nhật" class="change_info btn btn-lg btn-success" type="submit">
                                            Sửa thông tin
                                        </button>
                                    </div>


                                </form>

                            </div>
                        </div>
                    </div><!-- /.col-sm-8 -->
                </div><!-- /.row -->
            </div>
        </div>
    </div><!-- /. bg-gray -->
</main>
@endsection
