{{-- PRODUCT IS SPECIAL FORM, DONT USE THIS FILE FOR OTHER TABLE  --}}
@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.__('voyager::customize.'.$dataType->display_name_singular))

@section('css')
    <style>
        .panel .mce-panel {
            border-left-color: #fff;
            border-right-color: #fff;
        }

        .panel .mce-toolbar,
        .panel .mce-statusbar {
            padding-left: 20px;
        }

        .panel .mce-edit-area,
        .panel .mce-edit-area iframe,
        .panel .mce-edit-area iframe html {
            padding: 0 10px;
            min-height: 350px;
        }

        .mce-content-body {
            color: #555;
            font-size: 14px;
        }

        .panel.is-fullscreen .mce-statusbar {
            position: absolute;
            bottom: 0;
            width: 100%;
            z-index: 200000;
        }

        .panel.is-fullscreen .mce-tinymce {
            height:100%;
        }

        .panel.is-fullscreen .mce-edit-area,
        .panel.is-fullscreen .mce-edit-area iframe,
        .panel.is-fullscreen .mce-edit-area iframe html {
            height: 100%;
            position: absolute;
            width: 99%;
            overflow-y: scroll;
            overflow-x: hidden;
            min-height: 100%;
        }

        #stock .panel-body .row:first-child .deleteColor{
            display: none;
        }
    </style>
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.__('voyager::customize.'.$dataType->display_name_singular) }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="@if(isset($dataTypeContent->id)){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->id) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif" method="POST" enctype="multipart/form-data">
            <!-- PUT Method if we are editing -->
            @if(isset($dataTypeContent->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-8">
                    <!-- ### TITLE ### -->
                    <div class="panel">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="panel-heading">
                            <h3 class="panel-title">
                                {{-- <i class="voyager-character"></i>  --}}
                                {{-- {{  __('voyager::post.title')  }} --}}
                                {{__('voyager::customize.Name')}}
                                {{-- <span class="panel-desc"> {{ __('voyager::post.title_sub') }}</span> --}}
                            </h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            @include('voyager::multilingual.input-hidden', [
                                '_field_name'  => 'name',
                                '_field_trans' => get_field_translations($dataTypeContent, 'name')
                            ])
                            <input type="text" class="form-control" id="name" name="name" placeholder="{{ __('voyager::generic.name') }}" value="@if(isset($dataTypeContent->name)){{ $dataTypeContent->name }}@endif">
                        </div>
                    </div>

                    <!-- ### CONTENT ### -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ __('voyager::customize.content') }}</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                            </div>
                        </div>

                        <div class="panel-body">
                            @include('voyager::multilingual.input-hidden', [
                                '_field_name'  => 'body',
                                '_field_trans' => get_field_translations($dataTypeContent, 'body')
                            ])
                            @php
                                $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
                                $row = $dataTypeRows->where('field', 'body')->first();
                            @endphp
                            {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                        </div>
                    </div><!-- .panel -->

                    <!-- ### EXCERPT ### -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{!! __('voyager::customize.excerpt') !!}</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            @include('voyager::multilingual.input-hidden', [
                                '_field_name'  => 'excerpt',
                                '_field_trans' => get_field_translations($dataTypeContent, 'excerpt')
                            ])
                            <textarea class="form-control" name="excerpt">@if (isset($dataTypeContent->excerpt)){{ $dataTypeContent->excerpt }}@endif</textarea>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{__('voyager::customize.specification')}}</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            @php
                                $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
                                $exclude = ['name', 'body', 'excerpt', 'slug', 'status', 'category_id', 'author_id', 'featured', 'image', 'meta_description', 'meta_keywords', 'seo_title', 'view', 'warranty', 'weight', 'parent_category_id', 'image_fb'];
                            @endphp

                            @foreach($dataTypeRows as $row)
                                @if(!in_array($row->field, $exclude))
                                    @php
                                        $options = /* json_decode */($row->details);
                                        $display_options = isset($options->display) ? $options->display : NULL;
                                    @endphp
                                    
                                    @if ($options && isset($options->formfields_custom))
                                        @include('voyager::formfields.custom.' . $options->formfields_custom)
                                    @else
                                        {{-- Only Images --}}
                                        @if($row->display_name == 'Images')
                                            <div class="col-md-12">
                                        @endif
                                        {{-- Only Images --}}

                                        <div class="form-group 
                                            @if($row->type == 'hidden') hidden @endif 
                                            @if(isset($display_options->width)){{ 'col-md-' . $display_options->width }}@endif" 
                                            @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif 
                                            @if(isset($display_options->width)) @endif 
                                            @if(isset($display_options->width)) style="padding-left:0;" @endif
                                        >
                                            {{ $row->slugify }}
                                            <label for="name">{{ __('voyager::customize.'.$row->display_name) }}</label>
                                            @include('voyager::multilingual.input-hidden-bread-edit-add')
                                            @if($row->type == 'relationship')
                                                @include('voyager::formfields.relationship')
                                            @else
                                                {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                            @endif

                                            @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                                {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                            @endforeach
                                        </div>
                                        
                                        {{-- Only Images --}}
                                        @if($row->display_name == 'Images')
                                            </div>
                                        @endif
                                        {{-- Only Images --}}
                                    @endif
                                   
                                @endif
                            @endforeach
                        </div>
                    </div>

                    {{-- Addon: Color Quantity --}}
                    {{-- @php
                        $colorOptions = app('App\Models\Color')->all();//get all Colors
                    @endphp
                    <div class="panel" id="stock">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ __('voyager::customize.stock_management')}}</h3>
                            <div class="panel-actions">
                                <a class="btn btn-success" onclick="addColor()" href="javascript:;">+ {{__('voyager::customize.add_color')}}</a>
                            </div>
                        </div>
                        <div class="panel-body">
                            @if (isset($colorQty))
                                @foreach ($colorQty as $newRow)
                                    <div class="row">
                                        <div class="form-group col-xs-6 col-sm-7">
                                            <label for="status">{{__('voyager::customize.color')}}</label>
                                            <select class="form-control" name="colors[]">
                                                <option value="">No color</option>
                                                @foreach ($colorOptions as $color)
                                                    <option {{$newRow->color_id == $color->id ? 'selected' : ''}} value="{{$color->id}}">{{$color->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-xs-4 col-sm-4">
                                            <label for="status">{{__('voyager::customize.quantity')}}</label>
                                            <input class="form-control" type="text" name="quantities[]" placeholder="Quantity" value="{{$newRow->quantity ?? 1}}" required/>
                                        </div>
                                        <div class="col-xs-1 col-sm-1 deleteColor" style="margin-top: 20px;">
                                            <a href="javascript:;" onclick="deleteRow(this)"><i class="voyager-x" style="font-size: 30px;"></i></a>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="row">
                                    <div class="form-group col-xs-6 col-sm-7">
                                        <label for="status">Color</label>
                                        <select class="form-control" name="colors[]">
                                            <option value="">No color</option>
                                            @foreach ($colorOptions as $color)
                                                <option value="{{$color->id}}">{{$color->name}}</option>    
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4">
                                        <label for="status">Quantity</label>
                                        <input class="form-control" type="text" name="quantities[]" placeholder="Quantity" value="1" required />
                                    </div>
                                    <div class="col-xs-1 col-sm-1 deleteColor" style="margin-top: 20px;">
                                        <a href="javascript:;" onclick="deleteRow(this)"><i class="voyager-x" style="font-size: 30px;"></i></a>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div> --}}
                    {{-- /.Addon: Color Quantity --}}

                </div>
                <div class="col-md-4">
                    <!-- ### DETAILS ### -->
                    <div class="panel panel panel-bordered panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-clipboard"></i> {{ __('voyager::customize.details') }}</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="slug">{{ __("voyager::customize.unique_slug") }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'slug',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'slug')
                                ])
                                <input type="text" class="form-control" id="slug" name="slug"
                                    placeholder="slug"
                                    {{!! isFieldSlugAutoGenerator($dataType, $dataTypeContent, "slug") !!}}
                                    value="@if(isset($dataTypeContent->slug)){{ $dataTypeContent->slug }}@endif">
                            </div>
                            <div class="form-group">
                                <label for="status">{{ __('voyager::customize.status') }}</label>
                                <select class="form-control" name="status">
                                    <option value="PUBLISHED"@if(isset($dataTypeContent->status) && $dataTypeContent->status == 'PUBLISHED') selected="selected"@endif>{{ __('voyager::post.status_published') }}</option>
                                    <option value="DRAFT"@if(isset($dataTypeContent->status) && $dataTypeContent->status == 'DRAFT') selected="selected"@endif>{{ __('voyager::post.status_draft') }}</option>
                                    <option value="PENDING"@if(isset($dataTypeContent->status) && $dataTypeContent->status == 'PENDING') selected="selected"@endif>{{ __('voyager::post.status_pending') }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="category_id">{{ __('voyager::customize.category') }}</label>
                                <select class="form-control" name="category_id">
                                    {{-- @foreach(App\Models\Category::all()->translate() as $category)
                                        <option value="{{ $category->id }}"@if(isset($dataTypeContent->category_id) && $dataTypeContent->category_id == $category->id) selected="selected"@endif>{{ $category->name }}</option>
                                    @endforeach --}}
                                    @foreach(App\Models\Category::with('childCategories')->where('parent_id', null)->get() as $category)
                                        <option value="{{ $category->id }}" @if(isset($dataTypeContent->parent_category_id) && $dataTypeContent->category_id == $category->id) selected="selected"@endif>{{ $category->name }}</option>
                                        @if($category->childCategories)
                                            @foreach ($category->childCategories as $childCategory)
                                                <option value="{{ $childCategory->id }}" @if(isset($dataTypeContent->category_id) && $dataTypeContent->category_id == $childCategory->id) selected="selected"@endif>&nbsp;&nbsp;-- {{ $childCategory->name }}</option>
                                                @if($childCategory->childCategories)
                                                    @foreach ($childCategory->childCategories as $grantChildCategory)
                                                        <option value="{{ $grantChildCategory->id }}" @if(isset($dataTypeContent->category_id) && $dataTypeContent->category_id == $grantChildCategory->id) selected="selected"@endif>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;---- {{ $grantChildCategory->name }}</option>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            {{-- <div class="row">
                                <div class="form-group col-sm-6">
                                    <label for="parent_category_id">{{ __('voyager::post.parent_category_id') }}</label>
                                    <select id="parent_category_id" class="form-control" name="parent_category_id" >
                                        @foreach(App\Models\Category::where('parent_id', null)->get() as $category)
                                            <option value="{{ $category->id }}"@if(isset($dataTypeContent->parent_category_id) && $dataTypeContent->parent_category_id == $category->id) selected="selected"@endif>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                <div class="form-group col-sm-6">
                                    <label for="category_id">{{ __('voyager::post.category_id') }}</label>
                                    <select id="category_id" class="form-control" name="category_id">
                                        @if(isset($dataTypeContent->category_id))
                                            @foreach(App\Models\Category::where('parent_id', $dataTypeContent->parent_category)->get() as $category)
                                                <option value="{{ $category->id }}"@if(isset($dataTypeContent->category_id) && $dataTypeContent->category_id == $category->id) selected="selected"@endif>{{ $category->name }}</option>
                                                @if($category->childCategories)
                                                    @foreach ($category->childCategories as $childCategory)
                                                        <option value="{{ $childCategory->id }}" >-- {{ $childCategory->name }}</option>                                                        
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @else
                                             @foreach(App\Models\Category::where('parent_id', 1)->get() as $category)
                                                <option value="{{ $category->id }}"@if(isset($dataTypeContent->category_id) && $dataTypeContent->category_id == $category->id) selected="selected"@endif>{{ $category->name }}</option>
                                            @endforeach 
                                        @endif
                                    </select>
                                </div>
                            </div> --}}

                            <div class="form-group">
                                <label for="warranty">{{ __("voyager::customize.warranty") }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'warranty',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'warranty')
                                ])
                                <input type="text" class="form-control" id="warranty" name="warranty"
                                    placeholder="Thời gian bảo hành"
                                    {{!! isFieldSlugAutoGenerator($dataType, $dataTypeContent, "warranty") !!}}
                                    value="@if(isset($dataTypeContent->warranty)){{ $dataTypeContent->warranty }}@endif">
                            </div>
                            <div class="form-group">
                                <label for="warranty">{{ __("voyager::customize.weight") }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'weight',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'weight')
                                ])
                                <input type="text" class="form-control" id="weight" name="weight"
                                    placeholder="Thời gian bảo hành"
                                    {{!! isFieldSlugAutoGenerator($dataType, $dataTypeContent, "weight") !!}}
                                    value="@if(isset($dataTypeContent->weight)){{ $dataTypeContent->weight }}@endif">
                            </div>
                        </div>
                    </div>

                    <!-- ### IMAGE ### -->
                    <div class="panel panel-bordered panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-image"></i> {{ __('voyager::customize.image') }}</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            @if(isset($dataTypeContent->image))
                                <img src="{{ filter_var($dataTypeContent->image, FILTER_VALIDATE_URL) ? $dataTypeContent->image : Voyager::image( $dataTypeContent->image ) }}" style="width:100%" />
                            @endif
                            <input type="file" name="image">
                        </div>
                    </div>

                    <!-- ### FACEBOOK IMAGE ### -->
                    <div class="panel panel-bordered panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-image"></i> {{ __('voyager::customize.image_fb') }} (Tỉ lệ 2:1)</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            @if(isset($dataTypeContent->image_fb))
                                <img src="{{ filter_var($dataTypeContent->image_fb, FILTER_VALIDATE_URL) ? $dataTypeContent->image_fb : Voyager::image( $dataTypeContent->image_fb ) }}" style="width:100%" />
                            @endif
                            <input type="file" name="image_fb">
                        </div>
                    </div>

                    <!-- ### SEO CONTENT ### -->
                    <div class="panel panel-bordered panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-search"></i> {{ __('voyager::customize.seo_content') }}</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="meta_description">{{ __('voyager::post.meta_description') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'meta_description',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'meta_description')
                                ])
                                <textarea class="form-control" name="meta_description">@if(isset($dataTypeContent->meta_description)){{ $dataTypeContent->meta_description }}@endif</textarea>
                            </div>
                            <div class="form-group">
                                <label for="meta_keywords">{{ __('voyager::post.meta_keywords') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'meta_keywords',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'meta_keywords')
                                ])
                                <textarea class="form-control" name="meta_keywords">@if(isset($dataTypeContent->meta_keywords)){{ $dataTypeContent->meta_keywords }}@endif</textarea>
                            </div>
                            <div class="form-group">
                                <label for="seo_title">{{ __('voyager::post.seo_title') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'seo_title',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'seo_title')
                                ])
                                <input type="text" class="form-control" name="seo_title" placeholder="SEO Title" value="@if(isset($dataTypeContent->seo_title)){{ $dataTypeContent->seo_title }}@endif">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary pull-right">
                @if(isset($dataTypeContent->id)){{ __('voyager::post.update') }}@else <i class="icon wb-plus-circle"></i> {{ __('voyager::post.new') }} @endif
            </button>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        </form>
    </div>
    
    {{-- Modal --}}
    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> <?php echo e(__('voyager::generic.are_you_sure')); ?></h4>
                </div>

                <div class="modal-body">
                    <h4><?php echo e(__('voyager::generic.are_you_sure_delete')); ?> '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo e(__('voyager::generic.cancel')); ?></button>
                    <button type="button" class="btn btn-danger" id="confirm_delete"><?php echo e(__('voyager::generic.delete_confirm')); ?></button>
                </div>
            </div>
        </div>
    </div>
    {{-- .Modal --}}
@stop

@section('javascript')
    <script>
        var params = {}
        var $image;
        $('document').ready(function () {
            $('#slug').slugify();
            
            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });
            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');
                
                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();


            let element = $('#stock .panel-body > :first-child');
            let panel = $('#stock .panel-body');
            addColor = function(){
                panel.append(`<div class="row">${element.html()}</div>`);
            }
            deleteRow = function($this){
                if($('#stock .panel-body .row').length > 1 ){
                    $($this).closest('.row').remove();
                }
            }
        });

        /* Format currency */
        let currentPrice = $('input[name="current_price"]'),
            oldPrice = $('input[name="old_price"]'),
            shippingFee = $('input[name="shipping_fee"]'),
            price = $('input[name="price"]');
        currentPrice.val(parseInt(currentPrice.val()).toLocaleString('vi'));
        oldPrice.val(parseInt(oldPrice.val()).toLocaleString('vi'));
        shippingFee.val(parseInt(shippingFee.val()).toLocaleString('vi'));
        price.val(parseInt(price.val()).toLocaleString('vi'));

        $('input[name="current_price"], input[name="old_price"], input[name="shipping_fee"], input[name="price"]').on('input', function () {
            var number = parseInt($(this).val().replace(/[\.,]/g, ""));
            if (isNaN(number)) {
                $(this).val(0);
            } else {
                $(this).val(number.toLocaleString('vi'));
            }
        })
        
        if($('.readmore').length){
            $('.readmore').each( function(index, element){
                if( !isNaN(parseInt($(element).text())) ){
                    $(element).text( parseInt($(element).text()).toLocaleString('vi') );
                }
            })
        }
        /* /.Format currency */

        $('#parent_category_id').on('change', function (element) {
            $('#category_id').empty();
            console.log(element.target.value)
            $.ajax({
                url: '/get-child-categories',
                type: 'POST',
                data: {
                    id: element.target.value
                },
                success: function (result) {
                    let optionsHTML = '';
                    result.data.forEach(category => {
                        optionsHTML += `<option value="${category.id}">${category.name}</option>`;
                    });
                    $('#category_id').append(optionsHTML);
                }
            })
        });
    </script>
@stop
