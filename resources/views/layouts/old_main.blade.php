<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hệ thống camera giám sát - Thiết bị smarthome - Cáp HDMI - Phụ Kiện HD | Le Sa Digital Company.</title>
    <meta name="robots" content="no-cache" />
    <meta name="description" content="Hệ thống camera giám sát - Thiết bị smarthome - Cáp HDMI - Phụ Kiện HD | Le Sa Digital Company." />
    <meta name="keywords" content="Khoá Cửa Thông Minh,KHoá cửa 5asystem,khóa cửa vân tay,khóa cửa" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link href="https://khoacuadangcap.com/templates/lesa/assets/images/favicon.ico" rel="shortcut icon" type="" />
    <meta property="fb:app_id" content="188959431252089" />
    <meta property="fb:admins" content="100001651756436" />
    <meta name="language" content="english, en, Vietnamese,vn" />
    
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="google-site-verification" content="m-gKxp7KkujGijkvbkr3-Z-q5iX81tYIRu4M7xcQrnw" />
    <meta property='og:locale' content='vi_VN' />
    <meta property='og:type' content='product' />
    <meta property='og:title' content='Hệ thống camera giám sát - Thiết bị smarthome - Cáp HDMI - Phụ Kiện HD | Le Sa Digital Company.' />
    <meta property='og:description' content='Hệ thống camera giám sát - Thiết bị smarthome - Cáp HDMI - Phụ Kiện HD | Le Sa Digital Company.' />
    <meta property='og:url' content='https://khoacuadangcap.com/' />
    <meta property='og:site_name' content='Hệ thống camera giám sát - Thiết bị smarthome - Cáp HDMI - Phụ Kiện HD | Le Sa Digital Company.' />

    <script type="text/javascript">
        var web_address = "https://khoacuadangcap.com/";
        var web_folder = "https://khoacuadangcap.com/";
        var template_dir = "https://khoacuadangcap.com/templates/lesa/";
        var web_libs = "https://khoacuadangcap.com/libs/";
        var url_prefix = ".html";
        var check_user = 1;
    </script>

    <link rel="canonical" href="https://khoacuadangcap.com/" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/style.css?v=2.5')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/media-style.css?v=2.5')}}">

    <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/layout.js?v=2.5')}}"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div id="page" class="wsmenucontainer clearfix ">
        <div class="overlapblackbg"></div>
        <div class="olay-css"></div>

        @include('components.header')

        @yield('content')

        @include('components.footer')

        <script type="text/javascript" src="{{asset('js/quatang_box.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery-migrate-1.2.1.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/webslidemenu.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/owl.carousel.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/slick.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.tools.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/notify.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/validator.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/index.js?v=2.5')}}"></script>

        <div style="display:inline;"></div>

        <div class="zalo-support">
            <a data-animate="fadeInDown" rel="noopener noreferrer" href="{{$information['twitter']}}" target="_blank" class="button success" style="border-radius:99px;" data-animated="true">
                <span>Chat Zalo </span>
            </a>
        </div>
        <div class="fb-support">
            <a data-animate="fadeInDown" rel="noopener noreferrer" href="{{$information['facebook']}}" target="_blank" class="button success" style="border-radius:99px;" data-animated="true">
                <span>Chat Facebook</span>
            </a>
        </div>
        <div class="hotline2">
            <a id="callnowbutton" href="tel:{{str_replace(' ','',$information['hotline'])}}">HL:{{$information['hotline']}}</a><i class="fa fa-phone"></i>
        </div>
        <div class="hotline1">
            <a id="callnowbutton" href="tel:{{str_replace(' ','',$information['phone1'])}}">ĐT:{{$information['phone1']}}</a><i class="fa fa-phone"></i>
        </div>

    </div>
    <script async defer src="https://apis.google.com/js/api.js"></script>
    <!-- Bộ đếm google zippozippo.com-->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- Google Tag Manager -->
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-M6XBXCS');

    </script>
    <!-- End Google Tag Manager -->
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M6XBXCS" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- Kết thúc bộ đếm google   -->

    <!-- Google Code dành cho Thẻ tiếp thị lại -->
</body>
</html>
