<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{setting('site.title')}}</title>
    <meta name="robots" content="no-cache" />
    <meta name="description" content="{{setting('site.description')}}" />
    <meta name="keywords" content="{{$product->seo_keywords ?? setting('site.description')}}" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link href="{{asset('images/favicon.ico')}}" rel="shortcut icon" type="" />
    <meta name="language" content="english, en, Vietnamese,vn" />
    
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="google-site-verification" content="m-gKxp7KkujGijkvbkr3-Z-q5iX81tYIRu4M7xcQrnw" />
    <meta property='og:locale' content='vi_VN' />
    <meta property='og:type' content='product' />
    <meta property='og:title' content='{{$product->seo_title ?? setting('site.title')}}' />
    <meta property='og:description' content='{{$product->meta_description ?? setting('site.description')}}' />
    <meta property='og:url' content='{{url()->current()}}' />
    <meta property='og:site_name' content='{{$product->seo_title ?? setting('site.title')}}' />
    <meta property="og:image" content="{{ isset($product->image_fb) ? asset("storage/{$product->image_fb}") : asset('storage/'.setting('site.logo'))}}" />
    <meta property="fb:app_id" content="{{setting('site.app_id')}}"/>

    <link rel="canonical" href="{{route('home')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/style.css?v=2.5')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/media-style.css?v=2.5')}}">

    <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
    <script type="text/javascript">
        var currentRoute='{{Route::currentRouteName()}}';
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var appID = "{{setting('site.app_id')}}";
    </script>
    
    {{-- <script type="text/javascript" src="{{asset('js/layout.js?v=2.5')}}"></script> --}}
    <script type="text/javascript" src="{{asset('js/common.js')}}"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <div id="fb-root"></div>
    <script defer>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_GB/sdk.js#xfbml=1&autoLogAppEvents=1&version=v3.2&appId=${appID}";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <div id="page" class="wsmenucontainer clearfix ">
        <div class="overlapblackbg"></div>
        <div class="olay-css"></div>

        @include('components.header')

        @yield('content')

        @include('components.footer')

        {{-- <script type="text/javascript" src="{{asset('js/quatang_box.js')}}"></script> --}}
        <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery-migrate-1.2.1.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/webslidemenu.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/owl.carousel.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/slick.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.tools.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/notify.js')}}"></script>
        {{-- <script type="text/javascript" src="{{asset('js/validator.js')}}"></script> --}}
        <script type="text/javascript" src="{{asset('js/index.js?v=2.5')}}"></script>
        @stack('js')

        <div style="display:inline;"></div>

    <div class="support">
        <div class="zalo-support">
            <a data-animate="fadeInDown" rel="noopener noreferrer" href="{{$information->twitter}}" target="_blank" class="button success" style="border-radius:99px;" data-animated="true">
                <span>Chat Zalo </span>
            </a>
        </div>
        <div class="fb-support">
            <a data-animate="fadeInDown" rel="noopener noreferrer" href="{{$information->facebook}}" target="_blank" class="button success" style="border-radius:99px;" data-animated="true">
                <span>Chat Facebook</span>
            </a>
        </div>
        <div class="hotline2">
            <a id="callnowbutton" href="tel:{{str_replace(' ','',$information['hotline'])}}">HL:{{$information['hotline']}}</a><i class="fa fa-phone"></i>
        </div>
        <div class="hotline1">
            <a id="callnowbutton" href="tel:{{str_replace(' ','',$information['phone1'])}}">ĐT:{{$information['phone1']}}</a><i class="fa fa-phone"></i>
        </div>
    </div>

    </div>
    {{-- <script async defer src="https://apis.google.com/js/api.js"></script>
    <!-- Bộ đếm google zippozippo.com-->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- Google Tag Manager -->
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-M6XBXCS');

    </script>
    <!-- End Google Tag Manager -->
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M6XBXCS" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- Kết thúc bộ đếm google   --> --}}

</body>
</html>
