<?php

namespace App\Traits;

use App\Models\Product;
use App\Models\Pivot;

trait Session
{
    /**
     * Override cart in session to ensure no data manipulation in client
     *
     * @param [Array] $sessionCart
     * @return $totalPrice
     */
    public function override_session_data(array $sessionCart, String $newLocale=null): int
    {
        $totalPrice = 0;
        $newLocale ?? $newLocale = app()->getLocale();
        $idArray = array_map(function ($cart) {
            return $cart->product_id;
        }, $sessionCart);
        $unique_id_array = array_map(function ($cart) {
            return $cart->unique_id;
        }, $sessionCart);
        
        //Use DB data to override data in session to ensure they're not manipulated
        $addedProducts = Product::withTranslations($newLocale)->findOrFail($idArray)->translate($newLocale)->keyBy('id');
        $query = Pivot::whereIn('pivot_color_products.id',$unique_id_array);
        $query->when($newLocale == 'vi', function($q){
            return $q->join('yts_colors as color', 'color.id', '=', 'pivot_color_products.color_id')
                    ->select('color.*');
        });
        $query->when($newLocale == 'en', function($q){
            return $q->join('translations as trans', 'trans.foreign_key', '=', 'pivot_color_products.color_id')
                    ->where('trans.table_name', 'yts_colors')
                    ->where('trans.column_name', 'name')
                    ->select('trans.value as name');
        });
        $addedColors = $query->get();
        
        foreach ($sessionCart as $key => $cart):
            $cart->price = $addedProducts[$cart->product_id]['current_price'];
            $cart->name = $addedProducts[$cart->product_id]['name'];
            $cart->brand = $addedProducts[$cart->product_id]['brand'];
            $cart->color = $addedColors[$key]['name'];
            $totalPrice += $cart->price * $cart->quantity;
        endforeach;
        
        $this->set_cookie($sessionCart);
        return $totalPrice;
    }

    /**
     * set cookie
     *
     * @param [object] $sessionCart
     * @return void
     */
    public function set_cookie( $sessionCart, $name='cart'): void
    {
        setcookie($name, json_encode($sessionCart), time() + 3600 * 24, '/');
    }
}
