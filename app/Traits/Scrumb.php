<?php

namespace App\Traits;

/**
 * 
 */
trait Scrumb
{
    public function getScrumb($categoryDetail)
    {
        $scrumbs    = [];
        $scrumbs[0] = $categoryDetail->toArray();

        if ($categoryDetail->parent_id):
            $scrumbs[count($scrumbs)] = $categoryDetail->parentCategory->toArray();
            if ($categoryDetail->parentCategory->parent_id):
                $scrumbs[count($scrumbs)] = $categoryDetail->parentCategory->parentCategory->toArray();
            endif;
        endif;
        krsort($scrumbs);
        return $scrumbs;
    }
}
