<?php

namespace App\Traits;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

trait UpdateUsers
{
    /**
     * Handle a profile update
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)
    {
        $this->validatorForUpdateProfile($request);

        $this->update($request->all());

        return redirect()->back()->with('status', 'Cập nhật thành công');
    }

    public function updateAddress(Request $request)
    {
        $this->validatorForUpdateAddress($request);

        $this->update($request->all());

        return redirect()->back()->with('status', 'Cập nhật thành công');
    }

    public function updatePasswordChange(Request $request)
    {
        $this->validatorForUpdatePassword($request);

        User::find(Auth::id())->update(['password' => Hash::make($request->password)]);

        return redirect()->back()->with('status', 'Đổi mật khẩu thành công');
    }

    /**
     * Get a validator for an incoming profile update.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorForUpdateProfile(Request $request)
    {
        if ($request->email != Auth::user()->email):
            return $request->validate([
                'email'    => 'required|string|email|max:255|unique:users',
                'phone'    => 'required|string|min:9',
                'birthday' => 'required|date',
            ]);
        else:
            return $request->validate([
                'phone'    => 'required|string|min:9',
                'birthday' => 'required|date',
            ]);
        endif;
    }

    /**
     * Create a new user instance after a valid validation.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function update(array $data)
    {
        return User::find(Auth::id())->update($data);
    }

    protected function validatorForUpdateAddress(Request $request)
    {
        return $request->validate([
            'name'     => 'required|string|max:255',
            'address'  => 'required',
            'city'     => 'required',
            'district' => 'required',
            'ward'     => 'required',
        ]);
    }

    protected function validatorForUpdatePassword(Request $request)
    {
        $user = Auth::user();
        return $request->validate([
            'current_password' => ['required', 'min:6', function ($attribute, $value, $fail) use ($user) {
                if (!\Hash::check($value, $user->password)):
                    return $fail(__($attribute . ' không đúng.'));
                endif;
            }],
            'password'         => 'required|min:6|confirmed',
        ]);
    }
}
