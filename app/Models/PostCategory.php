<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;

class PostCategory extends Model
{
    protected $table   = "categories";
    protected $guarded = [];

    public function posts()
    {
        return $this->hasMany('App\Models\Post')
            ->published()
            ->orderBy('created_at', 'DESC');
    }

    public function parentId()
    {
        return $this->belongsTo(self::class);
    }
}
