<?php

namespace App\Models;

use App\Notifications\ResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'gender', 'birthday', 'city', 'district', 'ward', 'address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Override default function
     *
     * @return void
     */
    public function getLocaleAttribute()
    {
        // return $this->settings['locale']; //default
        return app()->getLocale();
    }

    public function districtList()
    {
        return $this->belongsTo('App\Models\District', 'city', 'matp');
    }

    public function wardList()
    {
        return $this->belongsTo('App\Models\Ward', 'district', 'maqh');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }
}
