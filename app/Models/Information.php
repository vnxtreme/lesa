<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Information extends Model
{
    use Translatable;
    protected $translatable = [
        // 'address',
        // // 'email1',
        // // 'email2',
        // // 'phone1',
        // // 'phone2',
        // // 'hotline',
        // 'working_hours',
        // 'shipping_fee',
        // 'bank_info',
        // // 'facebook',
        // // 'twitter',
        // // 'instagram',
        // // 'youtube',
        // 'copyright',
        // 'copyright_images'
    ];

    protected $table = "yts_information";

    // public static function boot()
    // {
    //     parent::boot();

    //     self::creating(function ($model) {
    //         $model->shipping_fee = str_replace('.', '', $model->shipping_fee);
    //     });

    //     self::updating(function ($model) {
    //         $model->shipping_fee = str_replace('.', '', $model->shipping_fee);
    //     });
    // }

}
