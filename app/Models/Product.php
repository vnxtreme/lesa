<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
// use TCG\Voyager\Traits\HasRelationships;
use TCG\Voyager\Traits\Resizable;
use TCG\Voyager\Traits\Translatable;

class Product extends Model
{
    use Resizable,
        Translatable;

    protected $table ="yts_products";
    protected $translatable = ['name', 'seo_title', 'excerpt', 'body', 'slug', 'meta_description', 'meta_keywords'];

    const PUBLISHED = 'PUBLISHED';

    protected $guarded = [];

    /**
     * Remove dot in price
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            /* replace \ problem with javascript */
            if(isset($model->image)) $model->image = str_replace('\\', '/', $model->image);
            if(isset($model->images)) $model->images = str_replace('\\\\', '/', $model->images);

            $model->current_price = str_replace('.', '', $model->current_price );
            $model->old_price = str_replace('.', '', $model->old_price );
            // $model->thumbnail = substr_replace($model->thumbnail, '-compress', strpos($model->thumbnail, '.'), 0);
        });

        self::updating(function($model){
            /* replace \ problem with javascript */
            if(isset($model->image)) $model->image = str_replace('\\', '/', $model->image);
            if(isset($model->images)) $model->images = str_replace('\\\\', '/', $model->images);

            $model->current_price = str_replace('.', '', $model->current_price );
            $model->old_price = str_replace('.', '', $model->old_price );

            // $checkStr = strpos($model->thumbnail, '-compress');
            // if(!$checkStr):
            //     $model->thumbnail = substr_replace($model->thumbnail, '-compress', strpos($model->thumbnail, '.'), 0);
            // endif;
        });
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function color()
    {
        return $this->belongsToMany('App\Models\Color', 'pivot_color_products');
    }

    public function pivot()
    {
        return $this->hasMany('App\Models\Pivot', 'product_id');
    }

    public function findOrFailById($id)
    {
        return $this->with('color', 'category')->findOrFail($id);
    }

    public function save(array $options = [])
    {
        // If no author has been assigned, assign the current user's id as the author of the post
        if (!$this->author_id && Auth::user()) {
            $this->author_id = Auth::user()->id;
        }

        parent::save();
    }

    public function authorId()
    {
        return $this->belongsTo(Voyager::modelClass('User'), 'author_id', 'id');
    }

    /**
     * Scope a query to only published scopes.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished(Builder $query)
    {
        return $query->where('status', '=', static::PUBLISHED);
    }
}
