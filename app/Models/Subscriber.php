<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    protected $table ="yts_subscribers";
    protected $fillable = ['email'];
}
