<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'yts_orders';

    protected $fillable = [
        'product_id','color','price','quantity','order_info_id','unique_id', 'brand'
    ];

    public function orderInfo()
    {
        return $this->belongsTo('App\Models\OrderInfo');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    /**
     * Remove dot in price
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->price = str_replace('.', '', $model->price );
        });

        self::updating(function($model){
            $model->price = str_replace('.', '', $model->price );
        });
    }
}
