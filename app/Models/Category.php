<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\HasRelationships;
use TCG\Voyager\Traits\Resizable;
use TCG\Voyager\Traits\Translatable;

class Category extends Model
{
    use Translatable,
        Resizable,
        HasRelationships;

    protected $translatable = ['slug', 'name', 'short_name'];

    protected $table = 'yts_categories';

    protected $fillable = ['slug', 'name', 'short_name'];

    // public function posts()
    // {
    //     return $this->hasMany(Voyager::modelClass('Post'))
    //         ->published()
    //         ->orderBy('created_at', 'DESC');
    // }

    public function parentId()
    {
        return $this->belongsTo(self::class);
    }

    public function product_by_parent_category()
    {
        return $this->hasMany('App\Models\Product', 'parent_category_id', 'id');
    }

    public function product()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function getLimitProduct()
    {
        return $this->hasMany('App\Models\Product', 'parent_category_id', 'id')->published()->orderBy('id', 'desc')->limit(8);
        // return $this->hasMany('App\Models\Product')->published()->orderBy('id', 'desc')->limit(8);
    }

    public function childCategories()
    {
        return $this->hasMany(self::class, 'parent_id', 'id')->where('parent_id', '!=', null);
    }

    public function parentCategory()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }
}
