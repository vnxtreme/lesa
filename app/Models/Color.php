<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Color extends Model
{
    use Translatable;
    protected $translatable = ['name'];
    protected $table = "yts_colors";

    public function product()
    {
        return $this->belongsToMany('App\Models\Product', 'pivot_color_products');
    }
}
