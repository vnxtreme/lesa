<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class TermsAndPolicy extends Model
{
    use Translatable;
    protected $translatable = ['terms', 'policy'];
    protected $table = "yts_terms_and_policies";
}
