<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Carousel extends Model
{
    use Translatable;
    protected $translatable = ['title', 'link'];
    protected $table = "yts_carousels";
}
