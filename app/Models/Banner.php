<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Banner extends Model
{
    use Translatable;

    protected $table = "yts_banners";
    protected $translatable = [
        // 'banner_full_width',
        'banner_full_width_title',
        'banner_full_width_link',
        // 'banner_left',
        'banner_left_title',
        'banner_left_link',
        // 'banner_right_top',
        'banner_right_top_title',
        'banner_right_top_link',
        // 'banner_right_bottom_left',
        'banner_right_bottom_left_title',
        'banner_right_bottom_left_link',
        // 'banner_right_bottom_right',
        'banner_right_bottom_right_title',
        'banner_right_bottom_right_link',
        // 'product_ads',
        'product_ads_title',
        'product_ads_link'
    ];

}
