<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    protected $table = "yts_enquiries";
    protected $fillable = ['name', 'email', 'enquiry'];
}
