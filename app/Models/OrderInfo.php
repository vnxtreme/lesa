<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderInfo extends Model
{
    protected $table ='yts_order_info';
    protected $fillable = [
        "payment_name",
        'payment_email',
        "payment_telephone",
        "payment_address",
        "payment_city",
        "payment_district",
        "payment_ward",
        "shipping_method",
        "payment_method",
        'comment',
        'delivery_fee',
        'collect_fee',
        'total',
        'status',
        'request_callback'
    ];
    public function order()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function getCity()
    {
        return $this->belongsTo('App\Models\City', 'payment_city', 'matp');
    }

    public function getDistrict()
    {
        return $this->belongsTo('App\Models\District', 'payment_district', 'maqh');
    }

    public function getWard()
    {
        return $this->belongsTo('App\Models\Ward', 'payment_ward', 'xaid');
    }

    
    
    public function paymentCity()
    {
        return $this->belongsTo('App\Models\City', 'payment_city', 'matp');
    }

    public function paymentDistrict()
    {
        return $this->belongsTo('App\Models\District', 'payment_district', 'maqh');
    }

    public function paymentWard()
    {
        return $this->belongsTo('App\Models\Ward', 'payment_ward', 'xaid');
    }
}
