<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use App\Models\OrderInfo;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;

class AjaxController extends Controller
{
    public function showProductHomepage(Request $request)
    {
        $categoryID = $request->cat_id;
        $type       = $request->type;

        $products = Product::with('category')
            ->where('parent_category_id', $categoryID)
            ->when($type == 'latest', function ($query) use ($type) {
                return $query->orderBy('id', 'desc');
            })
            ->when($type == 'bestseller', function ($query) use ($type) {
                return $query->where('bestseller', true)->orderBy('id', 'desc');
            })
            ->when($type == 'bestprice', function ($query) use ($type) {
                return $query->orderBy('current_price', 'desc');
            })
            ->when($type == 'hot', function ($query) use ($type) {
                return $query->where('outstanding', true)->orderBy('id', 'desc');
            })
            ->when($type == 'mostview', function ($query) use ($type) {
                return $query->orderBy('view', 'desc');
            })
            ->limit(8)
            ->get();

        $html = '';
        if ($products):
            $html .= $this->render_product_list_to_home_html($products);
        endif;

        return $html;
    }

    protected function render_product_list_to_home_html(Collection $products)
    {
        $html = '<div class="row list-product">';
        foreach ($products as $product):
            $percentage     = 0;
            $percentageHtml = '';
            $oldPriceHtml = '';
            $oldPrice     = $product->old_price ? number_format($product->old_price, 0, ',', '.') : 0;
            
            if ($product->old_price > $product->current_price):
                $percentage     = round(($product->old_price - $product->current_price) * 100 / $product->current_price);
                $percentageHtml = "<span class='price_discount'>-{$percentage}%</span>";
                $oldPriceHtml = "<div class='pd-price pd-price-old'>
                                    <span class='price_value' itemprop='price'>{$oldPrice}</span>
                                    <span class='price_symbol'>đ</span>
                                </div>";
            endif;
            
            $currentPrice = $product->current_price ? number_format($product->current_price, 0, ',', '.') : 0;
            
            $html .= "<div class='col-xs-6 col-sm-4 col-md-3'>
	                <div class='item'>
	                    <div class='cover imgproduct'>
	                        <a href='".route('product.categorySlug.id', ['categorySlug' => $product->category->name, 'id' => $product->id])."' title='{$product->name}'>
	                            <img src='" . asset("storage/{$product->thumbnail('medium', 'image')}") . "' class='imgresponsive lazy' alt='{$product->name}' />
	                        </a>
	                    </div>
	                    <h3>
	                        <a href='" . route('product.categorySlug.id', ['categorySlug' => $product->category->slug, 'id' => $product->id]) . "' title='{$product->name}'>
	                            {$product->name}
	                        </a>
	                    </h3>

	                    <div class='price'>
	                        <div class='pd-price'>
	                            <span class='price_value' itemprop='price'>{$currentPrice}</span>
	                            <span class='price_symbol'>đ</span>
	                            " . $percentageHtml . "
	                        </div>
	                        ".$oldPriceHtml."
	                        <div class='pd-views'><i class='fa fa-user-o'></i> {$product->view}</div>
	                    </div>

	                </div>
	            </div>";
        endforeach;
        $html .= '</div>';
        if (count($products) == 8):
            $html .= "<div class='view-all'><a href=" . route('product.categorySlug', ['categorySlug' => $product->category->slug]) . ">Xem tất cả</a></div>";
        endif;
        return $html;
    }

    /**
     * return District/province to ajax call
     *
     * @param Request $request
     * @return string||null
     */
    public function getDistrictList(Request $request)
    {
        $cityId = $request->cityId;
        if($request->name):
            return \DB::table('devvn_quanhuyen')->where('matp', $cityId)->first();
        endif;
        return \DB::table('devvn_quanhuyen')->where('matp', $cityId)->orderBy('type', 'desc')->orderByRaw('CHAR_LENGTH(name)')->get();
        // return \DB::table('devvn_quanhuyen')->where('matp', $cityId)->orderByRaw('CHAR_LENGTH(name)')->get();
    }

    public function getWardList(Request $request)
    {
        $districtId = $request->districtId;
        if($request->name):
            return \DB::table('devvn_xaphuongthitran')->where('maqh', $districtId)->first();
        endif;
        return \DB::table('devvn_xaphuongthitran')->where('maqh', $districtId)->orderBy('name')->get();
    }

    public function getName(Request $request)
    {
        $id = $request->id;
        $table = $request->table;

        switch ($table) {
            case 'district':
                $row = \DB::table('devvn_quanhuyen')->where('maqh', $id)->first();
                return response()->json(['name' => $row->name]);
                break;
            case 'ward':
                $row = \DB::table('devvn_xaphuongthitran')->where('xaid', $id)->first();
                return response()->json(['name' => $row->name]);
                break;
            default://city
                $row = \DB::table('devvn_tinhthanhpho')->where('matp', $id)->first();
                return response()->json(['name' => $row->name]);
                break;
        }
    }

    public function changeOrderStatus(Request $request)
    {
        $status = $request->status;
        $orderId = $request->orderId;

        OrderInfo::find($orderId)->update(['status' => $status]);
        
        return response()->json(['status' => 'success']);
    }

    public function getChildCategories(Request $request)
    {
        $child_categories = Category::where('parent_id', $request->id)->get();

        return response()->json(['data' => $child_categories], 201);
    }
}
