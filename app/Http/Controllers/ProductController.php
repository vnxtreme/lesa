<?php

namespace App\Http\Controllers;

use App\Mail\OrderConfirm;
use App\Mail\OrderReceive;
use App\Models\Bank;
use App\Models\Category;
use App\Models\City;
use App\Models\District;
use App\Models\Order;
use App\Models\OrderInfo;
use App\Models\Product;
use App\Models\RequestCallback;
use App\Models\TempUser;
use App\Models\TermsAndPolicy;
use App\Models\User;
use App\Models\Ward;
use App\Traits\Scrumb;
use App\Traits\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\EmailTemplate;

class ProductController extends Controller
{
    use Session, Scrumb;

    protected $relatedProductsLimit = 12;
    protected $hotProductsLimit     = 5;
    protected $searchLimit          = 30;

    /**
     * General data
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function all()
    {
        $products = Product::published()->paginate(30);
        $scrumbs  = ['Xem tất cả'];
        $this->variable += [
            'products' => $products,
            'scrumbs'  => $scrumbs,
        ];

        return view("product.list", $this->variable);
    }

    /**
     * Display a listing of products based on Category.
     *
     * @param String $categorySlug
     * @return view
     */
    function list(String $categorySlug, Request $request) {
        if ($categorySlug == 'xem-tat-ca'):
            $query = Product::with('pivot', 'category')
                ->published()
                ->withTranslations(app()->getLocale())
                ->select('yts_products.*', 'c.slug')
                ->join('yts_categories as c', 'c.id', '=', 'yts_products.category_id')
            // ->where('c.slug', $viCategorySlug)
            ;

            $query
                ->when($request->type == 'default', function ($q) use ($request) {
                    return $q->orderBy('id', $request->sort);
                })
                ->when($request->type == 'price', function ($q) use ($request) {
                    return $q->orderBy('current_price', $request->sort);
                })
                ->when($request->type == 'view', function ($q) use ($request) {
                    return $q->orderBy('view', $request->sort);
                })
                ->when($request->type == 'bestseller', function ($q) use ($request) {
                    return $q->orderBy('bestseller', $request->sort);
                });
            $products = $query->orderBy('id', 'desc')->paginate(40);

            // $categoryDetail = Category::with('childCategories', 'parentCategory')->whereSlug($categorySlug)->first();
            // $scrumbs        = $this->getScrumb($categoryDetail);
            $scrumbs = [
                ['name' => 'XEM TẤT CẢ SẢN PHẨM'],
            ];

            ////Small menu on right sidebar
            // $thisCategorySlug = $scrumbs[count($scrumbs) - 1]['slug'];
            // $thisCategory     = Category::with('childCategories')->where('slug', $thisCategorySlug)->first();
            $this->variable += [
                'products'     => $products,
                'categorySlug' => $categorySlug,
                'scrumbs'      => $scrumbs,
                // 'thisCategory' => $thisCategory,
                'sort'         => $request->sort,
                'type'         => $request->type,
            ];
        else:
            if (app()->getLocale() == 'en'):
                $translated_category_slug = $this->convert_en_category_slug_to_vi($categorySlug);
                $viCategorySlug           = $translated_category_slug->slug; //override english slug with translated vi slug
            else:
                $viCategorySlug = $categorySlug;
            endif;

            $category = Category::where('slug', $viCategorySlug)->first();

            $query = Product::with('pivot', 'category')
                ->published()
                ->withTranslations(app()->getLocale());
            if ($category->parent_id === null):
                $query->select('yts_products.*', 'c.slug')
                    ->join('yts_categories as c', 'c.id', '=', 'yts_products.category_id')
                    ->where('yts_products.parent_category_id', $category->id);
            else:
                $query->select('yts_products.*', 'c.slug')
                    ->join('yts_categories as c', 'c.id', '=', 'yts_products.category_id')
                    ->where('yts_products.category_id', $category->id);
            endif;

            $query
                ->when($request->type == 'default', function ($q) use ($request) {
                    return $q->orderBy('id', $request->sort);
                })
                ->when($request->type == 'price', function ($q) use ($request) {
                    return $q->orderBy('current_price', $request->sort);
                })
                ->when($request->type == 'view', function ($q) use ($request) {
                    return $q->orderBy('view', $request->sort);
                })
                ->when($request->type == 'bestseller', function ($q) use ($request) {
                    return $q->orderBy('bestseller', $request->sort);
                });
            $products = $query->paginate(12);

            $categoryDetail = Category::with('childCategories', 'parentCategory')->whereSlug($categorySlug)->first();
            $scrumbs        = $this->getScrumb($categoryDetail);

            //Small menu on right sidebar
            $thisCategorySlug = $scrumbs[count($scrumbs) - 1]['slug'];
            $thisCategory     = Category::with('childCategories')->where('slug', $thisCategorySlug)->first();

            $this->variable += [
                'products'     => $products,
                'categorySlug' => $categorySlug,
                'scrumbs'      => $scrumbs,
                'thisCategory' => $thisCategory,
                'sort'         => $request->sort,
                'type'         => $request->type,
            ];
        endif;

        return view("product.list", $this->variable);
    }

    /**
     * Item detail
     *
     * @param String $categorySlug
     * @param Int $id
     * @return view
     */
    public function detail(String $categorySlug, Int $id)
    {
        if (app()->getLocale() == 'en'):
            $translated_category_slug = $this->convert_en_category_slug_to_vi($categorySlug);
            $viCategorySlug           = $translated_category_slug->slug; //override english slug with translated vi slug
        else:
            $viCategorySlug = $categorySlug;
        endif;

        $product = Product::with('category', 'color', 'pivot')
        // ->withTranslations(app()->getLocale())
            ->published()
            ->where('yts_products.id', $id)
            ->join('yts_categories as c', 'c.id', '=', 'yts_products.category_id')
            ->leftJoin('pivot_color_products as pivot', 'pivot.product_id', '=', 'yts_products.id')
            ->select('pivot.id as unique_id', 'c.*', 'yts_products.*')
            ->firstOrFail()
        // ->translate()
        ;
        $product->increment('view');
        $relatedProducts = Product::where('id', '!=', $product->id)
            ->published()
            ->where('category_id', $product->category_id)
            ->limit($this->relatedProductsLimit)
            ->get();

        $categoryID     = $product->category_id;
        $categoryDetail = Category::with('childCategories', 'parentCategory')->find($categoryID);
        $scrumbs        = $this->getScrumb($categoryDetail);

        $banks = Bank::where('status', 1)->get();

        // //Small menu on right sidebar
        // $thisCategorySlug = $scrumbs[count($scrumbs)-1]['slug'];
        // $thisCategory = Category::with('childCategories')->where('slug', $thisCategorySlug)->first();

        //Sản phẩm HOT = latest product
        $hotProducts = Product::with('category')->limit($this->hotProductsLimit)->where('id', '!=', $product->id)->published()->orderby('id')->get();

        $arrayImages = [];
        if ($product->images):
            $arrayImages = json_decode($product->images);
        endif;
        array_unshift($arrayImages, $product->image); //can't control multi-images order in Voyager so append thumbnail to array-images

        $this->variable += [
            'scrumbs'         => $scrumbs,
            'product'         => $product,
            'arrayImages'     => $arrayImages,
            'categorySlug'    => $categorySlug,
            // 'thisCategory'  => $thisCategory,
            'relatedProducts' => $relatedProducts,
            'hotProducts'     => $hotProducts,
            'banks'           => $banks,
        ];

        return view("product.detail", $this->variable);
    }

    /**
     * private func
     *
     * @param String $categorySlug
     * @return Collection
     */
    private function convert_en_category_slug_to_vi(String $categorySlug)
    {
        return \DB::table('translations as trans')
            ->where('trans.table_name', 'yts_categories')
            ->where('trans.column_name', 'slug')
            ->where('trans.value', $categorySlug)
            ->join('yts_categories as c', 'c.id', '=', 'trans.foreign_key')
            ->first();
    }

    /**
     * Add cart via ajax
     *
     * @param Request $request
     * @return json
     */
    public function addCart(Request $request)
    {
        if ($request->isMethod('post')):
            $request->validate([
                // 'unique_id'  => 'required',
                'product_id' => 'required|integer',
                'quantity'   => 'required|min:1',
            ]);

            $update                    = false;
            $productDetail             = Product::with('category')->find($request->product_id);
            $productDetail_small_image = $productDetail->thumbnail('small');

            if (isset($_COOKIE['cart']) and is_array($sessionCart = json_decode($_COOKIE['cart']))):
                $newItem = [
                    'product_id' => $request['product_id'],
                    'name'       => $productDetail->name,
                    'image'      => url("storage/{$productDetail_small_image}"),
                    'color'      => $productDetail->colorName,
                    'hex'        => $productDetail->colorHex,
                    'url'        => route("product.categorySlug.id", ['categorySlug' => $productDetail->category->slug, 'id' => $request['product_id']]),
                    'quantity'   => (int) $request['quantity'],
                    'price'      => $productDetail->current_price,
                    // 'unique_id'  => $productDetail->id,
                    'brand'      => $productDetail->brand,
                    'weight'     => $productDetail->weight,
                ];

                foreach ($sessionCart as $cart):
                    if ($cart->product_id == $request['product_id']):
                        $cart->quantity += $request['quantity'];
                        // $cart->name = $productDetail->name; //override if switch language, temporary method
                        // $cart->color = $productDetail->colorName; //override if switch language, temporary method
                        $update = true;
                    endif;
                endforeach;

                if (!$update):
                    array_unshift($sessionCart, $newItem);
                endif;
            else:
                $sessionCart = [
                    [
                        'product_id' => $request['product_id'],
                        'name'       => $productDetail->name,
                        'image'      => url("storage/{$productDetail_small_image}"),
                        'color'      => $productDetail->colorName,
                        'hex'        => $productDetail->colorHex,
                        'url'        => route("product.categorySlug.id", ['categorySlug' => $productDetail->categorySlug, 'id' => $request['product_id']]),
                        'quantity'   => (int) $request['quantity'],
                        'price'      => $productDetail->current_price,
                        // 'unique_id'  => $productDetail->id,
                        'brand'      => $productDetail->brand,
                        'weight'     => $productDetail->weight,
                    ],
                ];
            endif;

            $this->set_cookie($sessionCart);

            $json = [
                'data'     => $sessionCart,
                'status'   => true,
                'response' => trans('translation.add_:name_cart_success', ['name' => $productDetail->name]),
            ];
            return response()->json($json);
        endif;
    }

    /**
     * Append html to cart-modal. Use Jquery.load to append to view
     *
     * @return html
     */
    public function cartInfo()
    {
        $listTtemHtml = '';
        $totalPrice   = 0;
        $html         = '<ul class="dropdown-menu dropdown-cart">
                    <li class="minicart_wrapper">
                        <h4 class="empty text-center">' . trans('translation.empty_cart') . '</h4>
                    </li>

                    <li class="minicart_summary">
                        Tổng cộng:
                        <span class="price">
                            <span class="price_value">0</span>
                            <span class="price_symbol">đ</span>
                        </span>
                    </li>
                    <li class="minicart_actions clearfix">
                        <a class="btn btn-success btn-buy-now btn-buy-now-new" href="' . route('cart') . '" rel="nofollow">Xem đơn hàng</a>
                    </li>
                </ul>';
        $sessionCart = json_decode($_COOKIE['cart']);

        if (isset($_COOKIE['cart']) and count($sessionCart) > 0):
            foreach ($sessionCart as $cart):
                $listTtemHtml .= view('components.list-of-added-product', ['cart' => $cart])->render();
                $totalPrice += ($cart->price * $cart->quantity);
            endforeach;

            $html = view('components.cart-popup', [
                'sessionCart'  => $sessionCart,
                'listTtemHtml' => $listTtemHtml,
                'totalPrice'   => $totalPrice,
            ])->render();
        endif;

        return $html;
    }

    public function cartUpdate(Request $request)
    {
        $sessionCart                          = $this->variable['sessionCart'];
        $sessionCart[$request->key]->quantity = (int) $request->quantity;

        $this->set_cookie($sessionCart);
        $totalQuantity = array_reduce($sessionCart, function ($total, $object) {
            return $total + $object->quantity;
        });
        $totalPrice = array_reduce($sessionCart, function ($total, $object) {
            return $total + $object->price * $object->quantity;
        });

        $this->set_cookie($sessionCart);
        return response()->json([
            'data'          => $sessionCart,
            'totalQuantity' => $totalQuantity ?? 0,
            'totalPrice'    => $totalPrice ?? 0,
            'status'        => true,
        ]);
    }

    /**
     * Remove 1 single item on cart-modal
     *
     * @param Request $request
     * @return json
     */
    public function cartRemove(Request $request)
    {
        $sessionCart = $this->variable['sessionCart'];

        foreach ($sessionCart as $key => $cart):
            if ($cart->product_id == $request['productId']):
                unset($sessionCart[$key]);
            endif;
        endforeach;
        $sessionCart = array_values($sessionCart);

        $totalQuantity = array_reduce($sessionCart, function ($total, $object) {
            return $total + $object->quantity;
        });
        $totalPrice = array_reduce($sessionCart, function ($total, $object) {
            return $total + $object->price * $object->quantity;
        });

        $this->set_cookie($sessionCart);
        return response()->json([
            'data'          => $sessionCart,
            'totalQuantity' => $totalQuantity ?? 0,
            'totalPrice'    => $totalPrice ?? 0,
            'status'        => true,
        ]);
    }

    public function cart(Request $request)
    {
        if (isset($_COOKIE['cart']) and $_COOKIE['cart'] and count($sessionCart = json_decode($_COOKIE['cart'])) > 0):
            $totalPrice    = 0;
            $totalQuantity = 0;

            if ($request->isMethod('POST')): //change quantity

                foreach ($sessionCart as $cart):
                    $cart->quantity = $request['quantity'][$cart->product_id];
                    $totalQuantity += $cart->quantity;
                    $totalPrice += $cart->price * $cart->quantity;
                endforeach;

                $this->set_cookie($sessionCart);
                $this->variable['sessionCart']   = $sessionCart;
                $this->variable['totalPrice']    = $totalPrice;
                $this->variable['totalQuantity'] = $totalQuantity;
            endif;
            return view('product.cart', $this->variable);
        endif;

        return redirect()->route('home', $this->variable);
    }

    //step 1
    public function payment()
    {
        if ($this->variable['sessionCart']):
            if ($user = Auth::user()):
                $this->set_cookie($user, 'user');

                return redirect()->route('payment2');
            else:
                return view('product.payment', $this->variable);
            endif;
        endif;
        return redirect()->route('home');
    }

    //submit step 1
    protected function checkout(Request $request)
    {
        if ($request->isMethod('post')):
            $request->validate([
                "customer_name"  => "required|min:3",
                'email'          => 'required|email',
                "customer_phone" => "required|min:9|confirmed",
            ]);

            $user = TempUser::updateOrCreate(
                [
                    'email' => $request->email,
                    'phone' => $request->customer_phone,
                ],
                ['name' => $request->customer_name]
            );

            $this->set_cookie($user, 'user');

            return redirect()->route('payment2');
        endif;
        return redirect()->route('home');
    }

    //step 2
    public function payment2()
    {
        if ($this->variable['sessionCart'] and $_COOKIE['user']):
            $user                     = json_decode($_COOKIE['user']);
            $cities                   = \DB::table('devvn_tinhthanhpho')->orderByRaw('CHAR_LENGTH(type) desc')->get();
            $this->variable['cities'] = $cities;
            $this->variable['user']   = $user;

            return view('product.payment2', $this->variable);
        endif;

        return redirect()->route('home');
    }

    public function checkout2(Request $request)
    {
        // $mailtemplate = EmailTemplate::find(1);
        // Mail::send('emails.template', $mailtemplate, function($message) use ($mailtemplate)
        // {
        //     $message->subject($mailtemplate['title']);        
        //     $message->from('us@example.com', 'Laravel');
        //     $message->to('foo@example.com');
        // });
        // return 'done';
        if ($request->isMethod('post')):
            if (isset($_COOKIE['cart']) and is_array(json_decode($_COOKIE['cart']))):
                $request->validate([
                    'customer_name'    => "required|string",
                    'customer_phone'   => "required|min:9",
                    "customer_address" => "required|string",
                    "city"             => "required",
                    "district"         => "required",
                    "ward"             => "required",
                    "shipping_method"  => "required",
                ]);
                $sessionCart = $this->variable['sessionCart'];
                $totalPrice  = $this->variable['totalPrice'];
                $deliveryFee = $request->deliveryFee;
                $collectFee  = $request->collectFee;
                $productList = [];

                switch ($request->shipping_method) {
                    case '2': //bank
                        $collectFee = 0;
                        break;
                    case '3': //direct
                        $deliveryFee = 0;
                        $collectFee  = 0;
                        break;

                    default: //cod
                        //
                        break;
                }
                $totalPrice += ($deliveryFee + $collectFee);

                $orderInfo = \DB::transaction(function () use ($request, $sessionCart, $totalPrice, $deliveryFee, $collectFee) {
                    $orderInfo = OrderInfo::create([
                        'payment_name'      => $request->customer_name,
                        'payment_email'     => $request->email,
                        'payment_telephone' => $request->customer_phone,
                        'payment_address'   => $request->customer_address,
                        'payment_city'      => $request->city,
                        'payment_district'  => $request->district,
                        'payment_ward'      => $request->ward,
                        'shipping_method'   => $request->shipping_method,
                        'comment'           => $request->customer_note,
                        'delivery_fee'      => (int) $deliveryFee,
                        'collect_fee'       => (int) $collectFee,
                        'total'             => (int) $totalPrice,
                    ]);
                    foreach ($sessionCart as $cart) {
                        OrderInfo::find($orderInfo->id)->order()->create([
                            'product_id' => $cart->product_id,
                            'color'      => $cart->hex,
                            'quantity'   => $cart->quantity,
                            'price'      => $cart->price,
                            // 'unique_id'  => $cart->unique_id,
                            'brand'      => $cart->brand,
                        ]);
                    }

                    return $orderInfo;
                });

                //Get email data
                foreach ($sessionCart as $key => $cart):
                    $productList[$key] = [
                        'productPrice'    => $cart->price,
                        'productName'     => $cart->name,
                        'productQuantity' => $cart->quantity,
                    ];
                endforeach;
                $customerWard     = Ward::where('xaid', $request->ward)->first()->name;
                $customerDistrict = District::where('maqh', $request->district)->first()->name;
                $customerCity     = City::where('matp', $request->city)->first()->name;
                $paymentMethod    = null;
                switch ($request->shipping_method) {
                    case 2:
                        $paymentMethod = 'Chuyển khoản';
                        break;
                    case 3:
                        $paymentMethod = 'Mua tại cửa hàng';
                        break;

                    default:
                        $paymentMethod = 'Thanh toán khi nhận hàng';
                        break;
                }

                $mailData = [
                    'orderId'       => $orderInfo->id,
                    'name'          => $request->customer_name,
                    'email'         => $request->email,
                    'phone'         => $request->customer_phone,
                    'address'       => "{$request->customer_address}, {$customerWard}, {$customerDistrict}, {$customerCity}",
                    'paymentMethod' => $paymentMethod,
                    'subTotal' => $this->variable['totalPrice'],
                    'totalPrice'    => $totalPrice,
                    'products'      => $productList,
                ];
                //Get email data

                setcookie('cart', null, time() + 3600 * 24, '/');
                setcookie('user', null, time() + 3600 * 24, '/');
                $this->variable['sessionCart']        = '';
                $this->variable['totalQuantity']      = 0;
                $this->variable['totalPrice']         = 0;
                $this->variable['totalPriceCheckout'] = $totalPrice;
                $this->variable['deliveryFee']        = $deliveryFee;
                $this->variable['collectFee']         = $collectFee;

                /* NGANLUONG */
                if ($request->shipping_method == 2):
                    $information = $this->variable['information'];
                    $receiver    = $information->email1; //nganluong email
                    if (!$receiver):
                        echo "Chưa liên kết đến tài khoản Ngân lượng";
                        sleep(3);
                        return redirect()->route('home');
                    endif;
                    $this->variable['orderInfoId'] = $orderInfo->id;
                    $return_url                    = route('checkout-success', $this->variable);
                    $cancel_url                    = route('cancel', ["id" => $orderInfo->id]);

                    return redirect("https://www.nganluong.vn/button_payment.php?receiver={$receiver}&product_name=LESA{$orderInfo->id}&price={$totalPrice}&return_url={$return_url}&cancel_url={$cancel_url}");
                endif;

                /* Send email to notify */
                $emailConfig = \DB::table('yts_email_configs')->find(1);

                if ($emailConfig):
                    $data = [
                        'url'        => url("admin/order-info/{$orderInfo->id}/edit"),
                        'totalPrice' => $totalPrice,
                    ];
                    Mail::to($emailConfig->notify_to_email)->send(new OrderReceive($data));
                    Mail::to($request->email)->send(new OrderConfirm($mailData));
                endif;

                return view('product.checkout-success', $this->variable);
            endif;

            return redirect()->route('home');
        endif;
    }

    public function termsAndPolicy()
    {
        return TermsAndPolicy::find(1);
    }

    /**
     * Post and Get
     *
     * @param Request $request
     * @return view
     */
    public function search(Request $request)
    {
        $query = Product::with('pivot')
            ->withTranslations(app()->getLocale())
            ->where('yts_products.name', 'like', '%' . $request->keywords . '%')
        /* ->orderBy('id', 'desc') */;

        $query->when($request->category_id, function ($q) use ($request) {
            return $q->where('yts_products.category_id', '=', $request->category_id);
        });

        $query
            ->when($request->type == 'default', function ($q) use ($request) {
                return $q->orderBy('id', $request->sort);
            })
            ->when($request->type == 'price', function ($q) use ($request) {
                return $q->orderBy('current_price', $request->sort);
            })
            ->when($request->type == 'view', function ($q) use ($request) {
                return $q->orderBy('view', $request->sort);
            })
            ->when($request->type == 'bestseller', function ($q) use ($request) {
                return $q->orderBy('bestseller', $request->sort);
            });

        $query->when(app()->getLocale() == 'vi', function ($q) {
            $q->join('yts_categories as c', 'c.id', '=', 'yts_products.category_id')
                ->select('yts_products.*', 'c.slug as categorySlug');
        });
        $query->when(app()->getLocale() == 'en', function ($q) {
            return $q->join('translations as trans', 'trans.foreign_key', '=', 'yts_products.category_id')
                ->where('trans.table_name', 'yts_categories')
                ->where('trans.column_name', 'slug')
                ->select('yts_products.*', 'trans.value as categorySlug');
        });

        if ($request->limit) {
            $this->searchLimit = $request->limit;
        }

        $products = $query->paginate($this->searchLimit);

        $this->variable += [
            'products' => $products,
            'keywords' => $request->keywords,
            'sort'     => $request->sort,
            'type'     => $request->type,
        ];

        if ($request->isMethod('post')):
            $this->variable += ['request' => $request->all()];
        endif;

        return view("product.search", $this->variable);
    }

    //Nganluong user cancel payment
    public function cancel(Request $request)
    {
        $orderID = $request->id;
        OrderInfo::destroy($orderID);

        return redirect()->route('home');
    }

    public function checkoutSuccess(Request $request)
    {
        $order = OrderInfo::find($request->orderInfoId);

        if ($order):
            $this->variable['totalPrice']  = $request->totalPrice;
            $this->variable['deliveryFee'] = $request->deliveryFee;
            $this->variable['collectFee']  = $request->collectFee;

            return view('product.checkout-success', $this->variable);
        endif;

        return redirect()->route('home');
    }

    public function requestCallback(Request $request)
    {
        $request->validate([
            "customer_name"  => "required|min:3",
            'email'          => 'required|email',
            "customer_phone" => "required|min:9|confirmed",
        ]);

        TempUser::updateOrCreate(
            [
                'email' => $request->email,
                'phone' => $request->customer_phone,
            ],
            ['name' => $request->customer_name]
        );

        $sessionCart = $this->variable['sessionCart'];
        $totalPrice  = $this->variable['totalPrice'];
        $deliveryFee = 0;
        $collectFee  = 0;

        $orderInfo = \DB::transaction(function () use ($request, $sessionCart, $totalPrice) {
            $orderInfo = OrderInfo::create([
                'payment_name'      => $request->customer_name,
                'payment_email'     => $request->email,
                'payment_telephone' => $request->customer_phone,
                'comment'           => 'Yêu cầu gọi lại',
                'total'             => (int) $totalPrice,
                'request_callback'  => 1,
            ]);

            foreach ($sessionCart as $cart):
                OrderInfo::find($orderInfo->id)->order()->create([
                    'product_id' => $cart->product_id,
                    'quantity'   => $cart->quantity,
                    'price'      => $cart->price,
                    'brand'      => $cart->brand,
                ]);
            endforeach;

            return $orderInfo;
        });

        setcookie('cart', null, time() + 3600 * 24, '/');
        setcookie('user', null, time() + 3600 * 24, '/');
        $this->variable['sessionCart']        = '';
        $this->variable['totalQuantity']      = 0;
        $this->variable['totalPrice']         = 0;
        $this->variable['totalPriceCheckout'] = $totalPrice;
        $this->variable['deliveryFee']        = $deliveryFee;
        $this->variable['collectFee']         = $collectFee;

        return redirect()->route('checkout-success.quick');
    }

    public function quickCheckoutSuccess()
    {
        return view('product.checkout-success-quick', $this->variable);
    }
}
