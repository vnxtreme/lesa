<?php

namespace App\Http\Controllers\Voyager;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController as BaseVoyagerBaseController;

class VoyagerBaseController extends BaseVoyagerBaseController
{
    //***************************************
    //                ______
    //               |  ____|
    //               | |__
    //               |  __|
    //               | |____
    //               |______|
    //
    //  Edit an item of our Data Type BR(E)AD
    //
    //****************************************
    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $relationships = $this->getRelationships($dataType);

        $dataTypeContent = (strlen($dataType->model_name) != 0)
        ? app($dataType->model_name)->with($relationships)->findOrFail($id)
        : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name

        foreach ($dataType->editRows as $key => $row) {
            $details                               = /* json_decode */($row->details);
            $dataType->editRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        /* Addon */
        $colorQty = DB::table('pivot_color_products')->where('product_id', $id)->get();
        /* /.Addon */

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'colorQty'));
    }

    protected function getParentCategory($category)
    {
        if ($category->parent_id) {
            $parentCategory = $category->parentCategory;
            $category       = $this->getParentCategory($parentCategory);
        }
        return $category;
    }

    // POST BR(E)AD
    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

        /* INJECT PARENT CATEGORY ID */
        if($request->category_id):
            $categoryId       = $request->category_id;
            $category         = Category::with('parentCategory')->find($categoryId);
        
            if (isset($category->parent_id) and $category->parent_id):
                $category   = $this->getParentCategory($category);
            endif;
            $request->merge(['parent_category_id' => $category->id]);
        endif;
        /* .INJECT PARENT CATEGORY ID */

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->ajax()) {
            /* Addon: insert pivot table */
            if (isset($request->colors)):
                DB::table('pivot_color_products')->where('product_id', $data->id)->delete();

                foreach ($request->colors as $key => $color):
                    DB::table('pivot_color_products')->insert([
                        'product_id' => $data->id,
                        'color_id'   => $color,
                        'quantity'   => $request->quantities[$key],
                    ]);
                endforeach;
            endif;
            /* Addon: insert pivot table */
            $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

            event(new BreadDataUpdated($dataType, $data));

            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message'    => __('voyager::generic.successfully_updated') . " {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }
    }

    /**
     * POST BRE(A)D - Store data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));
        
        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if($request->latest == null):
            $request->merge(['latest' => 0]);
        endif;

        if($request->weight == null):
            $request->merge(['weight' => 1000]);
        endif;
        
        /* INJECT PARENT CATEGORY ID */
        if($request->category_id):
            $categoryId       = $request->category_id;
            $category         = Category::with('parentCategory')->find($categoryId);
            
            if (isset($category->parent_id) and $category->parent_id):
                $category   = $this->getParentCategory($category);
            endif;
            $request->merge(['parent_category_id' => $category->id]);
        endif;
        /* .INJECT PARENT CATEGORY ID */

        if (!$request->has('_validate')) {
            $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

            /* Addon: insert pivot table */
            if (isset($request->colors)):
                foreach ($request->colors as $key => $color) {
                    DB::table('pivot_color_products')->insert([
                        'product_id' => $data->id,
                        'color_id'   => $color,
                        'quantity'   => $request->quantities[$key],
                    ]);
                }
            endif;
            /* Addon: insert pivot table */

            event(new BreadDataAdded($dataType, $data));

            if ($request->ajax()) {
                return response()->json(['success' => true, 'data' => $data]);
            }

            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message'    => __('voyager::generic.successfully_added_new') . " {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }
    }
}
