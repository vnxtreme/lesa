<?php

namespace App\Http\Controllers;

use App\Models\BrandImage;
use App\Models\Carousel;
use App\Models\Enquiry;
use App\Models\Introduction;
use App\Models\Subscriber;
use App\Traits\Session;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    use Session;

    protected $folder = 'home';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Home page
     *
     * @return view
     */
    public function index()
    {
        // $banners   = Banner::withTranslations()->first()->translate();
        $carousels = Carousel::all();
        $products  = [];
        foreach ($this->variable['categories'] as $key => $category):
            $products[$key] = $category->getLimitProduct()->get();
        endforeach;

        $brandImages  = BrandImage::all()->split(2);
        $rightImages  = $brandImages[0];
        $bottomImages = $brandImages[1];

        /* change \ to / to avoid Revolution slider conflict */
        foreach ($carousels as &$carousel):
            $carousel->image = str_replace("\\", "/", $carousel->image);
        endforeach;

        $this->variable += [
            // 'banners'     => $banners,
            'carousels'    => $carousels,
            'products'     => $products,
            'rightImages'  => $rightImages,
            'bottomImages' => $bottomImages,
        ];

        return view("{$this->folder}.index", $this->variable);
    }

    /**
     * About us view
     *
     * @return view
     */
    public function about()
    {
        $introduction = Introduction::withTranslations(app()->getLocale())->find(1)->translate();

        $this->variable += [
            'introduction' => $introduction,
        ];
        return view("{$this->folder}.about", $this->variable);
    }

    /**
     * Contact us view
     *
     * @return view
     */
    public function contact()
    {
        return view("{$this->folder}.contact", $this->variable);
    }

    /**
     * Ajax send message in contact page
     *
     * @param Request $request
     * @return json
     */
    public function postContact(Request $request)
    {
        $request->validate([
            'email'   => 'required|email',
            'enquiry' => 'required',
        ]);

        Enquiry::create($request->all());
        return response()->json(trans("translation.message_sent"));
    }

    public function postSubscription(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
        ]);

        Subscriber::create($request->all());
        return response()->json(trans("translation.subscribe_success"));
    }

    /**
     * Switch only between 2 languages
     *
     * @param Request $request
     * @return void
     */
    public function switchLanguage(Request $request)
    {
        $languages = config('app.languages');
        $locale    = app()->getLocale();
        $prefix    = '';
        if (array_key_exists($locale, $languages)):
            unset($languages[$locale]);
            $newLocale = array_keys($languages)[0];

            if (in_array($newLocale, config('app.alt_langs'))):
                $prefix = $newLocale;
            endif;
            if (isset($_COOKIE['cart']) and is_array($sessionCart = json_decode($_COOKIE['cart']))):
                $this->override_session_data($sessionCart, $newLocale);
            endif;

            return redirect($prefix); //return to home with new language
        endif;
        return redirect()->back();
    }

    public function iframe()
    {
        return view('iframe');
    }

}
