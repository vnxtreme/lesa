<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Category;
use App\Models\Information;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $variable = [];

    public function __construct()
    {
        $categories = Category::with('childCategories', 'getLimitProduct','getLimitProduct.category')
            ->where('parent_id', null)
            ->orderBy('order')
            ->get();
        $information = Information::withTranslations(app()->getLocale())
            ->find(1)
            ->translate();
        $addresses = Address::orderBy('headquarter', 'desc')->get();

        $sessionCart = null;
        if (isset($_COOKIE['cart']) and is_array($sessionCart = json_decode($_COOKIE['cart']))):
            $totalQuantity = array_reduce($sessionCart, function ($total, $object) {
                return $total + $object->quantity;
            });
            $totalPrice = array_reduce($sessionCart, function ($total, $object) {
                return $total + $object->price * $object->quantity;
            });
        endif;

        $this->variable = [
            'addresses'     => $addresses,
            'categories'    => $categories,
            'information'   => $information,
            'locale'        => app()->getLocale(),
            'sessionCart'   => $sessionCart,
            'totalQuantity' => $totalQuantity ?? 0,
            'totalPrice'    => $totalPrice ?? 0,
        ];
    }
}
