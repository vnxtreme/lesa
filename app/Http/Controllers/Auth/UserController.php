<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\OrderInfo;
use App\Traits\UpdateUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    use UpdateUsers;
    
    public function __construct() {
        parent::__construct();
        $this->middleware('auth');
    }

    public function showProfile()
    {
        $this->variable['user'] = Auth::user();

        return view('auth.profile', $this->variable);
    }

    public function showAddress()
    {
        $this->variable['cities'] = \DB::table('devvn_tinhthanhpho')->get();
        $this->variable['user'] = User::with('wardList', 'districtList')->whereId(Auth::id())->first();
        
        return view('auth.address', $this->variable);
    }

    public function showOrders()
    {
        $orderInfo = OrderInfo::with('order', 'order.product', 'order.product.category')->where('payment_email', Auth::user()->email)->get();

        $this->variable['user'] = Auth::user();
        $this->variable['orderInfo'] = $orderInfo;

        return view('auth.order-items', $this->variable);
    }

    public function ordersDetail(Request $request)
    {
        $order = OrderInfo::with('getCity', 'getDistrict', 'getWard', 'order', 'order.product', 'order.product.category')->where('id', $request->orderID)->where('payment_email', Auth::user()->email)->firstOrFail();
        
        $this->variable['user'] = Auth::user();
        $this->variable['order'] = $order;

        return view('auth.order-detail', $this->variable);
    }

    public function showPasswordChange()
    {
        $this->variable['user'] = Auth::user();

        return view('auth.password-change', $this->variable);
    }
}
