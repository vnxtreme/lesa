<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Traits\Scrumb;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Models\Category;

class PostController extends Controller
{
    use Scrumb;
    protected $listLimit = 12;

    public function __construct()
    {
        parent::__construct();
        $postCategories                   = DB::table('categories')->get();
        $this->variable['postCategories'] = $postCategories;
    }

    public function huongDanVaTinTuc()
    {
        $posts = Post::with('category')->published()->orderBy('id', 'DESC')->paginate($this->listLimit);

        $scrumbs = ['Hỏi Đáp Hướng Dẫn Và Tin Tức'];
        
        $this->variable['scrumbs'] = $scrumbs;
        $this->variable['posts'] = $posts;
        return view('post.list', $this->variable);
    }

    public function baiVietTheoDanhMuc(String $categorySlug)
    {
        $category     = Category::whereSlug($categorySlug)->first();
        $posts        = $category->posts()->paginate($this->listLimit);

        if($posts and $posts->count() == 1):
            return $this->baiViet($posts[0]->slug);
        endif;
        $scrumbs = $this->getScrumb($category);

        $this->variable['scrumbs'] = $scrumbs;
        $this->variable['posts']   = $posts;
        return view('post.list', $this->variable);
    }

    public function baiViet(String $slug)
    {
        $post    = Post::with('category')->where('slug', $slug)->published()->firstOrFail();
        $scrumbs = $this->getScrumb($post->category);

        $this->variable['scrumbs'] = $scrumbs;
        $this->variable['post']    = $post;
        return view('post.list', $this->variable);
    }
}
