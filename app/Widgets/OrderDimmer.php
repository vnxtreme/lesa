<?php

namespace App\Widgets;

use App\Models\Order;
use App\Models\OrderInfo;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Widgets\BaseDimmer;

class OrderDimmer extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $orderModel = new OrderInfo();
        $count = $orderModel->count();
        $latestOrder = $orderModel->latest()->first();

        return view('voyager::dimmer', array_merge($this->config, [
            'icon' => 'voyager-file-text',
            'title' => $count . ' ' . __("voyager::customize.orders"),
            'text' => __("voyager::customize.latest_order") . (isset($latestOrder) ? $latestOrder->created_at->format('d-m-Y') : ""),
            'button' => [
                'text' => __("voyager::customize.view_order"),
                'link' => route('voyager.order-info.index'),
            ],
            'image' => voyager_asset('images/widget-backgrounds/03.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return Auth::user()->can('browse', new Order());
    }
}
