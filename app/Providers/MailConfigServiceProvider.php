<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class MailConfigServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Extend to register customized email config.
     *
     * @return void
     */
    public function register()
    {
        if(Schema::hasTable('yts_email_configs')){
            $emailConfig = DB::table('yts_email_configs')->first();
            
            if ($emailConfig) {
                $config = array(
                    'driver' => 'smtp',
                    'host' => $emailConfig->host,
                    'port' => $emailConfig->port,
                    'from' => array(
                        'address' => $emailConfig->send_from_email,
                        'name' => $emailConfig->name
                    ),
                    'username' => $emailConfig->username,
                    'password' => $emailConfig->password,
                    // 'encryption'=>'ssl'
                );
                Config::set('mail', $config);
            }
        }
    }
}
